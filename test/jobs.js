'use strict';

// ** Dependencies
const logger = require('nodus-framework').logging.createLogger();
const program = require('nodus-framework').program;
const Action = require('nodus-framework').actions;
const Promise = require('bluebird');
const Job = require('../jobs/Job');

const ECHO_DELAY = {
    "name": "echo-delay",
    "handler": function (text, delay) {
        logger.info('ECHO-DELAY', arguments);
        return Promise
            .delay(Number(delay))
            .then(() => {
                logger.info('DELAY_COMPLETED');
                logger.info('Returning text...', text);
                return text;
            })
    },
    "parameters": [
        {
            "name": "text",
            "description": "Some text to echo back.",
            "type": "string",
            "required": true,
            "default": "Hello, World!"
        },
        {
            "name": "delay",
            "description": "The number of milliseconds to delay waiting for the job to complete",
            "type": "int",
            "required": true,
            "default": 1000 // 1 second
        }
    ]
};

Job.process(ECHO_DELAY.name, ECHO_DELAY.handler);