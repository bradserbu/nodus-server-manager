'use strict';

// ** Dependencies
const createServer = require('../web-api/servers/create');

const server = {
    "name": "aws-test-server",
    "provider": {
        "type": "aws",
        "config": {
            "aws_access_token": "fff",
            "aws_secret_key": "fff",
            "aws_region": "us-east-1",
            "aws_key_name": "bradserbu.com"
        }
    },
    "ssh": {
        "public_key": "/Users/bradserbu/.ssh/id_rsa.pub",
        "private_key": "/Users/bradserbu/.ssh/id_rsa"
    },
    "os": {
        "name": "ubuntu-16-04",
        "ssh_user": "ubuntu"
    },
    "instance": {
        "size": "t2.micro"
    },
    "network": {
        "ingress": [
            {
                "incoming": 22,
                "outgoing": 22
            }
        ]
    }
};

// ** Exports
module.exports = () => createServer(server);