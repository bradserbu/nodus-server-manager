'use strict';

// ** Dependencies
const _ = require('lodash');
const Promise = require('bluebird');
const logger = require('nodus-framework').logging.createLogger();
const jobs = require('../jobs');

function run(text, delay) {

    logger.info('Running echo-delay...');

    return jobs
        .run('echo-delay', {
            text: text,
            delay: delay
                ? Math.floor(Number(delay))
                : 1000
        });
}

// ** Exports
module.exports = run;