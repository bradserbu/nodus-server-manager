'use strict';

// ** Dependencies
const _ = require('lodash');
const Activity = require('../jobs/Workflow').Activity;
const uuid = require('uuid');
const Promise = require('bluebird');
const database = require('../web-api/database');
const logger = require('nodus-framework').logging.createLogger();

function Activities(job) {
    const activities = _.mapValues(job, (handler, name) => Activity(name, handler));
    return Promise.props(activities);
}

function RunActivities(activities) {

    let result = Promise.resolve();

    _.each(activities, (activity, name) => {
        result = result
            .then(args => {
                logger.info('RUN_ACTIVITY', name, args);

                return activity.run(args);
            });
    });

    return result;
}

function Run(job) {

    const id = uuid.v4();

    return database
        .insert('Jobs', {
            id,
            status: 'created'
        })
        .then(() => Activities(job))
        .then(activities => ({
            id,
            start: () => database
                .update('Jobs', {status: 'started'}, {id})
                .then(() => RunActivities(activities))
        }));
}

module.exports = () => {

    // Create a new Job
    return Run({
        'get-name': () => Promise
            .delay(100)
            .then(() => 'Brad Serbu'),
        'wait-10-seconds': name => Promise
            .delay(10000)
            .then(() => name),
        'get-greeting': name => Promise
            .delay(100)
            .then(name => `Hello, ${name}`),
        'say-hello': greeting => console
            .log(greeting)
    })
        .then(job => job.start());
    // .then(job => job.start());
};

