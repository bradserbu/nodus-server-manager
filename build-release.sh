#!/usr/bin/env sh

# Exit on error
set -e

# Change directory to script location
cd "$(dirname "$0")"

# Read package version from package.json file
PACKAGE_VERSION=$(cat package.json \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

mktempdir=`mktemp -d 2>/dev/null || mktemp -d -t 'mytmpdir'`

PROGRAM_NAME=nodus-server-manager
NSM_HOME=`pwd`
NSM_VERSION=$PACKAGE_VERSION
INSTALL_SCRIPT=$NSM_HOME/setup.sh

NSM_RELEASE_DIR="$NSM_HOME/build/release"
echo
echo "- NSM_RELEASE_DIR=$NSM_RELEASE_DIR"
echo
echo "Packing release: $PROGRAM_NAME-$NSM_VERSION'..."

# Create NPM package
PACKAGE=`npm pack`

# Create TMP directory
TEMP_DIR=`mktemp -d 2>/dev/null || mktemp -d -t '$PROGRAM_NAME'`
mv $PACKAGE $TEMP_DIR

# Change to release folder
cd $TEMP_DIR

echo "Packaging release..."
tar -xvf $PACKAGE
mv package $PROGRAM_NAME
rm $PACKAGE

echo "Creating release package..."
tar -zcvf $PACKAGE $PROGRAM_NAME

echo "Coping package to '$NSM_RELEASE_DIR/$NSM_VERSION'..."
mkdir -p $NSM_RELEASE_DIR/$NSM_VERSION
cp `pwd`/$PACKAGE $NSM_RELEASE_DIR/$NSM_VERSION

echo "Copying 'setup.sh' script to releases folder..."
cp $INSTALL_SCRIPT $NSM_RELEASE_DIR/$NSM_VERSION
cp $INSTALL_SCRIPT $NSM_RELEASE_DIR