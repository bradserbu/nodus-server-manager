#!/usr/bin/env bash

echo "Building Container..."
docker build -t bradserbu/nodus-web .

echo "Starting Container..."
CONTAINER_ID=`docker run -e DEBUG=$DEBUG -e NODUS_SERVER_URL=$NODUS_SERVER_URL -p 3000:3000 -d bradserbu/nodus-web`

function stop {
    echo
    echo "Stopping Container..."
    docker stop "$CONTAINER_ID"
}

trap stop INT

echo "Monitoring logs... (CTRL+C) to exit"
docker logs -f "$CONTAINER_ID"
