#!/usr/bin/env bash

# Build Nodus Web Docker Image
docker build -t bradserbu/nodus-web . \
    && docker push bradserbu/nodus-web
