(function() {
    'use strict';

    angular.module('app.auth.login').
        controller('LoginController', LoginController);

    /** @ngInject */
    function LoginController($mdToast, $window, AuthService) {

        // Data
        var vm = this;
        vm.login = function() {
            console.log('LOGIN_BUTTON_CLICKED');
            return AuthService.Login(vm.form.email, vm.form.password).
                then(function(result) {
                    console.log('LOGIN_RESULT', result);

                    return $mdToast.show(
                        $mdToast.simple().
                            textContent('Login Successful.').
                            position('top right')
                            .theme('success-toast').hideDelay(20)
                    ).then(function() {
                        $window.location.href = '/';
                    });

                    // Redirect to /home
                }).catch(function(error) {
                    console.log('LOGIN_ERROR', error);
                    return $mdToast.show(
                        $mdToast.simple().
                            textContent(error.message).
                            position('top right').
                            theme('error-toast')
                    );
                });
        };

        return;

        // Methods

        //////////
    }
})();
