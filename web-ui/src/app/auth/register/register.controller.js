(function() {
    'use strict';

    angular.module('app.auth.register').
        controller('RegisterController', RegisterController);

    /** @ngInject */
    function RegisterController($state, $q, msApi, $mdToast) {

        // DATA
        var vm = this;
        vm.register = function() {
            console.log('REGISTER_BUTTON_CLICKED');
            return register(vm.form.name, vm.form.email, vm.form.password).
                then(function(result) {
                    console.log('REGISTER_RESULT', result);

                    return $mdToast.show(
                        $mdToast.simple().
                            textContent('Account Created Successfully.').
                            position('top right').
                            theme('info-toast').hideDelay(300)
                    ).then(function() {
                        // Redirect to LOGIN page
                        return $location.path('/login');
                    });
                }).catch(function(error) {
                    console.log('REGISTER_ERROR', error);
                    return $mdToast.show(
                        $mdToast.simple().
                            textContent(error.message).
                            position('top right').
                            theme('error-toast')
                    );
                });
        };

        return;

        // Methods
        /**
         * Get products
         */
        function register(name, email, password) {

            // TODO: Hash Password
            console.log('REGISTER', {
                name: name,
                email: email,
                password: password,
            });

            // Create a new deferred object
            var deferred = $q.defer();

            msApi.request('auth.register@get', {
                    name: name,
                    email: email,
                    password: password,
                },

                // SUCCESS
                function(response) {
                    console.log('API_RESPONSE', response);

                    // Resolve the promise
                    deferred.resolve(response.data);
                },

                // ERROR
                function(response) {
                    console.error('API_RESPONSE', response);

                    var error = response.data.error;

                    // Reject the promise
                    deferred.reject(error);
                }
            );

            return deferred.promise;
        }
    }
})();
