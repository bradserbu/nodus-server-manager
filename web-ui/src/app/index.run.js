(function ()
{
    'use strict';

    angular
        .module('fuse')
        .run(runBlock);

    /** @ngInject */
    function runBlock($rootScope, $timeout, $state, $location, $localStorage, $http, jwtHelper, AuthService)
    {
        // Activate loading indicator
        var stateChangeStartEvent = $rootScope.$on('$stateChangeStart', function ()
        {
            $rootScope.loadingProgress = true;
        });

        // De-activate loading indicator
        var stateChangeSuccessEvent = $rootScope.$on('$stateChangeSuccess', function ()
        {
            $timeout(function ()
            {
                $rootScope.loadingProgress = false;
            });
        });

        // Store state in the root scope for easy access
        $rootScope.state = $state;

        // Cleanup
        $rootScope.$on('$destroy', function ()
        {
            stateChangeStartEvent();
            stateChangeSuccessEvent();
        });

        // redirect to login page if not logged in and trying to access a restricted page
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            var publicPages = ['/login', '/register'];
            var restrictedPage = publicPages.indexOf($location.path()) === -1;

            // TODO: Validate Token
            if (restrictedPage && !AuthService.IsAuthenticated()) {
                $location.path('/login');
            }
        });

        // Authentication TODO: MOve to AuthService
        if ($localStorage.currentUser) {
            console.log('AUTH_USERNAME', $localStorage.currentUser);

            var token = $localStorage.currentUser.token;

            // ** Inject Auth Token on all requests
            $http.defaults.headers.common.Authorization = 'Bearer ' + token;
        }

        return;
    }
})();
