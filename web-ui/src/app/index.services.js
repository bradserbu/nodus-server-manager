(function() {
    'use strict';

    angular.module('fuse').config(config);

    /** @ngInject */
    function config(msApiProvider) {

        // ** Register API Endpoints

        // Authentication
        msApiProvider.register('auth.login', ['/api/auth/login']);
        msApiProvider.register('auth.logout', ['/api/auth/logout']);

        // Server Management
        msApiProvider.register('containers', ['/api/containers']);

        msApiProvider.register('reports', ['/api/reports']);

        // Clusters
        msApiProvider.register('clusters', ['/api/clusters']);
        msApiProvider.register('clusters.create', ['/api/clusters/create']);

        msApiProvider.register('servers', ['/api/servers']);
        msApiProvider.register('servers.get', ['/api/servers/:id', {
            id: "@id"
        }]);
        msApiProvider.register('servers.apps', ['/api/servers/:id/apps', {
            id: "@id"
        }]);
        msApiProvider.register('servers.charts', ['/api/charts']);
        msApiProvider.register('servers.dashboard', ['/api/dashboards/server']);
        // msApiProvider.register('services.dashboard.stats', ['/api/stats']);

        // Stats
        msApiProvider.register('stats', ['/api/stats']);
        msApiProvider.register('stats.server', ['/api/stats/server']);
        msApiProvider.register('stats.network', ['/api/stats/network']);

        // msApiProvider.register('server.stats', ['/api/server/stats/:ipAddress', {
        //     ipAddress: "@ipAddress"
        // }]);

        msApiProvider.register('server.stats', ['/api/servers/:id/stats'], {
            id: "@id"
        });

        msApiProvider.register('products', ['app/data/e-commerce/products.json']);

        // Charts
        msApiProvider.register('charts', ['/api/charts']);
        msApiProvider.register('charts.get', ['/api/charts/:id'], {
            id: "@id"
        });
        msApiProvider.register('charts.series', ['/api/charts/:id/series'], {
            id: "@id"
        });
    }

})();
