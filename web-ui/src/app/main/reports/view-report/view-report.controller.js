(function() {
    'use strict';

    angular.module('app.reports').
        controller('ViewReportDialogController', ViewReportDialogController);

    /** @ngInject */
    function ViewReportDialogController($mdDialog, Report, msUtils) {
        var vm = this;

        // Data
        vm.report = Report;
        vm.closeDialog = closeDialog;

        /**
         * Close dialog
         */
        function closeDialog() {
            $mdDialog.hide();
        }
    }
})();
