(function() {
    'use strict';

    angular.module('app.reports').
        controller('ReportsController', ReportsController);

    /** @ngInject */
    function ReportsController($state, $document, $mdDialog, ReportsData) {
        // Load View Model
        var vm = this;

        vm.reports = ReportsData.data;
        vm.dtInstance = {};
        vm.dtOptions = {
            dom: 'rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
            columnDefs: [
                {
                    targets: 0,
                    width: '20px',
                    filterable: false,
                    sortable: false,
                    render: function(data, type) {
                        if (type === 'display') {
                            switch (data.toUpperCase()) {
                                // case 'BAR':
                                //     return '<i class="icon-chart-bar s24"></i>';
                                // case 'LINE':
                                //     return '<i class="icon-chart-line s24"></i>';
                                default:
                                    return '<i class="icon-table"></i>';
                            }
                        }

                        return data;
                    }
                },
                {
                    // Target the actions column
                    targets: 1
                },
                {
                    // Target the actions column
                    targets: 4,
                    responsivePriority: 1,
                    filterable: false,
                    sortable: false,
                },
            ],
            initComplete: function() {
                var api = this.api(),
                    searchBox = angular.element('body').find('#reports-search');

                // Bind an external input as a table wide search box
                if (searchBox.length > 0) {
                    searchBox.on('keyup', function(event) {
                        api.search(event.target.value).draw();
                    });
                }
            },
            pagingType: 'simple',
            lengthMenu: [10, 20, 30, 50, 100],
            pageLength: 20,
            scrollY: 'auto',
            responsive: true
        };

        //////////
        // Methods
        vm.viewReport = viewReport;

        //////////

        /**
         * Go to add product
         */
        function viewReport(report) {
            $mdDialog.show({
                controller         : 'ViewReportDialogController',
                controllerAs       : 'vm',
                templateUrl        : 'app/main/reports/view-report/view-report.html',
                // parent             : angular.element($document.find('#content-container')),
                // targetEvent        : ev,
                clickOutsideToClose: true,
                locals             : {
                    Report: angular.copy(report)
                }
            });
        }
    }
})();
