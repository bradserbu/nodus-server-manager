(function ()
{
    'use strict';

    angular
        .module('app.reports', [
        // 3rd Party Dependencies
        'datatables',
        'nvd3',
    ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.reports', {
                url      : '/reports',
                views    : {
                    'content@app': {
                        templateUrl: 'app/main/reports/reports.html',
                        controller : 'ReportsController as vm'
                    }
                },
                resolve  : {
                    ReportsData: function (msApi)
                    {
                        return msApi.resolve('reports@get');
                    }
                }
            });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/reports');

        // Navigation
        // msNavigationServiceProvider.saveItem('app', {
        //     title : 'SAMPLE',
        //     group : true,
        //     weight: 1
        // });

        msNavigationServiceProvider.saveItem('performance.reports', {
            title    : 'Reports',
            icon     : 'icon-table-large',
            state    : 'app.reports',
            /*stateParams: {
                'param1': 'page'
             },*/
            translate: 'REPORTS.NAV',
            weight   : 4
        });
    }
})();
