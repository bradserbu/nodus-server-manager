(function () {
    'use strict';

    angular.module('app.servers').controller('ServersController', ServersController);

    /** @ngInject */
    function ServersController($rootScope, $scope, $state, $mdDialog, $document, ServersData, Api) {
        // Load View Model
        var vm = this;

        // TODO: Move Colored Columns to it's on controller/factory
        var DEFAULT_COLORS = [
            "light-blue",
            "deep-orange",
            "indigo",
            "green",
            "teal",
            "grey"
        ];

        function LabelClass(color, value) {
            return '<span class="' + "text-boxed m-0 " + color + "-bg white-fg" + '">' + value + "</span>";
        }

        function ColoredLabels(colors) {

            colors = colors || DEFAULT_COLORS;

            var next_index = 0;
            var labels = {};

            return function (value) {
                if (!labels.hasOwnProperty(value)) {
                    // Assign a new color
                    var color = colors[next_index++];
                    // labels[value] = ;
                    labels[value] = LabelClass(color, value);

                    return labels[value];
                } else {
                    // Use existing color
                    return labels[value];
                }
            }
        }

        /// Label Controllers for Tags and OS columns
        var tagLables = ColoredLabels();
        var osLabels = ColoredLabels(["indigo", "blue", 'teal']);

        console.log('SERVERS_DATA', ServersData);
        vm.servers = ServersData.data;
        vm.dtInstance = {};
        vm.dtOptions = {
            dom: 'rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
            columnDefs: [
                {
                    width: '10px',
                    targets: 0,
                    sortable: false,
                    filterable: false,
                    // render: function(code, type) {
                    //     if (type === 'display') {
                    //         if (code === '200') {
                    //             return '<i class="icon-checkbox-marked-circle green-500-fg"></i>';
                    //         }
                    //
                    //         return '<i class="icon-cancel red-500-fg"></i>';
                    //     }
                    //
                    //     if (type === 'filter') {
                    //         if (code) {
                    //             return '1';
                    //         }
                    //
                    //         return '0';
                    //     }
                    //
                    //     return code;
                    // },
                },
                {
                    targets: 1,
                    render: function (data, type) {
                        return LabelClass('grey', data);
                    }
                },
                {
                    // Target the instances column
                    targets: 3,
                    // width: '20px',
                    // filterable: false,
                    // sortable  : false,
                    // child: true,
                    render: function (data, type) {

                        if (type == 'display') {
                            return osLabels(data);
                        }

                        return data;
                    }
                },
                {
                    // TAGS
                    targets: 8,
                    responsivePriority: 2,
                    render: function (data, type, row, meta) {
                        console.log('ARGUMENTS', arguments);
                        console.log('TYPE', type);

                        if (data == '')
                            return data;

                        var tags = data.split(',');

                        if (type == 'display') {
                            // Display tags as colored labels
                            var result = [];
                            for (var lcv=0; lcv<tags.length; lcv++) {
                                var tag = tags[lcv];

                                //return '<span class="' + classes + "'>" + tag + "</span>";
                                result.push(tagLables(tag));
                            }

                            console.log('DATA', data);

                            return result.join(' ');
                        }

                        return data;
                    },
                },
                {
                    // Target the actions column
                    targets: 9,
                    // width: '50px',
                    responsivePriority: 1,
                    filterable: false,
                    sortable: false,
                },
            ],
            initComplete: function () {
                var api = this.api(),
                    searchBox = angular.element('body').find('#servers-search');

                // Bind an external input as a table wide search box
                if (searchBox.length > 0) {
                    searchBox.on('keyup', function (event) {
                        api.search(event.target.value).draw();
                    });
                }
            },
            pagingType: 'simple',
            lengthMenu: [50, 100, 250, 500],
            pageLength: 100,
            scrollY: 'auto',
            responsive: true,
        };

        //////////
        // Methods
        vm.refreshData = refreshData;
        vm.gotoServerDetail = gotoServerDetail;
        vm.gotoServerDashboard = gotoServerDashboard;
        vm.createServer = createServer;
        vm.destroyServer = destroyServer;

        // Open Actions Menu
        var originatorEv;
        vm.openMenu = function($mdMenu, ev) {
            originatorEv = ev;
            $mdMenu.open(ev);
        };

        //////////
        function refreshData() {
            console.log('Refreshing data...');
            $rootScope.loadingProgress = true;
            Api.getServers().then(function (response) {
                console.log('REFRESH_RESPONSE', response);
                vm.servers = response.data;
                vm.dtInstance.rerender();
                $rootScope.loadingProgress = false;
            });
        }

        function createServer() {
            // $mdDialog.show({
            //     controller: 'CreateServerDialogController',
            //     controllerAs: 'vm',
            //     templateUrl: 'app/main/servers/create-server/create-server.html',
            //     parent: angular.element($document.find('#content-container')),
            //     targetEvent: event,
            //     clickOutsideToClose: true,
            //     fullscreen: true,
            //     locals: {
            //
            //     }
            // });

            $state.go('app.servers_create');
        }

        /**
         * Go to product detail
         *
         * @param id
         */
        function gotoServerDetail(id)
        {
            $state.go('app.servers.view', {id: id});
        }

        function gotoServerDashboard(id)
        {
            $state.go('app.servers_dashboard', {id: id});
        }

        function destroyServer(id) {
            console.log('DESTROY_ACTION', id);
            // $state.go('app.servers_view', {id: id});
            $state.go('app.servers.action', {id: id, action: 'destroy'});
        }
    }
})();
