(function () {
    'use strict';

    angular
        .module('app.servers.create', [

        ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider) {
        // State
        $stateProvider
            .state('app.servers_create', {
                url: '/servers/create',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/servers/server-create/server-create.html',
                        controller: 'ServerCreateController as vm'
                    }
                },
                resolve: {

                }
            });

        // msNavigationServiceProvider.saveItem('resources.server_details', {
        //     title    : 'Servers Details',
        //     icon     : 'icon-server',
        //     state    : 'app.server-details',
        //     /*stateParams: {
        //      'param1': 'page'
        //      },*/
        //     translate: 'SERVERS.NAV',
        //     weight   : 2
        // });
    }
})();
