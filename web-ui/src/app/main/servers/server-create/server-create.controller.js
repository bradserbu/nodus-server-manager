(function () {
    'use strict';

    angular
        .module('app.servers.create')
        .controller('ServerCreateController', ServerCreateController);

    /** @ngInject */
    function ServerCreateController($http, $mdToast, $rootScope, $state) {
        var vm = this;

        // vm.creating = false;

        // Data
        vm.steps = {
            step1: {},
            step2: {},
            step3: {},
            step4: {},
            step5: {}
        };

        // Save Form Data
        vm.form = {};

        // STEP 1: Choose Provider
        vm.providers = [
            {
                "name": "aws",
                "card": {
                    "image": {
                        "src": "assets/images/cloud/providers/amazon_web_services.svg",
                        "alt": "Amazon Web Services (AWS)"
                    },
                    "button": {
                        "text": "SELECTED",
                        "className": "md-raised"
                    }
                },
                "config": [
                    {
                        "name": "aws_access_token",
                        "title": "AWS Access Token",
                        "type": "text",
                        "required": true
                    },
                    {
                        "name": "aws_secret_key",
                        "title": "AWS Secret Key",
                        "type": "text",
                        "required": true
                    },
                    {
                        "name": "aws_key_name",
                        "title": "AWS Key Name",
                        "type": "text",
                        "required": true
                    }
                ],
                "regions": [
                    {
                        "name": "us-east-1",
                        "title": "US East 1",
                        "image": {
                            "src": "assets/images/cloud/regions/us.svg",
                            "alt": "US East 1"
                        },
                    },
                    {
                        "name": "us-east-2",
                        "title": "US East 2",
                        "image": {
                            "src": "assets/images/cloud/regions/us.svg",
                            "alt": "US East 2"
                        },
                    },
                    {
                        "name": "ap-south-1",
                        "title": "AP South 1",
                        "image": {
                            "src": "assets/images/cloud/regions/in.svg",
                            "alt": "AP South 1"
                        },
                    },
                    {
                        "name": "ap-northeast-1",
                        "title": "AP Northeast 1",
                        "image": {
                            "src": "assets/images/cloud/regions/kr.svg",
                            "alt": "AP Northeast 1"
                        },
                    },
                    {
                        "name": "ap-southeast-1",
                        "title": "AP Southeast 1",
                        "image": {
                            "src": "assets/images/cloud/regions/sg.svg",
                            "alt": "AP Southeast 1"
                        },
                    },
                    {
                        "name": "sa-east-1",
                        "title": "SA East 1",
                        "image": {
                            "src": "assets/images/cloud/regions/br.svg",
                            "alt": "SA Northeast 1"
                        },
                    },
                    {
                        "name": "ca-central-1",
                        "title": "CA Central 1",
                        "image": {
                            "src": "assets/images/cloud/regions/ca.svg",
                            "alt": "CA Central 1"
                        },
                    },
                    {
                        "name": "eu-west-1",
                        "title": "EU West 1",
                        "image": {
                            "src": "assets/images/cloud/regions/uk.svg",
                            "alt": "EU West 1"
                        },
                    }
                ],
                "os": [
                    {
                        "name": "ubuntu-16-04",
                        "title": "Ubuntu 16.04",
                        "type": "ubuntu",
                        "version": "16.04",
                        "dist": "xenial",
                        "ssh_user": "ubuntu",
                        "image": {
                            "src": "assets/images/cloud/os/ubuntu.svg",
                            "alt": "Ubuntu 16.04"
                        },
                    },
                    {
                        "name": "centos-7",
                        "title": "CentOS 7",
                        "image": {
                            "src": "assets/images/cloud/os/centos.png",
                            "alt": "CentOS 7"
                        },
                    },
                    {
                        "name": "windows",
                        "title": "Windows",
                        "image": {
                            "src": "assets/images/cloud/os/windows.svg",
                            "alt": "Windows"
                        },
                    }
                ],
            },
            {
                "name": "microsoft",
                "card": {
                    "image": {
                        "src": "assets/images/cloud/providers/microsoft.svg",
                        "alt": "Microsoft Azure"
                    },
                    "button": {}
                }
            },
            {
                "name": "packet",
                "card": {
                    "image": {
                        "src": "assets/images/cloud/providers/packet.png",
                        "alt": "Packet.net"
                    }
                },
                "config": [
                    {
                        "name": "api_key",
                        "title": "Packet API Key",
                        "type": "text",
                        "required": true
                    }
                ]
            }
        ];
        vm.providerClicked = function (event, index, provider) {
            console.log('INDEX', index);
            console.log('PROVIDER', provider);
            console.log('EVENT', event);

            selectProvider(index);
        };
        vm.regionClicked = function (event, index, region) {
            selectRegion(index);
        };

        vm.osClicked = function (event, index, os) {
            selectOS(index);
        };

        vm.serverConfig = function() {
            return {
                "name": vm.form.name,
                "description": vm.form.description,
                "provider": {
                    "type": vm.form.type,
                    "config": {
                        "aws_access_token": vm.form.config.aws_access_token,
                        "aws_secret_key": vm.form.config.aws_secret_key,
                        "aws_region": vm.form.region,
                        "aws_key_name": vm.form.config.aws_key_name
                    }
                },
                "ssh": {
                    "public_key": "/Users/bradserbu/.ssh/id_rsa.pub",
                    "private_key": "/Users/bradserbu/.ssh/id_rsa"
                },
                "os": vm.form.os,
                "instance": {
                    "size": "t2.micro",
                    "cpu_cores": 1,
                    "memory": "1GB"
                },
                "network": {
                    "ingress": [
                        {
                            "incoming": 22,
                            "outgoing": 22
                        },
                        // NODUS AGENT PORT
                        {
                            "incoming": 8081,
                            "outgoing": 8081
                        }
                    ]
                }
            };
        };

        vm.createServer = function (event) {
            vm.creating = true;

            var config = vm.serverConfig();

            $rootScope.loadingProgress = true;
            return $http
                .put('/api/servers/create', {server: config})
                .then(function (response) {
                    console.log('SUCCESS_RESPONSE', response);

                    var server_id = response.data.data;

                    $state.go('app.servers.action', {id: server_id, action: 'deploy'});
                })
                .catch(function (response) {
                    console.log('ERROR_RESPONSE', response);
                    var error = response.data.error;

                    console.log('ERROR', error);
                    return $mdToast.show(
                        $mdToast.simple().textContent('ERROR: ' + error.message || error.statusText).position('top right').theme('error-toast')
                    );
                });
        };

        // Methods
        function selectProvider(index) {

            // Deselect Provider
            // if (vm.selectedProvider) {
            // vm.selectedProvider.card.button = 'SELECT';
            // }

            // Select Provider
            var provider = vm.providers[index];

            // Keep track of selected provider
            vm.selectedProvider = provider;
            vm.selectedIndex = index;

            // Update Form Data
            vm.form.type = provider.name;
            vm.form.config = {};
        }

        function selectRegion(index) {

            // Deselect Provider
            // if (vm.selectedProvider) {
            // vm.selectedProvider.card.button = 'SELECT';
            // }

            // Select Provider
            var region = vm.selectedProvider.regions[index];

            // Keep track of selected provider
            vm.selectedRegion = region;
            vm.selectedRegionIndex = index;

            // Update Form Data
            vm.form.region = region.name;
        }

        function selectOS(index) {
            // Select Provider
            var os = vm.selectedProvider.os[index];

            // Keep track of selected provider
            vm.selectedOS = os;
            vm.selectedOSIndex = index;

            // Update Form Data
            vm.form.os = os;

            /*
             "os": {
             "name": "ubuntu-16-04",
             "type": "ubuntu",
             "version": "16.04",
             "ssh_user": "ubuntu"
             },
             */
        }

    }
})();