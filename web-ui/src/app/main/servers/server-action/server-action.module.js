(function () {
    'use strict';

    angular
        .module('app.servers.action', [
            // 3rd Party Dependencies
            'ngWebSocket'
        ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider) {

        // State
        $stateProvider
            .state('app.servers.action', {
                url: '/:id/:action',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/servers/server-action/server-action.html',
                        controller: 'ServerActionController as vm'
                    }
                },
                resolve: {
                    ServerData: function ($stateParams, msApi) {
                        var id = $stateParams.id;
                        console.log("HOST", id);

                        return msApi
                            .resolve('servers.get@get', {id: id})
                            .then(function (response) {
                                console.log('RESPONSE', server);
                                var server = response.data;

                                console.log('SERVER', server);
                                return server;
                            });
                        // return msApi.resolve('servers.list@get').then(servers => );
                        // return eCommerceService.getProduct($stateParams.id);
                    }
                }
            });

        // msNavigationServiceProvider.saveItem('resources.server_details', {
        //     title    : 'Servers Details',
        //     icon     : 'icon-server',
        //     state    : 'app.server-details',
        //     /*stateParams: {
        //      'param1': 'page'
        //      },*/
        //     translate: 'SERVERS.NAV',
        //     weight   : 2
        // });
    }
})();
