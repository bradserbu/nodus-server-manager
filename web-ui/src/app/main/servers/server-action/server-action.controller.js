(function () {
    'use strict';

    angular
        .module('app.servers.action')
        .controller('ServerActionController', ServerActionController);

    /** @ngInject */
    function ServerActionController($websocket, $scope, $location, $state, ServerData, $stateParams) {

        console.log('PARAMS', $stateParams);

        var PackageName = $location.search()['packageName'];
        console.log('PACKAGE_NAME', PackageName);

        // PAGE VIEW MODEL
        var vm = this;

        vm.server = ServerData;
        vm.server.actions = {
            "deploy": {
                "path": 'ws://localhost:3000/api/servers/deploy?id=' + ServerData._id,
                "activities": {
                    "deploy-server": {
                        "title": "Deploy Server",
                        "status": "pending"
                    },
                    "install-os": {
                        "title": "Install OS",
                        "status": "pending"
                    },
                    "install-agent": {
                        "title": "Install Agent",
                        "status": "pending"
                    },
                    "restart-server": {
                        "title": "Start Server",
                        "status": "pending"
                    },
                    "server-online": {
                        "title": "Server Online",
                        "status": "pending",
                        "completed": function () {
                            // $state.go('app.servers_dashboard', {id: ServerData._id});
                            $state.go('app.servers');
                        }
                    }
                }
            },
            "destroy": {
                "path": 'ws://localhost:3000/api/servers/destroy?id=' + ServerData._id,
                "activities": {
                    "stop-services": {
                        "title": "Stop Services",
                        "status": "pending"
                    },
                    "shutdown-server": {
                        "title": "Shutdown Server",
                        "status": "pending"
                    },
                    "destroy-server": {
                        "title": "Destroy Server",
                        "status": "pending",
                        "completed": function () {
                            $state.go('app.servers');
                        }
                    }
                }
            },
            "install": {
                "path": 'ws://localhost:3000/api/servers/install?server_id=' + ServerData._id + '&packageName=' + PackageName,
                "activities": {
                    "install-deps": {
                        "title": "Dependencies",
                        "status": "pending"
                    },
                    "install-app": {
                        "title": "Install",
                        "status": "pending"
                    },
                    "configure-app": {
                        "title": "Configure",
                        "status": "pending"
                    },
                    "start-app": {
                        "title": "Start",
                        "status": "pending",
                        "completed": function () {
                            $state.go('app.servers');
                        }
                    }
                }
            }
        };

        console.log('SERVER_STATE', vm.server.status);

        //vm.action = $stateParams.action;
        vm.action = vm.server.actions[$stateParams.action];
        console.log('RUN_ACTION', vm.action);

        // Run Action based on server status
        runAction(vm.action);

        // UI Events
        vm.gotoServers = function gotoServers() {
            $state.go('app.servers');
        };

        function runAction(action) {
            console.log('ACTION', action);

            var socket = $websocket(action.path);
            console.log('SOCKET', socket);

            socket.onMessage(function (message) {
                console.log('SOCKET_MESSAGE', message);

                var payload = JSON.parse(message.data);
                console.log('PAYLOAD', payload);

                var event = payload[0];
                var data = payload[1];

                // ** Handle Server Events
                if (event === 'update-activity') {
                    console.log('Updating Activity...', data);

                    // Update Activities
                    $scope.$apply(function () {
                        console.log('UPDATE_ACTIVITY');
                        var activity = action.activities[data.name];

                        if (!activity) {
                            return console.error('ACTIVITY_NOT_FOUND', data.name);
                        }

                        activity.status = data.status;
                        activity.message = data.message;
                        activity.started_on = data.started_on;

                        if (activity.status === 'completed' && activity.completed) {
                            console.log('Firing complete handler...', activity);
                            activity.completed();
                        }
                    });
                }
            });
        }
    }
})();