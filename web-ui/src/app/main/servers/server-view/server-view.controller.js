(function () {
    'use strict';

    angular
        .module('app.servers.view')
        .controller('ServerViewController', ServerViewController);

    /** @ngInject */
    function ServerViewController($stateParams, $state, $mdDialog, ServerData, ServerStats, ServerApps, $document) {

        console.log('PARAMS', $stateParams);
        console.log('SERVER', ServerData);

        // PAGE VIEW MODEL
        var vm = this;


        vm.server = ServerData;

        vm.apps = ServerApps;
        vm.stats = ServerStats.stats;

        vm.page = {
            title: 'Server',
            icon: 'icon-server',
        };

        // Events
        vm.gotoServers = function() {
            $state.go('app.servers');
        };

        // Methods
        vm.Bytes = Bytes;

        // Show Install Package Dialog
        vm.installPackage = function() {
            $mdDialog.show({
                controller: 'InstallPackageDialogController',
                controllerAs: 'vm',
                templateUrl: 'app/main/servers/server-view/install-package/install-package.html',
                parent             : angular.element($document.find('#content-container')),
                // targetEvent        : ev,
                clickOutsideToClose: true,
                locals: {
                    ServerId: angular.copy(vm.server.id)
                }
            });
        }
    }

    function Bytes(bytes, precision) {
        if (bytes === 0) return '0 bytes';
        if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
        if (typeof precision === 'undefined') precision = 1;
        var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
            number = Math.floor(Math.log(bytes) / Math.log(1024));

        return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
    }
})();