(function () {
    'use strict';

    angular
        .module('app.servers.view', [
            // 3rd Party Dependencies
            'ngWebSocket'
        ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider) {

        // State
        $stateProvider
            .state('app.servers.view', {
                url: '/:id',
                params: {},
                views: {
                    'content@app': {
                        templateUrl: 'app/main/servers/server-view/server-view.html',
                        controller: 'ServerViewController as vm'
                    }
                },
                resolve: {
                    ServerData: function ($stateParams, msApi) {
                        var id = $stateParams.id;
                        console.log("HOST", id);

                        return msApi
                            .resolve('servers.get@get', {id: id})
                            .then(function (response) {
                                console.log('RESPONSE', response);
                                var server = response.data;

                                console.log('SERVER', server);
                                return server;
                            });
                        // return msApi.resolve('servers.list@get').then(servers => );
                        // return eCommerceService.getProduct($stateParams.id);
                    },
                    ServerApps: function ($stateParams, msApi) {
                        var id = $stateParams.id;
                        console.log("HOST", id);

                        return msApi
                            .resolve('servers.apps@get', {id: id})
                            .then(function (response) {
                                console.log('APPS_RESPONSE', response);
                                var apps = response.data;

                                console.log('APPS', apps);
                                return apps;
                            });
                        // return msApi.resolve('servers.list@get').then(servers => );
                        // return eCommerceService.getProduct($stateParams.id);
                    },
                    ServerStats: function ($stateParams, msApi) {
                        var id = $stateParams.id;
                        console.log("HOST", id);

                        return msApi
                            .resolve('server.stats@get', {id: id})
                            .then(function (response) {
                                console.log('RESPONSE', response);
                                var server = response.data;

                                console.log('SERVER_STATS', server);
                                return server;
                            });
                        // return msApi.resolve('servers.list@get').then(servers => );
                        // return eCommerceService.getProduct($stateParams.id);
                    }
                }
            });
    }
})();
