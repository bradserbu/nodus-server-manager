(function () {
    'use strict';

    angular
        .module('app.servers.view')
        .controller('InstallPackageDialogController', InstallPackageDialogController);

    /** @ngInject */
    function InstallPackageDialogController($mdDialog, $state, $location, ServerId) {
        var vm = this;

        vm.packages = [
            {
                "name": "nodejs",
                "title": "NodeJS",
                "version": "7.10.0",
                "card": {
                    "image": {
                        "src": "assets/images/cloud/packages/nodejs.png",
                        "alt": "NodeJS"
                    }
                }
            }
        ];

        vm.form = {};

        vm.closeDialog = closeDialog;
        vm.saveForm = function () {
            console.log(vm.form);

            // $state.go('app.servers.action', {id: ServerId, action: 'install', packageName: 'nodejs'});
            $mdDialog.hide();
            $location.url('/servers/' + ServerId + "/install?packageName=nodejs");
        };

        vm.providerClicked = function (event, index, provider) {
            console.log('INDEX', index);
            console.log('PROVIDER', provider);
            console.log('EVENT', event);

            selectProvider(index);
        };

        function selectProvider(index) {

            // Deselect Provider
            if (vm.selectedProvider) {
                vm.selectedProvider.card.button.text = 'SELECT';
            }

            // Select Provider
            var provider = vm.packages[index];

            // Keep track of selected provider
            vm.selectedProvider = provider;
            vm.selectedIndex = index;

            // Update Form Data
            vm.form.type = provider.name;
            vm.form.config = {};
        }

        //////////

        /**
         * Close dialog
         */
        function closeDialog() {
            $mdDialog.hide();
        }
    }
})();