(function () {
    'use strict';

    angular
        .module('app.servers', [
            // 3rd Party Dependencies
            'wipImageZoom',
            'datatables',
            'flow',
            'nvd3',
            'textAngular',
            'uiGmapgoogle-maps',
            'xeditable',
            'app.servers.create',
            'app.servers.action',
            'app.servers.view'
        ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider) {
        // State
        $stateProvider
            .state('app.servers', {
                url: '/servers',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/servers/servers.html',
                        controller: 'ServersController as vm'
                    }
                },
                resolve: {
                    ServersData: function (msApi) {
                        return msApi.resolve('servers@get');
                    },
                    Api: function (msApi) {
                        return {
                            getServers: function () {
                                return msApi.resolve('servers@get');
                            }
                        };
                    }
                }
            });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/servers');

        // Navigation
        msNavigationServiceProvider.saveItem('resources', {
            title: 'RESOURCES',
            group: true,
            weight: 2
        });

        msNavigationServiceProvider.saveItem('resources.servers', {
            title: 'Servers',
            icon: 'icon-server',
            state: 'app.servers',
            /*stateParams: {
             'param1': 'page'
             },*/
            translate: 'SERVERS.NAV',
            weight: 2
        });
    }
})();
