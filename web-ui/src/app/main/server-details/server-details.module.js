(function () {
    'use strict';

    angular
        .module('app.server-details', [
            // 3rd Party Dependencies
            'wipImageZoom',
            'datatables',
            'flow',
            'nvd3',
            'textAngular',
            'uiGmapgoogle-maps',
            'xeditable'
        ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider) {
        // State
        $stateProvider
            .state('app.server_details', {
                url: '/server/:id',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/server-details/server-details.html',
                        controller: 'ServerDetailsController as vm'
                    }
                },
                resolve: {
                    RequestedAction: function($stateParams) {
                        return $stateParams.action;
                    },
                    ServerData: function ($stateParams, msApi) {
                        var id = $stateParams.id;
                        console.log("HOST", name);

                        return msApi.resolve('servers.get@get', {id: id})
                            .then(function (response) {
                                var server = response.data;

                                console.log('SERVER', server);
                                return server;
                            });
                        // return msApi.resolve('servers.list@get').then(servers => );
                        // return eCommerceService.getProduct($stateParams.id);
                    }
                }
            });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/server-details');

        // msNavigationServiceProvider.saveItem('resources.server_details', {
        //     title    : 'Servers Details',
        //     icon     : 'icon-server',
        //     state    : 'app.server-details',
        //     /*stateParams: {
        //      'param1': 'page'
        //      },*/
        //     translate: 'SERVERS.NAV',
        //     weight   : 2
        // });
    }
})();
