(function () {
    'use strict';

    angular
        .module('app.servers-dashboard',
            [
                // 3rd Party Dependencies
                'nvd3',
                'datatables'
            ]
        )
        .config(config);

    /** @ngInject */
    function config($stateProvider, msApiProvider, msNavigationServiceProvider) {

        // State
        $stateProvider.state('app.servers_dashboard', {
            url: '/dashboard/:id',
            views: {
                'content@app': {
                    templateUrl: 'app/main/servers-dashboard/servers-dashboard.html',
                    controller: 'DashboardServersController as vm'
                }
            },
            resolve: {
                DashboardData: function (msApi) {
                    return msApi.resolve('servers.dashboard@get');
                },
                ServersData: function (msApi) {
                    return msApi.resolve('servers@get');
                },
                Api: function (msApi) {
                    return msApi;
                },
                ChartsData: function (msApi) {
                    return msApi.resolve('servers.charts@get');
                }
            },
            bodyClass: 'servers-dashboard'
        });

        msNavigationServiceProvider.saveItem('performance', {
            title: 'MONITOR',
            group: true,
            weight: 3
        });

        msNavigationServiceProvider.saveItem('performance.dashboard', {
            title: 'Dashboard',
            icon: 'icon-poll',
            state: 'app.servers_dashboard',
            // stateParams: {
            //     'ipAddress': '127.0.0.1'
            // },
            // translate: 'SERVICES.SERVICES_NAV',
            weight: 3
        });
    }

})();
