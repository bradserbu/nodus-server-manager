(function () {
    'use strict';

    angular.module('app.servers-dashboard').controller('DashboardServersController', DashboardServersController);

    function ChartFactory() {

        ///////////
        // CHARTS
        // function construct(constructor, args) {
        //     function F() {
        //         return constructor.apply(this, args);
        //     }
        //
        //     F.prototype = constructor.prototype;
        //     return new F();
        // }
        // function construct(Cls, args) {
        //     return new (Cls.bind.apply(Cls, arguments))();
        //     // or even
        //     // return new (Cls.bind.apply(Cls, arguments));
        //     // if you know that Cls.bind has not been overwritten
        // }
        var createSomething = function construct(constructor, args) {
            function F() {
                return constructor.apply(this, args);
            }

            F.prototype = constructor.prototype;
            return new F();
        };

        function createFunction(def) {

            console.log('CREATE_FUNCTION', def);

            // Create copy of def.args
            var funcArgs = def.args.slice();

            // Add def.body to func args
            funcArgs.push(def.body);

            // Create a new function
            // return construct(Function, funcArgs);
            var func = createSomething(Function, funcArgs);

            console.log('FUNCTION', func);
            return func;
        }

        return {
            LineChart: function createLineChart(chartDefinition) {
                console.log('CREATE_LINE_CHART', chartDefinition);

                var chartOptions = chartDefinition.options.chart;

                // Transform functions from options
                chartOptions.x = createFunction(chartOptions.x);
                chartOptions.y = createFunction(chartOptions.y);
                chartOptions.xAxis.tickFormat = createFunction(chartOptions.xAxis.tickFormat);
                chartOptions.yAxis.tickFormat = createFunction(chartOptions.yAxis.tickFormat);

                return {
                    title: chartDefinition.title,
                    chart: {
                        options: {
                            chart: chartOptions
                        },
                        data: chartDefinition.series
                    }
                };
            }
        }
    }

    function Bytes(bytes, precision) {
        if (bytes === 0) return '0 bytes';
        if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
        if (typeof precision === 'undefined') precision = 1;
        var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
            number = Math.floor(Math.log(bytes) / Math.log(1024));

        return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
    }

    function ServerById(servers, id) {

        for (var lcv = 0; lcv < servers.length; lcv++) {
            var server = servers[lcv];

            if (server.id == id)
                return server;
        }
    }

    /**
     * Used in Live charts to initialize a set of values
     * @param data
     * @returns {*}
     */
    function zeroValues(size, data) {
        data.forEach(function (series) {
            console.log('ZERO_VALUES', series);

            var values = series.values;

            for (var lcv = 1; lcv < size; lcv++) {
                values.push({x: lcv, y: 0});
            }
        });

        return data;
    }

    /** @ngInject */
    function DashboardServersController($state, $scope, $interval, DashboardData, ServersData, ChartsData, Api, ChartService) {

        ///////////
        // PAGE VM
        var Charts = ChartFactory();

        ///////////
        // Create widget 1 from chart data (as test)
        var memoryConsumptionChart = Charts.LineChart(ChartsData.data[0]);
        console.log('MEMORY_CONSUMPTION_CHART', memoryConsumptionChart);

        ///////////
        // VIEW MODEL
        var vm = this;

        vm.Bytes = Bytes;

        var UPDATE_STATS_INTERVAL = 1;

        //////////
        // Methods
        vm.selectServer = selectServer;

        //////////
        // Data
        vm.dashboardData = DashboardData.data;
        vm.servers = ServersData.data;
        vm.selectedServer = vm.servers[0];

        vm.serverId = $state.params.id || vm.selectedServer.id;

        // Widgets
        vm.storage_widget = vm.dashboardData.storage_widget;
        vm.memory_widget = vm.dashboardData.memory_widget;
        vm.network_transmitted = {
            title: vm.dashboardData.network_transmitted.title,
            value: vm.dashboardData.network_transmitted.value,
            detail: vm.dashboardData.network_transmitted.detail,
            chart: {
                config: {
                    refreshDataOnly: true,
                    deepWatchData: true,
                },
                options: {
                    chart: {
                        type: 'lineChart',
                        color: ['rgba(0, 0, 0, 0.27)'],
                        height: 50,
                        margin: {
                            top: 8,
                            right: 0,
                            bottom: 0,
                            left: 0,
                        },
                        duration: UPDATE_STATS_INTERVAL,
                        clipEdge: true,
                        interpolate: 'cardinal',
                        interactive: false,
                        isArea: true,
                        showLegend: false,
                        showControls: false,
                        showXAxis: false,
                        showYAxis: false,
                        x: function (d) {
                            return d.x;
                        },
                        y: function (d) {
                            return d.y;
                        }
                    },
                },
                data: vm.dashboardData.network_transmitted.chart,
            },
            init: function () {
                // Run this function once to initialize the widget

                // Grab the x value
                var lastIndex = vm.dashboardData.network_transmitted.chart[0].values.length - 1,
                    x = vm.dashboardData.network_transmitted.chart[0].values[lastIndex].x;

                /**
                 * Emulate constant data flow
                 *
                 * @param min
                 * @param max
                 */
                function updateNetTX(networkStats) {

                    console.log('UPDATE_NETWORK_STATS', networkStats);

                    if (isNaN(networkStats.tx_sec))
                        networkStats.tx_sec = 0;

                    // Increase the x value
                    x++;

                    var newValue = {
                        x: x,
                        y: networkStats.tx_sec
                    };

                    // vm.network_transmitted.chart.data[0].values.shift();
                    vm.network_transmitted.chart.data[0].values.push(newValue);

                    // Update Widget Text
                    vm.network_transmitted.value.bytes = networkStats.tx;
                    vm.network_transmitted.value.bytes_per_sec = networkStats.tx_sec;
                }

                // Set interval
                $scope.$on('network-stats', function (event, data) {
                    console.log('Updating Network Widgets...', data);
                    updateNetTX(data.network.stats);
                });
            }
        };
        vm.network_received = {
            title: vm.dashboardData.network_received.title,
            value: vm.dashboardData.network_received.value,
            detail: vm.dashboardData.network_received.detail,
            chart: {
                config: {
                    refreshDataOnly: true,
                    deepWatchData: true,
                },
                options: {
                    chart: {
                        type: 'lineChart',
                        color: ['rgba(0, 0, 0, 0.27)'],
                        height: 50,
                        margin: {
                            top: 8,
                            right: 0,
                            bottom: 0,
                            left: 0,
                        },
                        duration: 5,
                        clipEdge: true,
                        interpolate: 'cardinal',
                        interactive: false,
                        isArea: true,
                        showLegend: false,
                        showControls: false,
                        showXAxis: false,
                        showYAxis: false,
                        x: function (d) {
                            return d.x;
                        },
                        y: function (d) {
                            return d.y;
                        }
                        // yDomain: [0, 50],
                    },
                },
                data: vm.dashboardData.network_received.chart,
            },
            init: function () {
                // Run this function once to initialize the widget

                // Grab the x value
                var lastIndex = vm.dashboardData.network_received.chart[0].values.length -
                        1,
                    x = vm.dashboardData.network_received.chart[0].values[lastIndex].x;

                /**
                 * Emulate constant data flow
                 *
                 * @param min
                 * @param max
                 */
                function updateNetRX(networkStats) {

                    console.log('UPDATE_NETWORK_STATS', networkStats);

                    if (isNaN(networkStats.rx_sec))
                        networkStats.rx_sec = 0;

                    // Increase the x value
                    x++;

                    var newValue = {
                        x: x,
                        y: networkStats.rx_sec
                    };

                    // vm.network_received.chart.data[0].values.shift();
                    vm.network_received.chart.data[0].values.push(newValue);

                    // Update Widget Text
                    vm.network_received.value.bytes = networkStats.rx;
                    vm.network_received.value.bytes_per_sec = networkStats.rx_sec;
                }

                // Set interval
                $scope.$on('network-stats', function (event, data) {
                    console.log('Updating Network Widgets...', data);
                    updateNetRX(data.network.stats);
                });
            }
        };

        // Widget 5
        vm.widget5 = vm.dashboardData.widget5;

        // CPU (Live)
        vm.cpu_live = {
            title: vm.dashboardData.cpu_live.title,
            chart: {
                config: {
                    refreshDataOnly: true,
                    deepWatchData: true,
                },
                options: {
                    chart: {
                        type: 'lineChart',
                        color: ['#03A9F4'],
                        height: 140,
                        margin: {
                            top: 8,
                            right: 32,
                            bottom: 16,
                            left: 48,
                        },
                        duration: 1,
                        clipEdge: true,
                        clipVoronoi: false,
                        interpolate: 'cardinal',
                        isArea: true,
                        useInteractiveGuideline: true,
                        showLegend: false,
                        showControls: false,
                        x: function (d) {
                            return d.x;
                        },
                        y: function (d) {
                            return d.y;
                        },
                        yDomain: [0, 100],
                        xAxis: {
                            tickFormat: function (d) {
                                return d + ' sec.';
                            },
                            showMaxMin: false,
                        },
                        yAxis: {
                            tickFormat: function (d) {
                                return d + '%';
                            },
                        },
                        interactiveLayer: {
                            tooltip: {
                                gravity: 's',
                                classes: 'gravity-s',
                            },
                        },
                    },
                },
                data: zeroValues(vm.dashboardData.cpu_live.size, vm.dashboardData.cpu_live.chart),
            },
            init: function () {

                // Grab the x value
                var lastIndex = vm.dashboardData.cpu_live.chart[0].values.length -
                        1,
                    x = vm.dashboardData.cpu_live.chart[0].values[lastIndex].x;

                /**
                 * Emulate constant data flow
                 *
                 * @param min
                 * @param max
                 */
                function cpuTicker(load) {
                    // Increase the x value
                    x = x + UPDATE_STATS_INTERVAL;

                    var newValue = {
                        x: x,
                        // y: Math.floor(load / numCpu)
                        y: Math.floor(load)
                        // y: Math.floor(Math.random() * (max - min + 1)) + min,
                    };

                    console.log('UPDATE_CPU_LOAD', newValue);
                    vm.cpu_live.chart.data[0].values.shift();
                    vm.cpu_live.chart.data[0].values.push(newValue);
                }

                // Subscribe to updated-stats event
                $scope.$on('server-stats', function (event, data) {
                    console.log('Updating CPU Load Chart...', data);

                    var load = data.cpu.load.currentload;
                    // var numCpu = data.cpu.cores;

                    console.log('CPU_LOAD', load);
                    // console.log('NUM_CPU', numCpu);

                    cpuTicker(load);
                });
            },
        };

        vm.memory_live = {
            title: vm.dashboardData.memory_live.title,
            chart: {
                config: {
                    refreshDataOnly: true,
                    deepWatchData: true,
                },
                options: {
                    chart: {
                        type: 'lineChart',
                        isArea: false,
                        showLegend: false,
                        showControls: false,
                        color: [
                            "#4caf50",
                            "#3f51b5",
                            "#ff5722",
                            "#03A9F4"
                        ],
                        height: 200,
                        margin: {
                            top: 8,
                            right: 32,
                            bottom: 16,
                            left: 55,
                        },
                        duration: 1,
                        clipEdge: true,
                        clipVoronoi: false,
                        interpolate: 'cardinal',
                        useInteractiveGuideline: true,
                        x: function (d) {
                            return d.x;
                        },
                        y: function (d) {
                            return d.y;
                        },
                        xAxis: {
                            tickFormat: function (d) {
                                return d + ' sec.';
                            },
                            showMaxMin: false,
                        },
                        yAxis: {
                            tickFormat: function (d) {

                                if (d)
                                    return Bytes(d);

                                return d;
                            },
                        },
                        yDomain: [0, 100],
                        interactiveLayer: {
                            tooltip: {
                                gravity: 's',
                                classes: 'gravity-s',
                            },
                        }
                    },
                },
                data: zeroValues(vm.dashboardData.memory_live.size, vm.dashboardData.memory_live.chart),
            },
            init: function () {

                /**
                 * Emulate constant data flow
                 *
                 * @param min
                 * @param max
                 */
                function updateChart(seriesIndex, yValue) {

                    var maxValue = vm.memory_live.chart.maxValue || 0;
                    var yDomain = vm.memory_live.chart.options.chart.yDomain;
                    var values = vm.memory_live.chart.data[seriesIndex].values;

                    // Grab the x value
                    var lastIndex = values.length - 1,
                        x = values[lastIndex].x;

                    // Increase the x value
                    x = x + UPDATE_STATS_INTERVAL;

                    var point = {
                        x: x,
                        y: yValue
                    };

                    // Update Y Domain
                    if (yDomain && yValue >= maxValue) {
                        console.log('MEMORY_MAX_VALUE', yValue);
                        vm.memory_live.chart.maxValue = yValue;
                        yDomain[1] = yValue * 1.25;
                    }

                    console.log('UPDATE_MEMORY_LIVE', point);

                    // Add new data point to the list of values
                    values.push(point);
                    values.shift();
                }

                // Subscribe to updated-stats event
                $scope.$on('server-stats', function (event, data) {
                    console.log('Updating Memory Live Chart...', data);

                    var total = data.memory.total;
                    var available = data.memory.available;
                    var used = data.memory.used;
                    var swapUsed = data.memory.swapused;

                    console.log('CHART', vm.memory_live.chart);

                    updateChart(0, total);
                    updateChart(1, available);
                    updateChart(2, used);
                    updateChart(3, swapUsed);
                });
            },
        };

        // Widget 7
        vm.widget7 = {
            title: vm.dashboardData.widget7.title,
            table: vm.dashboardData.widget7.table,
            dtOptions: {
                dom: '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
                pagingType: 'simple',
                pageLength: 10,
                lengthMenu: [10, 20, 50, 100],
                autoWidth: false,
                responsive: true,
                columnDefs: [
                    {
                        width: '20%',
                        targets: [0, 1, 2, 3, 4],
                    },
                ],
                columns: [
                    {},
                    {},
                    {
                        render: function (data, type) {
                            if (type === 'display') {
                                return data + ' KB/s';
                            }
                            else {
                                return data;
                            }
                        },
                    },
                    {
                        render: function (data, type) {
                            if (type === 'display') {
                                return data + '%';
                            }
                            else {
                                return data;
                            }
                        },
                    },
                    {
                        render: function (data, type) {
                            if (type === 'display') {
                                var el = angular.element(data);
                                el.html(el.text() + ' MB');

                                return el[0].outerHTML;
                            }
                            else {
                                return data;
                            }
                        },
                    },
                ],
            },
        };

        // Widget 8
        vm.widget8 = vm.dashboardData.widget8;

        // Methods

        //////////

        // Init Live Widgets
        vm.cpu_live.init();
        vm.memory_live.init();
        vm.network_transmitted.init();
        vm.network_received.init();

        function UpdateStorage(stats) {

            // Update storage widget
            var disks = stats.fs.size;
            console.log('DISKS', disks);

            var totalSize = 0;
            var totalUsed = 0;
            var numDisks = 0;

            // Calculate totals for storage consumption
            disks.forEach(function (disk) {
                totalSize += disk.size;
                totalUsed += disk.used;
                numDisks++;
            });

            var totalPercentage = (totalUsed / totalSize * 100).toFixed(2);
            var data = {
                used: Bytes(totalUsed),
                total: Bytes(totalSize),
                percentage: totalPercentage
            };

            console.log('STORAGE_DATA', data);

            // Update Storage Widget Data
            vm.storage_widget.value = data;
        }

        function UpdateMemory(stats) {
            var data = {
                used: stats.memory.used,
                total: stats.memory.total,
                percentage: (stats.memory.used / stats.memory.total * 100).toFixed(2)
            };

            console.log('MEMORY', data);
            vm.memory_widget.value = data;
        }

        function updateStats() {

            var id = vm.serverId;

            console.log('UPDATING_STATS', {
                id: id
            });

            return Api.resolve('server.stats@get', {
                id: id
            })
                .then(function (response) {

                    // Update server name
                    var svr = ServerById(vm.servers, id);
                    vm.serverName = svr.name;

                    var data = response.data;
                    console.log('SERVER_STATS', data);

                    // Update Storage Widget
                    UpdateStorage(data.stats);
                    UpdateMemory(data.stats);
                    ChartService.HideTooltips();

                    // Notify Live Charts they can update as well.
                    $scope.$emit('server-stats', data.stats);
                    $scope.$emit('network-stats', data.stats);

                    // ** Set Timeout to update network stats again
                    if (!cancel_timer)
                        setTimeout(updateStats, UPDATE_STATS_INTERVAL * 1000);
                });
        }

        // Cleanup
        var cancel_timer = false;
        $scope.$on('$destroy', function () {
            cancel_timer = true;
            ChartService.HideTooltips();
        });

        // Initialize Stats Updates
        // updateServerStats();
        // updateNetworkStats();
        updateStats();

        /**
         * Select project
         */
        function selectServer(server) {
            vm.selectedServer = server;

            cancel_timer = true;

            var id = server.id;
            $state.go('app.servers_dashboard', {id: id});
        }
    }
})
();
