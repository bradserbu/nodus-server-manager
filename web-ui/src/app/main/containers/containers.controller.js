(function() {
    'use strict';

    angular.module('app.containers').
        controller('ContainersController', ContainersController);

    /** @ngInject */
    function ContainersController($rootScope, $scope, $state, ContainersData, Api) {
        // Load View Model
        var vm = this;

        console.log('CONTAINERS_DATA', ContainersData);
        vm.containers = ContainersData.data;
        vm.dtInstance = {};
        vm.dtOptions = {
            dom: 'rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
            columnDefs: [
                {
                    // Target the 'success' column
                    targets: 0,
                    filterable: false,
                    width: '20px',
                    render: function(code, type) {
                        if (type === 'display') {
                            if (code === '200') {
                                return '<i class="icon-checkbox-marked-circle green-500-fg"></i>';
                            }

                            return '<i class="icon-cancel red-500-fg"></i>';
                        }

                        if (type === 'filter') {
                            if (code) {
                                return '1';
                            }

                            return '0';
                        }

                        return code;
                    },
                },
                {
                    // Target the instances column
                    targets: 1,
                    width: '20px',
                    // filterable: false,
                    // sortable  : false,
                    // child: true
                },
                {
                    // Target the instances column
                    targets: 2,
                    width: '20px',
                    // filterable: false,
                    // sortable  : false,
                    // child: true
                },
                {
                    // Target the instances column
                    targets: 3,
                    // width: '20px',
                    // filterable: false,
                    // sortable  : false,
                    // child: true
                },
                {
                    // Target the instances column
                    targets: 4,
                    // width: '20px',
                    // filterable: false,
                    // sortable  : false,
                    // child: true,
                    // type: 'datetime',
                    // format: 'LLL',
                    render: function(data, type) {
                        if (type === 'display') {
                            var date = new Date(data);
                            return moment(data).format('YYYY/MM/DD HH:mm:ss');
                        }

                        return data;
                    }
                },
                {
                    targets: 5,
                    render: function(data, type) {
                        if (type === 'display')
                            return Math.floor(data) + ' ms';
                        return data;
                    }
                },
                {
                    // Target the actions column
                    targets: 6,
                    // width: '50px',
                    responsivePriority: 1,
                    filterable: false,
                    sortable: false,
                },
            ],
            initComplete: function() {
                var api = this.api(),
                    searchBox = angular.element('body').
                        find('#containers-search');

                // Bind an external input as a table wide search box
                if (searchBox.length > 0) {
                    searchBox.on('keyup', function(event) {
                        api.search(event.target.value).draw();
                    });
                }
            },
            pagingType: 'simple',
            lengthMenu: [50, 100, 250, 500],
            pageLength: 100,
            scrollY: 'auto',
            responsive: true,
        };

        //////////
        // Methods
        vm.refreshData = refreshData;

        //////////
        function refreshData() {
            console.log('Refreshing data...');
            $rootScope.loadingProgress = true;
            Api.getContainers().then(function(response) {
                console.log('REFRESH_RESPONSE', response);
                vm.containers = response.data;
                vm.dtInstance.rerender();
                $rootScope.loadingProgress = false;
            });
        }
    }
})();
