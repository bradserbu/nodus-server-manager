(function ()
{
    'use strict';

    angular
        .module('app.containers', [
        // 3rd Party Dependencies
        'wipImageZoom',
        'datatables',
        'flow',
        'nvd3',
        'textAngular',
        'uiGmapgoogle-maps',
        'xeditable'
    ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.containers', {
                url      : '/containers',
                views    : {
                    'content@app': {
                        templateUrl: 'app/main/containers/containers.html',
                        controller : 'ContainersController as vm'
                    }
                },
                resolve  : {
                    ContainersData: function (msApi)
                    {
                        return msApi.resolve('containers@get');
                    },
                    Api: function(msApi) {
                        return {
                            getContainers: function () {
                                return msApi.resolve('containers@get');
                            }
                        };
                    }
                }
            });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/containers');

        // Api

        // Navigation
        // msNavigationServiceProvider.saveItem('app', {
        //     title : 'SAMPLE',
        //     group : true,
        //     weight: 1
        // });

        msNavigationServiceProvider.saveItem('resources.containers', {
            title    : 'Containers',
            icon     : 'icon-blur-linear',
            state    : 'app.containers',
            /*stateParams: {
                'param1': 'page'
             },*/
            translate: 'CONTAINERS.NAV',
            weight   : 2
        });

        msNavigationServiceProvider.saveItem('resources.storage', {
            title    : 'Storage',
            icon     : 'icon-harddisk',
            state    : 'app.containers',
            /*stateParams: {
             'param1': 'page'
             },*/
            // translate: 'CONTAINERS.NAV',
            weight   : 3
        });

        msNavigationServiceProvider.saveItem('resources.network', {
            title    : 'Network',
            icon     : 'icon-server-network',
            state    : 'app.containers',
            /*stateParams: {
             'param1': 'page'
             },*/
            // translate: 'CONTAINERS.NAV',
            weight   : 4
        });
    }
})();
