(function () {
    'use strict';

    angular
        .module('app.clusters')
        .controller('CreateClusterDialogController', CreateClusterDialogController);

    /** @ngInject */
    function CreateClusterDialogController($mdDialog, $http, $mdToast) {
        var vm = this;

        vm.form = {};

        // Select Providers
        vm.providers = [
            {
                "name": "aws",
                "card": {
                    "image": {
                        "src": "assets/images/cloud/providers/amazon_web_services.svg",
                        "alt": "Amazon Web Services (AWS)"
                    },
                    "button": {
                        "text": "SELECTED",
                        "className": "md-raised"
                    }
                },
                "form": [
                    {
                        "name": "aws_access_token",
                        "title": "AWS Access Token",
                        "type": "text",
                        "required": true
                    },
                    {
                        "name": "aws_secret_key",
                        "title": "AWS Secret Key",
                        "type": "text",
                        "required": true
                    },
                    {
                        "name": "aws_key_name",
                        "title": "AWS Key Pair",
                        "type": "text",
                        "required": true
                    }
                ]
            },
            {
                "name": "microsoft",
                "card": {
                    "image": {
                        "src": "assets/images/cloud/providers/microsoft.svg",
                        "alt": "Microsoft Azure"
                    },
                    "button": {}
                }
            },
            {
                "name": "packet",
                "card": {
                    "image": {
                        "src": "assets/images/cloud/providers/packet.png",
                        "alt": "Packet.net"
                    },
                    "button": {}
                },
                "form": [
                    {
                        "name": "api_key",
                        "title": "Packet API Key",
                        "type": "text",
                        "required": true
                    }
                ]
            }
        ];

        // Methods
        vm.providerClicked = function (event, index, provider) {
            console.log('INDEX', index);
            console.log('PROVIDER', provider);
            console.log('EVENT', event);

            selectProvider(index);
        };
        vm.closeDialog = closeDialog;
        vm.saveForm = function() {
            return createCluster(vm.form);
        };

        //////////

        function selectProvider(index) {

            // Deselect Provider
            if (vm.selectedProvider) {
                vm.selectedProvider.card.button.text = 'SELECT';
            }

            // Select Provider
            var provider = vm.providers[index];

            // Keep track of selected provider
            vm.selectedProvider = provider;
            vm.selectedIndex = index;

            // Update Form Data
            vm.form.provider = provider.name;
            vm.form.config = {};
        }

        /**
         * Close dialog
         */
        function closeDialog() {
            $mdDialog.hide();
        }

        function createCluster(config) {
            console.log('CREATE_CLUSTER', config);

            return $http
                .put('/api/clusters/create', {cluster: config})
                .then(function (response) {
                    console.log('SUCCESS_RESPONSE', response);

                    var cluster_id = response.data.data;

                    console.log('CLUSTER_ID', cluster_id);

                    // $state.go('app.clusters.action', {id: server_id, action: 'deploy'});
                    closeDialog();
                })
                .catch(function (response) {
                    console.log('ERROR_RESPONSE', response);
                    var error = response.data.error;

                    console.log('ERROR', error);
                    return $mdToast.show(
                        $mdToast.simple().textContent('ERROR: ' + error.message || error.statusText).position('top right').theme('error-toast')
                    );
                });
        }

    }
})();