(function () {
    'use strict';

    angular.module('app.clusters').controller('ClustersController', ClustersController);

    /** @ngInject */
    function ClustersController($rootScope, $scope, $state, $mdDialog, $document, ClustersData, Api) {
        // Load View Model
        var vm = this;

        // TODO: Move Colored Columns to it's on controller/factory
        var DEFAULT_COLORS = [
            "light-blue",
            "deep-orange",
            "indigo",
            "green",
            "teal",
            "grey"
        ];

        function LabelClass(color, value) {
            return '<span class="' + "text-boxed m-0 " + color + "-bg white-fg" + '">' + value + "</span>";
        }

        function ColoredLabels(colors) {

            colors = colors || DEFAULT_COLORS;

            var next_index = 0;
            var labels = {};

            return function (value) {
                if (!labels.hasOwnProperty(value)) {
                    // Assign a new color
                    var color = colors[next_index++];
                    // labels[value] = ;
                    labels[value] = LabelClass(color, value);

                    return labels[value];
                } else {
                    // Use existing color
                    return labels[value];
                }
            }
        }

        function TableOptions(tableDefinition) {
            var columnDefs = [];

            var numColumns = tableDefinition.columns.length;

            for (var index = 0; index < numColumns; index++) {
                var col = tableDefinition.columns[index];

                var colDef = {
                    "targets": index,
                    "sortable": col.hasOwnProperty("sortable") ? col.sortable : true,
                    "filterable": col.hasOwnProperty("filterable") ? col.filterable : true,
                };

                // if (col.title)
                //     colDef.title = col.title;

                if (col.width)
                    colDef.width = col.width;

                // Render options
                switch (col.type) {
                    case "icon":
                        var icon = col.config.icon;
                        colDef.render = function (data) {
                            return '<i class="' + icon + '"></i>'
                        };
                        break;
                    case "label":
                        var labelClass = col.config.labelClass;
                        colDef.render = function (data) {
                            return LabelClass(labelClass, data);
                        };
                        break;
                    case "tags":
                        colDef.render = function (data, type, row, meta) {
                            console.log('ARGUMENTS', arguments);
                            console.log('TYPE', type);

                            if (data == '')
                                return data;

                            var tags = data.split(',');

                            if (type == 'display') {
                                // Display tags as colored labels
                                var result = [];
                                for (var lcv = 0; lcv < tags.length; lcv++) {
                                    var tag = tags[lcv];

                                    //return '<span class="' + classes + "'>" + tag + "</span>";
                                    result.push(tagLabels(tag));
                                }

                                console.log('DATA', data);

                                return result.join(' ');
                            }

                            return data;
                        };
                        break;
                    default:
                        break;
                }

                columnDefs.push(colDef);
            }

            // Add the Actions Column
            columnDefs.push({
                targets: numColumns,
                responsivePriority: 1,
                filterable: false,
                sortable: false,
            });

            return {
                dom: 'rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
                columnDefs: columnDefs,
                initComplete: function () {

                    if (tableDefinition.searchElement) {

                        var api = this.api(),
                            searchBox = angular.element('body').find('#' + tableDefinition.searchElement);

                        // Bind an external input as a table wide search box
                        if (searchBox.length > 0) {
                            searchBox.on('keyup', function (event) {
                                api.search(event.target.value).draw();
                            });
                        }
                    }
                },
                pagingType: 'simple',
                lengthMenu: [50, 100, 250, 500],
                pageLength: 100,
                scrollY: 'auto',
                responsive: true,
            };
        }

        /// Label Controllers for Tags and OS columns
        var tagLabels = ColoredLabels();

        vm.table = {
            "searchElement": "servers-search",
            "columns": [
                {
                    "type": "icon",
                    "width": "10px",
                    "sortable": false,
                    "filterable": false,
                    "config": {
                        "icon": "icon-grid s16"
                    }
                },
                {
                    "name": "name",
                    "title": "Name",
                    "type": "label",
                    "config": {
                        "labelClass": "grey"
                    }
                },
                {
                    "name": "status",
                    "title": "Status"
                },
                {
                    "name": "nodes",
                    "title": "Nodes"
                },
                {
                    "name": "services",
                    "title": "Services"
                },
                {
                    "name": "tags",
                    "title": "Tags",
                    "type": "tags"
                }
            ]
        };

        console.log('CLUSTERS_DATA', ClustersData);
        vm.servers = ClustersData.data;
        vm.dtInstance = {};
        vm.dtOptions = TableOptions(vm.table);

        //////////
        // Methods
        vm.refreshData = refreshData;
        vm.gotoServerDetail = gotoServerDetail;
        vm.createCluster = createCluster;

        //////////
        function refreshData() {
            console.log('Refreshing data...');
            $rootScope.loadingProgress = true;
            Api.getClusters().then(function (response) {
                console.log('REFRESH_RESPONSE', response);
                vm.servers = response.data;
                vm.dtInstance.rerender();
                $rootScope.loadingProgress = false;
            });
        }

        function createCluster() {
            $mdDialog.show({
                controller: 'CreateClusterDialogController',
                controllerAs: 'vm',
                templateUrl: 'app/main/clusters/create-cluster/create-cluster.html',
                parent: angular.element($document.find('#content-container')),
                targetEvent: event,
                clickOutsideToClose: true,
                fullscreen: true,
                locals: {

                }
            });
        }

        /**
         * Go to product detail
         *
         * @param id
         */
        function gotoServerDetail(id) {
            console.log('SERVER_ID', id);
            $state.go('app.cluster', {id: id});
        }
    }
})();
