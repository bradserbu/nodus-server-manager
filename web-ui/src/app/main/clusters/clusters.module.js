(function ()
{
    'use strict';

    angular
        .module('app.clusters', [
        // 3rd Party Dependencies
        'wipImageZoom',
        'datatables',
        'flow'
    ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.clusters', {
                url      : '/clusters',
                views    : {
                    'content@app': {
                        templateUrl: 'app/main/clusters/clusters.html',
                        controller : 'ClustersController as vm'
                    }
                },
                resolve  : {
                    ClustersData: function (msApi)
                    {
                        return msApi.resolve('clusters@get');
                    },
                    Api: function(msApi) {
                        return {
                            getClusters: function () {
                                return msApi.resolve('clusters@get');
                            },
                            createCluster: function () {
                                return msApi.resolve('clusters.create@post');
                            }
                        };
                    }
                }
            });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/clusters');

        // Navigation
        msNavigationServiceProvider.saveItem('resources', {
            title : 'MANAGE',
            group : true,
            weight: 2
        });

        msNavigationServiceProvider.saveItem('resources.clusters', {
            title    : 'Clusters',
            icon     : 'icon-grid',
            state    : 'app.clusters',
            translate: 'CLUSTERS.NAV',
            weight   : 2
        });
    }
})();
