(function ()
{
    'use strict';

    angular
        .module('app.application-dashboard',
            [
                // 3rd Party Dependencies
                'nvd3'
            ]
        )
        .config(config);

    /** @ngInject */
    function config($stateProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider.state('app.application_dashboard', {
            url      : '/application-dashboard',
            views    : {
                'content@app': {
                    templateUrl: 'app/main/application-dashboard/application-dashboard.html',
                    controller : 'ApplicationDashboardController as vm'
                }
            },
            resolve  : {
                DashboardData: function (msApi)
                {
                    return msApi.resolve('dashboard.analytics@get');
                },
                ApplicationsData: function (msApi) {
                    return msApi.resolve('applications@get', ['/api/applications'])
                }
            },
            bodyClass: 'dashboard-application'
        });

        // Api
        msApiProvider.register('dashboard.analytics', ['app/data/dashboard/analytics/data.json']);

        // Api
        msApiProvider.register('applications', ['/api/applications']);
        // msApiProvider.register('servers.dashboard', ['/api/servers/dashboard']);
        // msApiProvider.register('services.dashboard.stats', ['/api/stats']);
        // msApiProvider.register('stats.server', ['/api/stats/server']);
        // msApiProvider.register('stats.network', ['/api/stats/network']);

        msNavigationServiceProvider.saveItem('application-dashboard', {
            title    : 'Applications',
            icon  : 'icon-apps',
            state    : 'app.application_dashboard',
            /*stateParams: {
             'param1': 'page'
             },*/
            // translate: 'SERVICES.SERVICES_NAV',
            weight   : 1
        });
    }

})();
