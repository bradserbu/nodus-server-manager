(function () {
    'use strict';

    angular
        .module('app.charts')
        .controller('ViewChartDialogController', ViewChartDialogController);

    /** @ngInject */
    function ViewChartDialogController($mdDialog, Chart, ChartService) {

        var vm = this;

        // Data
        vm.chart = Chart;
        vm.closeDialog = closeDialog;

        ///////////
        ChartService.CreateChart(Chart).then(function(chart) {
            vm.widget1 = chart;
        });
        // console.log('VIEW_CHART', widget1);

        // Request the chart data
        // widget1.getData().then(function() {
        //     console.log("Request for chart data completed.");
        // });

        /**
         * Close dialog
         */
        function closeDialog() {

            $mdDialog.hide();
        }
    }
})();
