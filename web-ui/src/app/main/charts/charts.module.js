(function ()
{
    'use strict';

    angular
        .module('app.charts', [
        // 3rd Party Dependencies
        'datatables',
        'nvd3',
    ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.charts', {
                url      : '/charts',
                views    : {
                    'content@app': {
                        templateUrl: 'app/main/charts/charts.html',
                        controller : 'ChartsController as vm'
                    }
                },
                resolve  : {
                    ChartsData: function (msApi)
                    {
                        return msApi.resolve('charts@get');
                    }
                }
            });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/charts');

        // Navigation
        // msNavigationServiceProvider.saveItem('app', {
        //     title : 'SAMPLE',
        //     group : true,
        //     weight: 1
        // });

        msNavigationServiceProvider.saveItem('performance.charts', {
            title    : 'Charts',
            icon     : 'icon-chart-pie',
            state    : 'app.charts',
            /*stateParams: {
                'param1': 'page'
             },*/
            translate: 'CHARTS.NAV',
            weight   : 5
        });
    }
})();
