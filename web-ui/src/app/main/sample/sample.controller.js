(function () {
    'use strict';

    angular
        .module('app.sample')
        .controller('SampleController', SampleController);

    /** @ngInject */
    function SampleController(SampleData, ChartService) {
        var vm = this;

        // Data
        vm.helloText = SampleData.data.helloText;

        // Methods
        var chartId = "fb32448e-4df8-47a5-8e4a-45f3f6820203";
        ChartService
            .GetChart(chartId)
            .then(function (Chart) {
                console.log("CHART", Chart);
                return ChartService
                    .CreateChart(Chart)
                    .then(function (chart) {
                        console.log("CHART_INSTANCE", chart);
                        vm.chartInstance = chart;
                    });
            });


        //////////
    }
})();
