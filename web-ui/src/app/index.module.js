(function() {
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular.module('fuse', [

        'uiGmapgoogle-maps',
        'textAngular',
        'xeditable',
        'ngStorage',
        'angular-jwt',

        // Core
        'app.core',

        // Navigation
        'app.navigation',

        // Toolbar
        'app.toolbar',

        // Quick Panel
        'app.quick-panel',

        // Sample
        // 'app.sample',
        'app.home',
        'app.clusters',
        'app.servers',
        'app.server-details',
        'app.containers',
        'app.charts',
        'app.reports',
        'app.servers-dashboard',

        // Login/Authentication
        'app.auth.login',
        'app.auth.register',

        // Load Admin Module
        'app.admin'
    ]);
})();
