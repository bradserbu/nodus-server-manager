(function () {
    'use strict';

    angular
        .module('fuse')
        .factory('FunctionService', FunctionService);

    /** @ngInject */
    function FunctionService($http, $q, $localStorage, $window, jwtHelper, msApi) {
        // ** Constants
        var DEFAULT_WIDTH = 600;

        var service = {};

        // ** Methods
        service.Create = CreateFunction;

        return service;

        function construct(type, args) {
            function F() {
                return type.apply(this, args);
            }

            F.prototype = type.prototype;
            return new F();
        }

        function CreateFunction(def) {

            console.log('CREATE_FUNCTION', def);

            // Create copy of def.args
            var funcArgs = def.args.slice();

            // Add def.body to func args
            funcArgs.push(def.body);

            // Create a new function
            // return construct(Function, funcArgs);
            // const func = new Function(def.args.join(','), def.body);
            var func = construct(Function, funcArgs);

            console.log('FUNCTION', func);
            return func;
        }
    }
})();
