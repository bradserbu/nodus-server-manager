(function () {
    'use strict';

    angular
        .module('fuse')
        .factory('UserService', UserService);

    /** @ngInject */
    function UserService($http, $localStorage, jwtHelper, $q) {
        var service = {};

        service.CurrentUser = CurrentUser();
        service.CurrentUserPromise = CurrentUserPromise;
        //service.Logout = Logout;

        return service;

        function CurrentUser() {

            var currentUser = $localStorage.currentUser;
            if (!currentUser)
                return;

            var token = $localStorage.currentUser.token;
            var tokenPayload = jwtHelper.decodeToken(token);

            var user = tokenPayload.user;
            var userEmail = user.email;
            console.log('USER_EMAIL', userEmail);
            console.log('PROFILE_IMAGE', user.profileImage);

            // Return the Current User Information from the payload
            return tokenPayload.user;
        }

        function CurrentUserPromise() {
            return $q.when(CurrentUser());
        }
    }
})();
