(function () {
    'use strict';

    // ** Constants
    var DEFAULT_WIDTH = 600;

    angular
        .module('fuse')
        .factory('ChartService', ChartService);

    /** @ngInject */
    function ChartService(msApi, FunctionService) {

        var service = {};

        // ** Methods
        service.GetChart = GetChart;
        service.CreateChart = CreateChart;
        service.HideTooltips = HideTooltips;

        return service;

        function HideTooltips() {
            // ** Ensure no chart tooltips are still showing...
            // (see: https://github.com/krispo/angular-nvd3/issues/427)
            d3.selectAll('.nvtooltip').style('opacity', '0');
        }

        function GetChart(id) {
            return msApi.request("charts.get@get", {
                id: id
            }).then(function (response) {
                return response.data
            });
        }

        function CreateChart(chartDefinition) {
            console.log('CREATE_LINE_CHART', chartDefinition);

            var chartOptions = chartDefinition.options.chart;

            // Transform functions from options
            if (chartOptions.x)
                chartOptions.x = FunctionService.Create(chartOptions.x);
            if (chartOptions.y)
                chartOptions.y = FunctionService.Create(chartOptions.y);
            if (chartOptions.xAxis && chartOptions.xAxis.tickFormat)
                chartOptions.xAxis.tickFormat = FunctionService.Create(
                    chartOptions.xAxis.tickFormat);
            if (chartOptions.yAxis && chartOptions.yAxis.tickFormat)
                chartOptions.yAxis.tickFormat = FunctionService.Create(
                    chartOptions.yAxis.tickFormat);

            if (chartOptions.tooltip && chartOptions.tooltip.headerFormatter)
                chartOptions.tooltip.headerFormatter = FunctionService.Create(
                    chartOptions.tooltip.headerFormatter);

            // Set width of chart, if it doesn't have one defined
            if (!chartOptions.width)
                chartOptions.width = DEFAULT_WIDTH;

            // Store the id of the chart to get data
            var id = chartDefinition.id;
            console.log("CHART_ID:", id);

            // Store the chart reference so we can update the data on getData()
            var chart = {
                options: {
                    chart: chartOptions,
                }
            };

            return msApi
                .request('charts.series@get', {
                    id: id
                })
                .then(function (response) {

                    chart.data = response.data;
                    return {
                        title: chartDefinition.title,
                        chart: chart
                    }
                });
        }
    }
})();
