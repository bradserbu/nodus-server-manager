(function() {
    'use strict';

    angular
        .module('fuse')
        .factory('AuthService', AuthService);

    /** @ngInject */
    function AuthService($http, $q, $localStorage, $window, jwtHelper, msApi) {
        var service = {};

        service.IsAuthenticated = IsAuthenticated;
        service.Login = Login;
        service.Logout = Logout;

        return service;

        function validateToken(token) {
            if (!token) {
                console.error('TOKEN_NOT_FOUND');
                return false;
            }

            // Ensure token is not Expired
            // if (jwtHelper.isTokenExpired(token)) {
            //     console.error('TOKEN_EXPIRED');
            //     return false;
            // }

            var tokenPayload = jwtHelper.decodeToken(token);
            console.log('TOKEN_PAYLOAD', tokenPayload);

            console.log('TOKEN_IS_VALID', token);
            return true;
        }

        function IsAuthenticated() {
            var currentUser = $localStorage.currentUser;

            // No Current User information in local storage
            if (!currentUser) {
                console.error('No currentUser information in localstorage...');
                return false;
            }

            var token = currentUser.token;
            if (!validateToken(token)) {
                console.error('Auth token is invalid...');
                return false;
            }

            return true;
        }

        function Login(username, password) {

            console.log('LOGIN', {
                username: username,
                password: password,
            });

            // Create a new deferred object
            var deferred = $q.defer();

            msApi.request('auth.login@get', {
                    username: username,
                    password: password // TODO: Hash Password
                },

                // SUCCESS
                function(response) {
                    console.log('LOGIN_RESPONSE', response);

                    var token = response.data.token;

                    console.log('TOKEN', token);

                    // store username and token in local storage to keep user logged in between page refreshes
                    $localStorage.currentUser = {
                        username: username,
                        token: token,
                    };

                    // add jwt token to auth header for all requests made by the $http service
                    $http.defaults.headers.common.Authorization = 'Bearer ' + response.token;

                    // Resolve the promise
                    deferred.resolve();
                },

                // ERROR
                function(response) {
                    console.error('ERROR_RESPONSE', response);

                    var error = response.data.error;

                    // Reject the promise
                    deferred.reject(error);
                }
            );

            return deferred.promise;

        }

        function Logout() {

            console.log(
                'Removing authentication information from local storage...');
            delete $localStorage.currentUser;

            // ** Redirect user to login page
            $window.location.href = '/login';

            // var token = $localStorage.currentUser;
            // var tokenPayload = jwtHelper.decodeToken(token);
            // console.log('TOKEN_PAYLOAD', tokenPayload);

            // return tokenPayload.user;
        }
    }
})();
