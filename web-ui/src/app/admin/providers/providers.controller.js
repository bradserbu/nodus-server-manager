(function () {
    'use strict';

    angular
        .module('app.admin.providers')
        .controller('ProvidersController', ProvidersController);

    /** @ngInject */
    function ProvidersController($mdDialog, $mdSidenav, $document, $scope, msUtils, Contacts, User, Providers) {

        // View Model
        var vm = this;

        // Page
        vm.page = {
            title: 'Providers',
            icon: 'icon-cloud',
        };

        // Current User
        vm.user = User.data;

        // List View
        vm.list = ListView({
            itemType: 'Users',
            items: Contacts.data,
            groups: Providers.data.groups,
            filterIds: null,
            listType: 'all',
            listOrder: 'name',
            listOrderAsc: false,
            selectedItems: [],
            newGroupName: '',
            onViewItem: function (event, item) {
                $mdDialog.show({
                    controller: 'AddProviderDialogController',
                    controllerAs: 'vm',
                    templateUrl: 'app/admin/providers/dialogs/add-provider/add-provider.html',
                    parent: angular.element($document.find('#content-container')),
                    targetEvent: event,
                    clickOutsideToClose: true,
                    fullscreen: true,
                    locals: {
                        Contact: item,
                        User: vm.user,
                        Contacts: vm.list.items
                    }
                });
            }
        });

        // TODO: Move to Shared 'ListView' Factory/Module
        function ListView(options) {

            var list = {};

            // Data
            list.items = options.items || [];
            list.groups = options.groups || {};
            list.filterIds = options.filterIds || null;
            list.listType = options.listType || 'all';
            list.listOrder = options.listOrder || 'name';
            list.listOrderAsc = options.listOrderAsc || false;
            list.selectedItems = options.selectedItems || [];
            list.newGroupName = '';

            // Methods
            list.filterChange = filterChange;
            list.viewItemDialog = viewItemDialog;
            list.deleteItemConfirm = deleteItemConfirm;
            list.deleteItem = deleteItem;
            list.deleteSelectedItems = deleteSelectedItems;
            list.toggleSelectItem = toggleSelectItem;
            list.deselectItems = deselectItems;
            list.selectAllItems = selectAllItems;
            list.addNewGroup = addNewGroup;
            list.deleteGroup = deleteGroup;
            list.toggleSidenav = toggleSidenav;
            list.toggleInArray = msUtils.toggleInArray;
            list.exists = msUtils.exists;

            return list;

            //////////
            // METHODS
            //////////

            /**
             * Change List Filter
             * @param type
             */
            function filterChange(type) {

                console.log('FILTER_CHANGE:', type);

                list.listType = type;

                if (type === 'all') {
                    list.filterIds = null;
                }
                // Select by group
                else if (angular.isObject(type)) {
                    list.filterIds = type.itemIds;
                }

                list.selectedItems = [];
            }

            /**
             * Open view item dialog
             *
             * @param ev
             * @param item
             */
            function viewItemDialog(ev, item) {
                options.onViewItem(ev, item);
            }

            /**
             * Delete Item Confirmation Dialog
             */
            function deleteItemConfirm(item, ev) {
                var confirm = $mdDialog
                    .confirm()
                    .title('Are you sure want to delete this item?')
                    .htmlContent('<b>' + item.name + ' ' + item.lastName + '</b>' + ' will be deleted.')
                    .ariaLabel('delete item')
                    .targetEvent(ev)
                    .ok('OK')
                    .cancel('CANCEL');

                $mdDialog.show(confirm).then(function () {

                    deleteItem(item);
                    list.selectedItems = [];

                }, function () {

                });
            }

            /**
             * Delete Item
             */
            function deleteItem(item) {
                // TODO: Why doesn't this show confirmation
                list.items.splice(list.items.indexOf(item), 1);
            }

            /**
             * Delete Selected Contacts
             */
            function deleteSelectedItems(ev) {
                var confirm = $mdDialog.confirm()
                    .title('Are you sure want to delete the selected items?')
                    .htmlContent('<b>' + list.selectedItems.length + ' selected</b>' + ' will be deleted.')
                    .ariaLabel('delete items')
                    .targetEvent(ev)
                    .ok('OK')
                    .cancel('CANCEL');

                $mdDialog.show(confirm).then(function () {

                    list.selectedItems.forEach(function (item) {
                        deleteItem(item);
                    });

                    list.selectedItems = [];
                });

            }

            /**
             * Toggle selected status of the item
             *
             * @param item
             * @param event
             */
            function toggleSelectItem(item, event) {
                if (event) {
                    event.stopPropagation();
                }

                if (list.selectedItems.indexOf(item) > -1) {
                    list.selectedItems.splice(list.selectedItems.indexOf(item), 1);
                }
                else {
                    list.selectedItems.push(item);
                }
            }

            /**
             * Deselect items
             */
            function deselectItems() {
                list.selectedItems = [];
            }

            /**
             * Sselect all items
             */
            function selectAllItems() {
                list.selectedItems = $scope.filteredItems;
            }

            /**
             *
             */
            function addNewGroup() {
                if (list.newGroupName === '') {
                    return;
                }

                var newGroup = {
                    'id': msUtils.guidGenerator(),
                    'name': list.newGroupName,
                    'itemIds': []
                };

                list.groups.push(newGroup);
                list.newGroupName = '';
            }

            /**
             * Delete Group
             */
            function deleteGroup(ev) {
                var group = list.listType;

                var confirm = $mdDialog.confirm()
                    .title('Are you sure want to delete the group?')
                    .htmlContent('<b>' + group.name + '</b>' + ' will be deleted.')
                    .ariaLabel('delete group')
                    .targetEvent(ev)
                    .ok('OK')
                    .cancel('CANCEL');

                $mdDialog.show(confirm).then(function () {

                    list.groups.splice(list.groups.indexOf(group), 1);

                    filterChange('all');
                });

            }

            /**
             * Toggle sidenav
             *
             * @param sidenavId
             */
            function toggleSidenav(sidenavId) {
                $mdSidenav(sidenavId).toggle();
            }

        }
    }
})();
