(function () {
    'use strict';

    angular
        .module('app.admin.providers', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, msApiProvider) {

        // State
        $stateProvider
            .state('app.admin_providers', {
                url: '/admin/providers',
                bodyClass: 'providers',
                views: {
                    'content@app': {
                        templateUrl: 'app/admin/providers/providers.html',
                        controller: 'ProvidersController as vm'
                    }
                },
                resolve: {
                    Contacts: function (msApi)
                    {
                        return msApi.resolve('contacts.contacts@get');
                    },
                    User: function (msApi)
                    {
                        return msApi.resolve('contacts.user@get');
                    },
                    Providers: function(msApi) {
                        return msApi.resolve('providers.providers@get');
                    }
                }
            });

        // Api
        msApiProvider.register('providers.providers', ['app/data/providers/providers.json']);
        // msApiProvider.register('contacts.user', ['app/data/contacts/user.json']);
    }
})();
