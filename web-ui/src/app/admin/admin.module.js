(function () {
    'use strict';

    angular
        .module('app.admin', [
            'app.admin.providers',
            'app.admin.users',
            // 'app.admin.contacts',
        ])
        .config(config);

    /** @ngInject */
    function config(msNavigationServiceProvider) {
        // Navigation
        msNavigationServiceProvider.saveItem('admin', {
            title: 'ADMIN',
            group: true,
            weight: 10
        });

        // Cloud Providers
        msNavigationServiceProvider.saveItem('admin.providers', {
            title: 'Providers',
            icon: 'icon-cloud',
            state: 'app.admin_providers'
        });

        // Contacts
        msNavigationServiceProvider.saveItem('admin.users', {
            title: 'Users',
            icon: 'icon-account-box',
            state: 'app.admin_users'
        });

        // // Contacts
        // msNavigationServiceProvider.saveItem('admin.contacts', {
        //     title: 'Contacts',
        //     icon: 'icon-cloud',
        //     state: 'app.admin_contacts'
        // });

    }
})();