'use strict';

// ** Dependencies
const _ = require('lodash');
const moment = require('moment');
const db = require('../db');
const Promise = require('bluebird');
const influx = require('../influx');
const Excel = require('exceljs');
const logger = require('nodus-framework').logging.createLogger();
const File = require('nodus-framework').files.File;

/**
 * Convert a JSON array to {columns:, values:} result set
 * @param json
 */
function JsonToTable(json) {

    const columns = [];

    // Add a new column or return a previously added column
    const Collumn = name => {
        // const column = columns.find(column => column.name === name);
        const index = columns.indexOf(name);

        // If the column is not found, then this is a new column
        if (index === -1) {
            const new_index = columns.length;

            columns.push(name);
            return new_index;
        }

        return index;
    };

    const rows = [];

    // Add a new Row
    const AddRow = json_values => {

        const row = [];

        const kv_pairs = _.toPairs(json_values);

        kv_pairs.forEach(([key, value]) => {
            const index = Collumn(key);
            row[index] = value;
        });

        // Add the row to the 'rows' collection
        rows.push(row);
    };

    // Add All Rows to the Table
    json.forEach(value => AddRow(value));

    // Return the Table object: {columns, rows}
    return {
        columns,
        rows
    };
}

/**
 * Convert a Table (see JsonToTable) to an Excel Worksheet
 * @constructor
 */
function AddWorksheet(workbook, name, table) {

    const sheet = workbook.addWorksheet(name);

    // Add Columns to worksheet
    sheet.columns = table.columns.map(col => ({
        header: col,
        key: col
    }));

    // table.rows.forEach(row => sheet.addRow(row));
    sheet.addRows(table.rows);

    return sheet;
}

/**
 * Create a new Workbook
 * @constructor
 */
function CreateWorkbook() {
    const workbook = new Excel.Workbook();

    workbook.creator = "nsm-reports";
    workbook.created = new Date();
    workbook.modified = new Date();

    return workbook;
}

const getMetricNames = _.memoize(() => db
    .showMetrics()
    .then(metrics => {
        // Select just the metric names
        const metric_names = _.map(metrics, metric => metric.name);
        return metric_names;
    }));

const DateTimeFormat = (format, cols) => values => _
    .mapValues(values, (value, col) =>
        cols.indexOf(col) !== -1
            ? moment(value).format(format)
            : value
    );

/**
 * Produce a server report for a given host between a given start/end datetime
 * @param host
 * @param start
 * @param end
 */
function getServerStats(hosts, from, to, interval) {

    from = from || moment();
    to = to || moment(from).subtract(1, 'hour');
    interval = interval || '1m';
    hosts = hosts.split(',');

    // Start/End datetimes
    const start_date = moment(from).toISOString();
    const end_date = moment(to).toISOString();

    // ** Get list of Metrics available from the server
    return getMetricNames()
        .then(metrics => {
            // Determine the tables to query based on the specified interval
            const tables = metrics.map(metric => `${metric}_${interval}`);

            const hosts_clause = hosts.map(host => `host='${host}'`).join(' and ');

            const server_query = table =>
            `SELECT * from ${table} where ` +
            hosts_clause +
            ` and time <= '${start_date}' and time >= '${end_date}'`;

            // Create client to run the query
            const client = influx.createClient();

            // Return metrics for each query
            return Promise
                .map(tables, metric => client
                    .runQuery(server_query(metric))
                    .then(results => results.map(DateTimeFormat('DD/MM/YYYY HH:mm:ss', ['time'])))
                    .then(result => [metric, result])
                );
        })
        .then(results => _.fromPairs(results))
        .then(results => _.mapValues(results, JsonToTable))
        .then(results => {

            // Create a new Workbook
            const workbook = CreateWorkbook();

            _.each(results, (table, name) => AddWorksheet(workbook, name, table));

            // Add A worksheet for each metric in the report
            return workbook;
        })
        .then(workbook => {
            //const filename = `${start_date}-${end_date}-${host}-metrics-${interval}.xlsx`;
            const filename = `metrics.xlsx`;

            logger.debug("Returning workbook...");
            return new File(filename, workbook, {
                writeStream: stream => {
                    logger.debug("Writing workbook to stream...");
                    return workbook.xlsx.write(stream);
                }
            });
        });
}

// ** Exports
module.exports = getServerStats;