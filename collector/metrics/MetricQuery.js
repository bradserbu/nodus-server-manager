'use strict';

// ** Constants
const METRICS = require('./metrics.json'); // List of available metrics and how to query them
const DEFAULT_INFLUX_HOST = 'localhost';
const DEFAULT_INFLUX_DB = 'collectd';

// ** Dependencies
const _ = require('lodash');
const util = require('util');
const influx = require('influx');
const moment = require('moment');
const logger = require('nodus-framework').logging.createLogger();
const errors = require('nodus-framework').errors;

// ** Configuration
const host = process.env["INFLUX_HOST"] || DEFAULT_INFLUX_HOST;
const database = process.env["INFLUX_DB"] || DEFAULT_INFLUX_DB;

// ** Metrics Database (InfluxDB) client
const InfluxDB = new influx.InfluxDB({
    host: host,
    database: database,
});

function ArrayArgument(value) {

    if (util.isNullOrUndefined(value))
        return [];

    if (util.isArray(value))
        return value;

    if (util.isString(value))
        return value.split(',').map(value => value.trim());

    throw errors('INVALID_ARGUMENT', 'The argument value is not a valid array.', value);
}

/**
 * Load query definition for a particular metric (name)
 * @param name
 * @constructor
 */
function MetricDefinition(name) {

    if (!name)
        throw errors('ARGUMENT_REQUIRED', 'The argument "name" is required to load a metric definition.');

    logger.debug('METRIC_NAME', name);

    let metric = METRICS[name.toLowerCase()];

    if (!metric) {
        logger.warn("The metric definition was not found.", {metric: metric});
        // throw errors('NOT_FOUND', 'The metric definition could not be found', {metric: metric});

        // DEFAULT METRIC DEFINITION
        metric = {
            table: name,
            instance: 'instance',
            measurement: 'type'
        };

        logger.warn("Using default metric definition...");
    }

    return metric;
}

/**
 * Turn a QueryDefinition into an actual InfluxDB statement that can be executed.
 * @param query
 * @returns {string}
 */
function buildStatement(query) {
    // Turn query into a string
    let statement = `SELECT * FROM ${query.table}`;

    // Add conditions
    if (query.where.length)
        statement += ' WHERE ' + query.where.join(' and ');

    // Add group by
    if (query.groupBy.length)
        statement += ' GROUP BY ' + query.groupBy.join(', ');

    // Add Order By
    if (query.orderBy.length)
        statement += ' ORDER BY ' + query.orderBy.join(', ');

    return statement;
}

/**
 * Execute an Influx DB query
 * @param query
 */
function runQuery(query) {

    const statement = buildStatement(query);

    return InfluxDB
        .query(statement)
}

function MetricQuery(metricName, interval, from, to, hosts = [], instances = [], measurements = []) {

    // From/To as DateTime objects
    from = moment(from);
    to = moment(to);

    // Arrary Arguments
    hosts = ArrayArgument(hosts);
    instances = ArrayArgument(instances);
    measurements = ArrayArgument(measurements);

    // Load the query definition for the specified metric
    const metric = MetricDefinition(metricName);

    // Determine the Table to select from
    const table = `${metric.table}_${interval}`;

    // Build Where Conditions
    const where = _.map(metric.filter || {}, (value, key) => `${key} = '${value}'`);

    const orClause = (col, values) => {
        if (values.length)
            where.push(' (' + values.map(value => `${col} = '${value.trim()}'`).join(' or ') + ') ')
    };

    // Add Host Criteria
    orClause('host', hosts);

    // Add Instance Criteria
    if (metric.instance)
        orClause(metric.instance, instances);

    // Add measurement Criteria
    if (metric.measurement)
        orClause(metric.measurement, measurements);

    // Add Time Criteria (From/To)
    where.push(`time >= '${from.toISOString()}'`);
    where.push(`time <= '${to.toISOString()}'`);

    // Add GroupBy
    const groupBy = [];
    if (metric.measurement) groupBy.push(metric.measurement);
    if (metric.instance) groupBy.push(metric.instance);

    // Add OrderBy
    const orderBy = ["time asc"];

    return {
        table: table,
        where: where,
        groupBy: groupBy,
        orderBy: orderBy,
        run() { return runQuery(this) }
    };
}

// ** Exports
module.exports = MetricQuery;