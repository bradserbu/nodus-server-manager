// ** Dependencies
'use strict';

// ** Dependencies
const _ = require('lodash');
const util = require('util');
const moment = require('moment');
const MetricQuery = require('./MetricQuery');
const errors = require('nodus-framework').errors;

/**
 * Returns the average CPU consumption over a specified interval between two points in time.
 * - Computes the averageCPU for a specified interval between two points in time (from-to)
 * - Interval should be a multiple of the time range specified (from-to)
 *
 * Returns a separate set of datapoints per CPU Instance.
 * - For instance, if a server has 2 CPU Cores, then there would be two sets of 60 datapoints
 * representing the average CPU for the time and interval specified.
 *
 * Example: averageCPU("nodus-server1", "2017-05-22 12:00PM", "2017-05-22 1:00PM", "1min")
 * - Computes the averageCPU for each minute between 12:00PM and 1:00PM.
 * - 60 datapoints are returned.
 * - Each datapoint has the from/to and the averageCPU returned.
 * @param server - The host which identifies the server in the metrics database
 * @param interval - The interval of time to average (1min, 5min, 15min, 1hour, 1day).
 * @param from - The point in time to begin the average calculation.
 * @param to - The point in time to end the average calculation.
 */
function average(metric, interval, from, to, hosts, instances, measurements) {

    // From/To must be datetimes
    from = moment(from);
    to = moment(to);

    const query = MetricQuery(metric, interval, from, to, hosts, instances, measurements);

    return query.run();
        // .then(results => _.groupBy(results, entry => entry.instance));
}

module.exports = average;