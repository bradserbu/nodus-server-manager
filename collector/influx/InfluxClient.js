'use strict';

// ** Constants
const DEFAULT_INFLUX_HOST = 'localhost';
const DEFAULT_INFLUX_PORT = 8086;
const DEFAULT_INFLUX_DB = 'nsm';

// ** Dependencies
const _ = require('lodash');
const Promise = require('bluebird');
const influx = require('influx');
const logger = require('nodus-framework').logging.createLogger("nsm:influx:client");
const {map, prop} = require('ramda');

function StringEqualsIgnoreCase(s1, s2) {
    return s1.match(new RegExp(s2, "i"));
}

function State(properties) {
    const state = Object.assign({}, properties);

    // Method to ensure a state condition otherwise preform an action to transition
    state.ensure = (property, action) => {

        // Check if the state is already true
        if (state[property])
            return Promise.resolve();

        // Run action then transition to the new state
        return action(state)
            .then(() => state[property] = true);
    };

    return state;
}

function InfluxClient() {

    // Configuration
    const host = process.env["INFLUX_HOST"] || DEFAULT_INFLUX_HOST;
    const port = process.env["INFLUX_PORT"] || DEFAULT_INFLUX_PORT;
    const database = process.env["INFLUX_DB"] || DEFAULT_INFLUX_DB;

    // Metrics Database (InfluxDB) client
    logger.debug("Creating InfluxDB client...", {host, port, database});
    const client = new influx.InfluxDB({
        host: host,
        port: port,
        database: database
    });

    // Initialize InfluxDB client
    const state = State({
        INITIALIZED: false
    });

    const init = () => {
        logger.debug("Initializing InfluxDB client...");

        // Ensure Database Exists
        return client
            .getDatabaseNames()
            .then(databases => {
                if (!databases.includes(database)) {
                    logger.info('DATABASE_NOT_FOUND', 'Creating database...', database);
                    return client
                        .createDatabase(database)
                        .then(() => logger.info("Database created successfully."));
                }
            })
            .then(() => {
                logger.debug("InfluxDB client initialized successfully.");
            });
    };

    // ** Ensure client is initialized before running any actions
    // TODO: Move to generic Action/State library
    const ensureInit = () => state.ensure('INITIALIZED', init);

    // TODO: Move to generic Action/State library
    const ensureActions = actions => _.mapValues(actions, action => ensureInit(action));

    const showTags = measurement => client
        .query(`SHOW TAG KEYS FROM "${measurement}"`, {database: database})
        .then(map(prop('tagKey')));

    const showFields = measurement => client
        .query(`SHOW FIELD KEYS FROM ${measurement}`, {database: database})
        .then(map(row => [row['fieldKey'], row['fieldType']]))
        .then(_.fromPairs);

    return {
        get database() {
            return database
        },
        get host() {
            return host
        },
        get port() {
            return port
        },
        createRetentionPolicy: function (name, duration, replication = 1) {
            return ensureInit().then(() =>
                client.createRetentionPolicy(name, {
                    database,
                    duration,
                    replication
                }));
        },
        writePoints: (points, options) => ensureInit().then(() => client.writePoints(points, options)),
        measurements: () => ensureInit().then(() => client
            .getMeasurements(database)
            // TODO: The following patterns should definitely be in ecapsulated in the 'actions' library
            .then(measurements => Promise.map(measurements, measurement =>
                Promise.props({
                    name: measurement,
                    fields: showFields(measurement),
                    tags: showTags(measurement)
                })
            ))),
        continuousQueries: () => ensureInit().then(() => client.showContinousQueries(database)),
        series: (measurement) => ensureInit().then(() => client.getSeries({measurement, database})),
        fields: (measurement) => ensureInit().then(() => showFields(measurement)),
        tags: (measurement) => ensureInit().then(() => showTags(measurement)),
        createContinuousQuery: (name, query) => ensureInit().then(() => client
            .showContinousQueries(database) // Check if the continuous query already exists
            .then(database_queries => {
                logger.debug("DATABASE_QUERIES", database_queries);

                // Check if the query by the same name exists
                const db_query = database_queries.find(q => q.name === name);
                if (!db_query)
                    return;

                // Drop the continuous query if it already exists
                // TODO: Add --force option or --dropQueries option
                if (StringEqualsIgnoreCase(db_query.name, name)) {
                    logger.info("A continuous query with the same name already exists.", {name});
                    logger.info("Comparing query texts...");

                    // TODO: Add --force option or --dropQueries option
                    // Check if the query texts are different... InfluxDB should let it through anyway...
                    if (!StringEqualsIgnoreCase(db_query.query, query)) {
                        logger.info("The query has been changed...");
                        logger.warn("Dropping continuous query...", {name});
                        return client.dropContinuousQuery(name, database);
                    }
                }
            })
            .then(() => {
                logger.info("CQ_CREATE", "Creating continuous query...", {name, query});
                return client
                    .query(query)
            })),
        runQuery: (query) => ensureInit().then(() => {
            logger.debug('RUN_QUERY', query);
            return client.query(query);
        })
    }
}

// ** Exports
module.exports = InfluxClient;
module.exports.createClient = InfluxClient;
module.exports.Precision = influx.Precision;