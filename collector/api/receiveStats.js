'use strict';

// ** Dependencies
const _ = require('lodash');
const extend = require('extend');
const logger = require('nodus-framework').logging.createLogger();
const influx = require('../influx');

// Shared Client
const InfluxDB = influx.createClient();

function mapIndex(values, func) {

    const results = [];

    for (let index = 0; index < values.length; index++) {
        const value = values[index];
        results.push(func(value, index));
    }

    return results;
}

function eachIndex(values, func) {
    for (let index = 0; index < values.length; index++) {
        const value = values[index];
        func(value, index);
    }
}

function CPULoad(stats, addPoint) {

    const cpu_load = stats.cpu.load;
    const cpu_load_cpus = cpu_load.cpus;

    // Aggregate Load
    addPoint({
        measurement: 'cpu_load',
        fields: {
            load: cpu_load.currentload,
            load_user: cpu_load.currentload_user,
            load_system: cpu_load.currentload_system,
            load_nice: cpu_load.currentload_nice,
            load_irq: cpu_load.currentload_irq
        }
    });

    // Add Point for Each CPU
    eachIndex(cpu_load_cpus, (value, index) =>
        addPoint({
            measurement: 'cpu_cpus_load',
            fields: {
                load: value.load,
                load_user: value.load_user,
                load_system: value.load_system,
                load_nice: value.load_nice,
                load_irq: value.load_irq
            },
            tags: {
                instance: index
            }
        }));
}

function CPUTemp(stats, addPoint) {

    const cpu_temp = stats.cpu.temp;

    // Main Temp
    addPoint({
        measurement: 'cpu_temp',
        fields: {
            main: cpu_temp.main,
            max: cpu_temp.max
        }
    });

    // Core Temp
    const cores = cpu_temp.cores;
    eachIndex(cores, (temp, index) => {
        addPoint({
            measurement: 'cpu_cores_temp',
            fields: {
                temp: temp
            },
            tags: {
                instance: index
            }
        });
    });
}

function CPUSpeed(stats, addPoint) {
    const currentSpeed = stats.cpu.currentSpeed;
    addPoint({
        measurement: 'cpu_speed',
        fields: currentSpeed
    });
}

function Memory(stats, addPoint) {
    const memory = stats.memory;
    addPoint({
        measurement: 'memory',
        fields: memory
    });
}

function Uptime(stats, addPoint) {
    addPoint({
        measurement: 'system',
        fields: {
            uptime: stats.system.uptime
        }
    });
}

function FSStats(stats, addPoint) {
    addPoint({
        measurement: 'fs_stats',
        fields: stats.fs.stats
    });
}

function FSSize(stats, addPoint) {
    const disks = stats.fs.size;

    eachIndex(disks, disk => addPoint({
        measurement: 'fs_size',
        fields: {
            size: disk.size,
            used: disk.used,
            use: disk.use
        },
        tags: {
            instance: disk.fs
        }
    }));
}

function DiskIO(stats, addPoint) {
    const diskio = stats.fs.diskio;

    addPoint({
        measurement: 'disk_io',
        fields: diskio
    });
}

function NetworkStats(stats, addPoint) {
    const network_stats = stats.network.stats;

    addPoint({
        measurement: 'network_stats',
        fields: {
            rx: network_stats.rx,
            tx: network_stats.tx,
            rx_sec: network_stats.rx_sec,
            tx_sec: network_stats.tx_sec,
            ms: network_stats.ms
        },
        tags: {
            instance: network_stats.iface
        }
    })
}

function saveMetrics({timestamp, stats}) {

    // Timestamp Measured in Microseconds
    timestamp = timestamp;

    // The host name of the system to write to influx
    // TODO: This should be the ID of the server from MongoDB
    const hostname = stats.system.hostname;
    const points = [];

    const addPoint = point => points.push(
        extend(true, {
            timestamp: timestamp,
            tags: {
                host: hostname
            }
        }, point)
    );

    // Add CPU Current Speed
    CPULoad(stats, addPoint);
    CPUTemp(stats, addPoint);
    CPUSpeed(stats, addPoint);
    Memory(stats, addPoint);
    Uptime(stats, addPoint);
    FSStats(stats, addPoint);
    FSSize(stats, addPoint);
    DiskIO(stats, addPoint);
    NetworkStats(stats, addPoint);

    // Write CPU Load for each processor to InfluxDB
    // return points;
    return InfluxDB
        .writePoints(points, {
            precision: influx.Precision.Microseconds
        });
}

function receiveStats($request) {

    // Pull Data from the payload of the request
    const data = $request.body;

    logger.debug('RECEIVE_DATA', data);
    return saveMetrics(data);
}

// ** Exports
module.exports = receiveStats;