'use strict';

// ** Constants
const DEFAULT_CALCULATIONS = ['count', 'mean', 'max', 'min'];

const DEFAULT_INTERVALS = ['1m', '5m', '15m', '30m', '1h'];

/**
 * Maps an interval to how far back in time it should be caluclated.  This is used
 * to ensure any datapoints that arrive from collector that may arrive just after the continuous query is applied to calculate the ranges.
 * - Example: 1m:5m Will apply the aggregate calculations every 1m for each 1m interval for data collected over the last 5 minutes.
 */
const DEFAULT_RESAMPLE_RATES = {
    "1m": "5m",
    "5m": "10m",
    "15m": "30m",
    "30m": "1h",
    "1h": "2h"
};

// ** Dependencies
const _ = require('lodash');
const util = require('util');
const Promise = require('bluebird');
const influx = require('./influx');
const fs = require('fs-extra');
const files = require('nodus-framework').files;
const errors = require('nodus-framework').errors;
const logger = require('nodus-framework').logging.createLogger();

function lookupResampleRate(interval) {

    // TODO: Make configurable
    return DEFAULT_RESAMPLE_RATES[interval];
}

// Create Influx DB connection
const client = influx.createClient();

const database = client.database;

// Generate a continuous Query for a specified measurement
const buildContinuousQuery = function ({name: measurement, fields, tags}, interval, calculate = DEFAULT_CALCULATIONS) {

    logger.debug("ARGUMENTS", arguments);
    logger.info("Building continuous query...", {measurement: measurement});

    // The name of the continuous query
    const cq_name = `cq_${measurement}_${interval}`; // The name of the continuous query

    // Lookup resample rate
    const resample_rate = lookupResampleRate(interval);
    if (util.isNullOrUndefined(resample_rate)) {
        logger.warn('RESAMPLE_RATE_NOT_FOUND', 'Re-sampling for the interval will be disabled...', {interval: interval});
    }

    // Build Resample Clause
    const resample_clause = resample_rate
        ? ` RESAMPLE FOR ${resample_rate}`
        : '';
    logger.debug("RESAMPLE_CLAUSE", resample_clause);

    // Build Select Caluse
    const select = _
        .keys(fields)
        // Apply Calculation to Field and select as <calc>_<field> (i.e. mean_load)
        .map(field => calculate
            .map(calc => `${calc}(${field}) as "${calc}_${field}"`))
        .join(" , ");

    // Group by host, instance, {tags}, time({interval})
    const group_by = tags.join(', ');

    const cq_query =
        `CREATE CONTINUOUS QUERY ${cq_name} ON "${database}" ` +
        ` ${resample_clause} ` +
        ` BEGIN SELECT ` +
        select +
        ` INTO "${database}".autogen.${measurement}_${interval} FROM "${database}".autogen.${measurement} ` +
        ` GROUP BY ${group_by}, time(${interval}) END`;

    return {
        name: cq_name,
        query: cq_query
    };
};

const isIntervalTable = table_name => _.find(DEFAULT_INTERVALS, interval => table_name.endsWith(`_${interval}`));

// Show Tables that store raw metrics
const showMetrics = () => client
    .measurements()
    // Don't create continuous queries on interval tables
    .then(measurements => _.reject(measurements, measurement => isIntervalTable(measurement.name)));

// Build a list of continuous queries for all measurements
const buildIntervalContinuousQueries = measurement => Promise.map(DEFAULT_INTERVALS, interval => buildContinuousQuery(measurement, interval));
const generateContinuousQueries = () => showMetrics()
    .then(measurements => Promise.map(measurements, buildIntervalContinuousQueries))
    .then(_.flatten);

const createContinuousQueries = (filename) => {

    if (util.isNullOrUndefined(filename)) {
        throw errors('ARGUMENT_ERROR', 'The argument "filename" is required.');
    }

    const filepath = files.resolvePath(filename);
    logger.debug("FILEPATH", filename);

    return fs
        .readJson(filepath)
        .then(queries => {
            logger.debug("- DATABASE", queries);
            logger.debug("- QUERIES", queries);

            logger.info("Creating continuous queries...");
            return Promise.each(queries, q => client.createContinuousQuery(q.name, q.query));
        });
};

// ** Exports
module.exports = {
    showMetrics,
    generateContinuousQueries,
    createContinuousQueries
};