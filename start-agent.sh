#!/usr/bin/env bash

# Exit on error
set -e

# Change directory to script location
cd "$(dirname "$0")"

# Variables
NSM_PATH=`pwd`
SERVICE_NAME=nsm-agent
NODE=`which node`
NSM_AGENT_PATH=agent
NODUS_RUN="$PWD/node_modules/.bin/nodus-run"
DEBUG="*"

export COLLECTOR_HOST="${NSM_COLLECTOR_HOST:-147.75.104.69}"
export COLLECTOR_PORT="${NSM_COLLECTOR_PORT:-8081}"
export DEBUG="*"

# Ensure nodejs is installed
if [ -z "$NODE" ]; then
  echo "NodeJS could not be found on this system."
  echo "Please install NodeJS."
  exit 0;
fi

# Start Agent
cd $NSM_AGENT_PATH
exec -a $SERVICE_NAME $NODUS_RUN "commands/collect-stats.js" "$@"
#"$NODE" "node_modules/.bin/nodus-run" "commands/collect-stats.js" "$@"