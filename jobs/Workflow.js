'use strict';

const ACTIVITY_STATUS = {
    CREATED: 'created',
    STARTED: 'started',
    FAILED: 'failed',
    COMPLETED: 'completed'
};

// ** Dependencies
const _ = require('lodash');
const uuid = require('uuid');
const microtime = require('microtime');
const Jobs = require('./Job');
const database = require('../web-api/database');
const Action = require('nodus-framework').actions;
const Promise = require('bluebird');
const logger = require('nodus-framework').logging.createLogger();

function Activity(name, handler) {
    logger.info('CREATE_ACTIVITY:', name);

    const id = uuid.v4();
    const status = {
        timestamp: microtime.now(),
        status: ACTIVITY_STATUS.CREATED,
    };
    const action = Action(`run:${name}`, handler);

    const activity = {
        id,
        name,
        status,
        run: (args) => action
            .run(args)
            // Update the status -> COMPLETED
            .tap(result => database.update('activities', {
                status: {
                    timestamp: microtime.now(),
                    status: ACTIVITY_STATUS.COMPLETED
                }
            }, {id}))
            .tapCatch(err => database.update('activities', {
                status: {
                    timestamp: microtime.now(),
                    status: ACTIVITY_STATUS.FAILED
                }
            }, {id}))
    };

    return database
        .insert('activities', activity)
        .then(() => activity);
}

function Workflow(workflow) {

    const id = uuid.v4();
    const activities = _.mapValues(workflow, (handler, name) => Activity(name, handler));

    const job = {
        id,
        status: {
            timestamp: microtime.now(),
            status: ACTIVITY_STATUS.CREATED
        }
    };

    const create_job = () => database
        .insert('jobs', job);

    const start_job = () => database
        .update('jobs', {
            id,
            status: {timestamp: microtime.now(), status: ACTIVITY_STATUS.STARTED}
        });

    const fail_job = () => database
        .update('jobs', {
            id,
            status: {timestamp: microtime.now(), status: ACTIVITY_STATUS.FAILED}
        });

    const complete_job = () => database
        .update('jobs', {
            id,
            status: {timestamp: microtime.now(), status: ACTIVITY_STATUS.COMPLETED}
        });

    const runActivities = () => {
        let result = Promise.resolve();

        _.each(activities, activity => result = Promise.resolve(result)
            .then(result => activity.run(result)));

        return result;
    };

    return create_job()
        .then(() => activities)
        .then(() => ({
            id,
            activities: activities.map(activity => activity.id),
            start: () => start_job()
                .then(() => runActivities())
                .tap(() => complete_job())
                .tapCatch(err => fail_job())
        }))
}

// ** Exports
module.exports = Workflow;
module.exports.Activity = Activity;