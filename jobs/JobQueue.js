'use strict';

const SHUTDOWN_TIMEOUT = 10000; // 10 Seconds

// ** Dependencies
const kue = require('kue');
const logger = require('nodus-framework').logging.createLogger();
const program = require('nodus-framework').program;

logger.debug('Creating Job Queue...');
const JobQueue = kue.createQueue({
    redis: 'redis://localhost:6379/'
});

// Shutdown the queue on program shutdown
program.on('shutdown', () => {
    logger.debug('Shutting down job queue...');
    JobQueue.shutdown(SHUTDOWN_TIMEOUT, (err) => {
        if (err) logger.error('Error shutting down job queue:', err);
        else logger.debug('Job queue shutdown successfully.');
    });
});

// ** Exports
module.exports = JobQueue;