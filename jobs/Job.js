'use strict';

// ** Constants
const DEFAULT_PROCESS_OPTIONS = {
    mapNamedArguments: true
};  // Default options for process job

// ** Dependencies
const extend = require('extend');
const Action = require('nodus-framework').actions;
const JobQueue = require('./JobQueue');
const Promise = require('bluebird');
const logger = require('nodus-framework').logging.createLogger();
const errors = require('nodus-framework').errors;

function runJob(name, data) {
    logger.info('RUN_JOB', {name, data});

    // Wait for the job to complete
    return new Promise((resolve, reject) => {
        JobQueue
            .create(name, data)
            .on('failed', err => {
                logger.info('FAILED', err);
                reject(err);
            })
            .on('complete', result => {
                logger.info('COMPLETED', result);
                resolve(result);
            })
            .save();
    });
}

function processJob(name, handler) {

    logger.debug('Creating Action to handle job...', name);

    const action = Action(
        `job:${name}`,
        handler
    )
        .mapNamedArguments();

    logger.debug('Registering processor with job queue...', name);
    JobQueue
        .process(name, function (job, done) {
            logger.debug('PROCESS_JOB', {name, id: job.id, data: job.data});
            logger.info('DATA', job.data);

            action
                .run(job.data)
                .then(result => {
                    logger.info('JOB_RESULT', result);
                    done(null, result)
                })
                .catch(err => done(err))
        });
}

// ** Exports
module.exports.run = runJob;
module.exports.process = processJob;