#!/usr/bin/env sh

PAUSE=0

# Exit on error
set -e

# Change directory to script location
cd "$(dirname "$0")"

# Variables
NSM_HOME=`pwd`
NSM_AGENT_PATH="$NSM_HOME/agent"
NSM_COLLECTOR_PATH="$NSM_HOME/collector"

echo $NSM_COLLECTOR_PATH

# Ensure NodeJS is installed
if [ -z `which node` ]; then
  echo "NodeJS could not be found on this system."
  echo "Please install NodeJS."
  echo ""
  echo "   https://nodejs.org/en/download/package-manager/#enterprise-linux-and-fedora   "
  exit 0;
fi

# Install dependencies

echo "Installing 'core' Dependencies..."
sleep $PAUSE
cd $NSM_HOME
npm install  --silent

echo "Installing 'nsm-agent' Dependencies..."
sleep $PAUSE
cd $NSM_AGENT_PATH
npm install --silent

echo "Installing 'nsm-collector' Dependencies..."
sleep $PAUSE

cd $NSM_COLLECTOR_PATH
npm install --silent

echo
echo " **** SUCCESS ****"
