'use strict';

// ** Dependencies
const _ = require('lodash');
const Promise = require('bluebird');
const stats = require('./stats');
const microtime = require('nodus-framework').microtime;

// TODO: Use nodus-framework 'actions' module instead of promises directly
const run = target => _.isFunction(target)
    ? Promise.try(target)
    : Promise.props(promisifyModule(target));

const promisifyModule = module => Promise
    .props(_.mapValues(module, provider => run(provider)));

// ** Exports
function getStats() {

    const timestamp = microtime.now().toString();

    return promisifyModule(stats)
        .then(stats => ({
            timestamp: timestamp,
            stats: stats
        }));
}

// ** Exports
module.exports = getStats;