#!/usr/bin/env node
'use strict';

// ** Dependencies
const collect = require('./commands/collect-stats');
const yargs = require('yargs');

// Parse Command Line Arguments
const argv = yargs.argv;

// Start Collector
collect(argv.interval);