'use strict';

// ** Dependencies
const os = require('os');
const si = require('systeminformation');

function getMemory() {

    const platform = os.platform();

    // Use 'systeminformation' as it reports more data for linux/darwin OS
    switch (platform) {
        case 'darwin':
        case 'linux':
            return si.mem();
        case 'win32':
        default:
            const total = os.totalmem();
            const free = os.freemem();

            return {
                total: total,
                free: free,
                used: total - free
            };
    }
}

// ** Exports
module.exports = getMemory;