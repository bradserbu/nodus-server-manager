'use strict';

// ** Dependencies
const os = require('os');

function networkInterfaces() {
    return os.endianness();
}

// ** Exports
module.exports = networkInterfaces;