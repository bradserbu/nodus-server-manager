'use strict';

// ** Dependencies
const si = require('systeminformation');

function getCpuTemp() {

    return si.cpuTemperature();
}

// ** Exports
module.exports = getCpuTemp;