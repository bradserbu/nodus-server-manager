'use strict';

// ** Dependencies
const si = require('systeminformation');

function getCpuSpeed() {
    return si.cpuCurrentspeed();
}

// ** Exports
module.exports = getCpuSpeed;