'use strict';

// ** Dependencies
const si = require('systeminformation');

function getCpuFlags() {
    return si.cpuFlags();
}

// ** Exports
module.exports = getCpuFlags;