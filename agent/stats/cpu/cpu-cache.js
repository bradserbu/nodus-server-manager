'use strict';

// ** Dependencies
const si = require('systeminformation');

function getCpuFlags() {
    return si.cpuCache();
}

// ** Exports
module.exports = getCpuFlags;