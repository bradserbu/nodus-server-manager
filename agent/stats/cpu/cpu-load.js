'use strict';

// ** Dependencies
const si = require('systeminformation');
const os = require('os');

function getCpuInfo() {
    return si.currentLoad();
    // return os.cpus();
}

// ** Exports
module.exports = getCpuInfo;