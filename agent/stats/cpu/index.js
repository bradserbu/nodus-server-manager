module.exports = {
    info: require('./cpu-info'),
    cache: require('./cpu-cache'),
    endian: require('./cpu-endian'),
    flags: require('./cpu-flags'),
    load: require('./cpu-load'),
    currentSpeed: require('./cpu-current-speed'),
    temp: require('./cpu-temp')
};