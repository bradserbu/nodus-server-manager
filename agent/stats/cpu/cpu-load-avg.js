'use strict';

// ** Dependencies
const os = require('os');

function getCpuLoadAvg() {

    const avgLoad = os.loadavg();
    return {
        "1_min": avgLoad[0],
        "5_min": avgLoad[1],
        "15_min": avgLoad[2]
    };
}

// ** Exports
module.exports = getCpuLoadAvg;