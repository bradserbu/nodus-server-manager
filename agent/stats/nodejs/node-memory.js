'use strict';

function getMemoryStats() {
    const memory = process.memoryUsage();

    return {
        used: memory.heapUsed,
        total: memory.heapTotal,
        rss: memory.rss
    };
}

// ** Exports
module.exports = getMemoryStats;