'use strict';

// ** Dependencies
const eventLoopStats = require('event-loop-stats');

function getEventLoopStats() {
    const stats = eventLoopStats.sense();

    const formattedStats = stats && {
            max: stats.max,
            maxUnit: 'ms',
            min: stats.min,
            minUnit: 'ms',
            sum: stats.sum,
            sumUnit: 'ms',
            num: stats.num,
            numUnit: 'pcs'
        };

    return {
        stats: formattedStats,
        requests: process._getActiveRequests().length,
        requestsUnit: 'pcs',
        handlers: process._getActiveHandles().length,
        handlersUnit: 'pcs'
    };
}

// ** Exports
module.exports = getEventLoopStats;