module.exports = {
    devices: require('./fs-devices'),
    diskio: require('./fs-diskio'),
    size: require('./fs-size'),
    stats: require('./fs-stats')
};