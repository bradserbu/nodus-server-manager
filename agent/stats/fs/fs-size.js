'use strict';

// ** Dependencies
const si = require('systeminformation');

function getFSSize() {
    return si.fsSize();
}

// ** Exports
module.exports = getFSSize;