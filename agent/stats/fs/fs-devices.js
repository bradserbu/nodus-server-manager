'use strict';

// ** Dependencies
const si = require('systeminformation');

function getDisksIO() {
    return si.blockDevices();
}

// ** Exports
module.exports = getDisksIO;