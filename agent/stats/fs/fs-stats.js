'use strict';

// ** Dependencies
const si = require('systeminformation');

function getFsStats() {
    return si.fsStats();
}

// ** Exports
module.exports = getFsStats;