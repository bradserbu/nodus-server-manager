'use strict';

// ** Dependencies
const si = require('systeminformation');

function getDisksIO() {
    return si.disksIO();
}

// ** Exports
module.exports = getDisksIO;