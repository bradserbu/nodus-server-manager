'use strict';

// ** Dependencies
const _ = require('lodash');
const Promise = require('bluebird');
const si = require('systeminformation');
const networkInterfaces = require('./network-interfaces');

function getNetworkStatsAllIntefaces() {
    const interfaces = networkInterfaces();

    // Get Stats for EVERY NETWORK INTERFACE (warning: This is much slower...)
    return Promise.props(
        _.mapValues(interfaces, (value, iface) =>
            si.networkStats(iface)
        )
    );
}

/**
 * Get the network stats for just the default interface
 * @returns {*}
 */
function getNetworkStats() {
    return si.networkStats(); // TODO: Make 'AllInterfaces', or list of interfaces to monitor a configurable option
}

// ** Exports
module.exports = getNetworkStats;