module.exports = {
    defaultInterface: require('./network-default-interface'),
    interfaces: require('./network-interfaces'),
    connections: require('./network-connections'),
    stats: require('./network-stats')
};