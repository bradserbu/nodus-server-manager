'use strict';

// ** Dependencies
const os = require('os');

function networkInterfaces() {
    return os.networkInterfaces();
}

// ** Exports
module.exports = networkInterfaces;