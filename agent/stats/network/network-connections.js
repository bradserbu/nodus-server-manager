'use strict';

// ** Dependencies
const si = require('systeminformation');

function getNetworkStats() {
    return si.networkConnections();
}

// ** Exports
module.exports = getNetworkStats;