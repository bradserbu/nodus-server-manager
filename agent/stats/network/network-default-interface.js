'use strict';

// ** Dependencies
const si = require('systeminformation');

function getDefaultInterface() {
    // TODO: Windows Support
    return si.networkInterfaceDefault();
}

// ** Exports
module.exports = getDefaultInterface;