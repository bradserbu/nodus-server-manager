module.exports = {
    os: require('./platform-os'),
    constants: require('./platform-constants'),
    environment: require('./platform-env')
};