'use strict';

// ** Dependencies
const os = require('os');

function getConstants() {
    return os.constants;
}

// ** Exports
module.exports = getConstants;