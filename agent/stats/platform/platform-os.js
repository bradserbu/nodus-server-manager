'use strict';

// ** Dependencies
const os = require('os');
const linuxDistro = require('./linux/distro');
const isLinux = os.platform() === 'linux';

function getOsInfo() {
    const ret = {
        arch: process.arch,
        type: os.type(),
        release: os.release()
    };

    // ** Return Linux Specific info which includes additional 'distro' information.
    if (isLinux) {
        return linuxDistro()
            .then(result => {
                ret.dist = result;
                return ret;
            });
    }

    return ret;
}

// ** Exports
module.exports = getOsInfo;