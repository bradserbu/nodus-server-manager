'use strict';

// ** Dependencies
const si = require('systeminformation');

function getVersions() {
    return si.versions();
}

// ** Exports
module.exports = getVersions;