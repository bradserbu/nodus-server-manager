'use strict';

// ** Dependencies
const getos = require('getos');

function getOsInfo() {
    return new Promise((resolve, reject) => {
        getos(function (err, result) {
            if (err) return reject(err);

            const ret = {
                name: result.dist,
                version: result.release
            };

            if (result.codename)
                ret["dist-codename"] = result.codename;

            resolve(ret);
        });
    });
}

// ** Exports
module.exports = getOsInfo;