'use strict';

// ** Dependencies
const os = require('os');

function getEnvironmentVariables() {
    return process.env;
}

// ** Exports
module.exports = getEnvironmentVariables;