module.exports = {
    cpu: require('./cpu'),
    // docker: require('./docker'),
    fs: require('./fs'),
    memory: require('./memory'),
    nodejs: require('./nodejs'),
    platform: require('./platform'),
    processes: require('./processes'),
    system: require('./system'),
    network: require('./network')
};