'use strict';

// ** Constants
const FIELD_MAPPINGS = {
    'IDProcess': 'pid',
    'Name': 'command',
    'PercentProcessorTime': 'pcpu',
    'PrivateBytes': 'pmem',
    'VirtualBytes': 'mem_vsz'
};

// ** Dependencies
const _ = require('lodash');
const os = require('os');
const execa = require('execa');

function mapResults(results) {

    // Results will contain one extra field which is the machine name
    results = results.slice(1);

    const keys = _.values(FIELD_MAPPINGS);
    const pairs = _.zip(keys, results);

    const data = _.fromPairs(pairs);

    return data;
}

function getProcessList() {

    const fields = _.keys(FIELD_MAPPINGS);
    const fieldList = fields.join(',');
    const num_fields = fields.length;

    return execa
        .stdout('wmic', ['path', 'Win32_PerfFormattedData_PerfProc_Process', 'get', fieldList, '/format:csv'])
        .then(output => {
            // Split output line-by-line
            const lines = output.trim().split(os.EOL);

            // Parse output csv -> json
            return lines
                .filter((line, index) => line && index >= 1)
                .map(line => line.trim().split(','))
                .map(mapResults);
        });
}

// ** Exports
module.exports = getProcessList;