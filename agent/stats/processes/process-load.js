'use strict';

// ** Dependencies
const si = require('systeminformation');

function getProcessStats() {

    // if (isRunningInVm()) {
    //     logger.debug('WARNING: Process information is not available when running on docker.');
    //     return Promise.resolve({
    //         processes: [],
    //         processLoad: []
    //     });
    // }

    // Wrap in another Promise because si promise fails on docker container (Error: cannot read from of undefined);
    return si.processLoad();
}

// ** Exports
module.exports = getProcessStats;
