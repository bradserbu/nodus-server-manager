'use strict';

// ** Dependencies
const si = require('systeminformation');
const os = require('os');
const isWindows = os.platform() === 'win32';
const getProcessListWin32 = require('./win32/process-list');
const logging = require('nodus-framework').logging.createLogger();

function getProcessList() {
    return si
        .processes()
        .catch(err => {
            // This fails on docker
            logger.warn('Error fetching process list.', err);
        });
}

// ** Exports
module.exports = isWindows ? getProcessListWin32 : getProcessList;