'use strict';

// ** Dependencies
const fs = require('fs');
const errors = require('nodus-framework').errors;
const logger = require('nodus-framework').logging.createLogger();

function readProcFile(file) {
    let procFileContent;

    try {
        procFileContent = fs.readFileSync('/proc' + file, 'utf-8')
    } catch (err) {
        logger.warn(errors('READ_PROC_FILE_ERROR', 'Cannot read procfile:', {file: file, error: err}));
    }

    return procFileContent;
}

function isRunningInVm() {
    const cGroupContent = readProcFile('/self/cgroup');

    if (!cGroupContent) {
        logger.warn('VM detction failed.');
        return; // Undefined = 'unknown'
    }

    const cGroupLines = cGroupContent.split('\n');
    const patterns = [
        /^\/docker/,
        /^\/lxc/
    ];
    let parts;
    let j;
    let i;
    for (i = 0; i < cGroupLines.length; i++) {
        parts = cGroupLines[i].split(':');
        if (parts.length !== 3) {
            continue;
        }
        for (j = 0; j < patterns.length; j++) {
            if (parts[2].match(patterns[i])) {
                return true
            }
        }
    }

    return false;
}

// ** Exports
module.exports = isRunningInVm;

