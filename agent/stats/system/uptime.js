'use strict';

// ** Dependencies
const os = require('os');

function getUptime() {
    return os.uptime();
}

// ** Exports
module.exports = getUptime;