'use strict';

// ** Dependencies
const si = require('systeminformation');

function getSystemInfo() {
    return si.system();
}

// ** Exports
module.exports = getSystemInfo;