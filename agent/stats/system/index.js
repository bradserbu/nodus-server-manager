module.exports = {
    hostname: require('./hostname'),
    info: require('./info'),
    isVM: require('./is-virtual'),
    uptime: require('./uptime')
};