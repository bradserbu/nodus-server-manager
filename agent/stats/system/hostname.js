'use strict';

// ** Dependencies
const os = require('os');

function getHostname() {
    return os.hostname();
}

// ** Exports
module.exports = getHostname;