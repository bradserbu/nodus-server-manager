'use strict';

// ** Dependencies
const execa = require('execa');
const logger = require('nodus-framework').logging.createLogger();

/**
 * Get the path to the docker command (if available)
 */
function dockerCommand() {
    return execa
        .stdout('which', ['docker'])
        .catch(err => {
            logger.warn('DOCKER_COMMAND_NOT_FOUND', err);

            // Return undefined (indicating docker command is not installed locally)
            return;
        });
}

// ** Exports
module.exports = dockerCommand;