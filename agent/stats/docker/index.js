module.exports = {
    info: require('./docker-info'),
    stats: require('./docker-stats')
};