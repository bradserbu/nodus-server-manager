'use strict';

// ** Dependencies
const execa = require('execa');
const docker_cmd = require('./docker-cmd');

function getDockerInfo() {
    return docker_cmd()
        .then(docker_installed => {
            if (docker_installed) {
                return execa
                    .stdout('docker', ['info', '--format', '{{ json . }}'])
                    .then(JSON.parse);
            }

            // Undefined = no docker information found
            return;
        });
}

// ** Export
module.exports = getDockerInfo;