'use strict';

// ** Dependencies
const execa = require('execa');
const docker_cmd = require('./docker-cmd');

function getDockerInfo() {
    return docker_cmd()
        .then(docker_installed => {
            if (docker_installed) {
                return execa
                    // .stdout('docker', ['stats', '--no-stream', '--format', '{{ json . }}'])
                    .stdout('docker', ['stats', '--no-stream', '--format', '{\"container\":\"{{ .Container }}\",\"memory\":{\"raw\":\"{{ .MemUsage }}\",\"percent\":\"{{ .MemPerc }}\"},\"cpu\":\"{{ .CPUPerc }}\"}'])
                    .then(result => {
                        if (!result)
                            return; // No running containers

                        // Return an Array of Container Stats
                        return result.split('\n').map(JSON.parse);
                    });
            }

            // Undefined = no docker information found
        });
}

// ** Export
module.exports = getDockerInfo;