'use strict';

/**
 * Created by bradserbu on 5/10/17.
 * - Use SCP and SSH to install ASM:AGENT on remote server
 */

// ** Dependencies
const util = require('util');
const execa = require('execa');
const logger = require('nodus-framework').logging.createLogger();
const path = require('path');

// ** Constants
const INSTALL_DIR = '~/.asm-agent/';
const INSTALL_FILES = [
    path.join(__dirname + `/assets/collectd-5.7.1.tar.bz2`),
    path.join(__dirname + `/assets/install.sh`)
];

const SSH = (host, ...args) => execa.stdout('ssh', [host, ...args]);
const SCP = (host, dest, ...files) => execa.stdout('scp', [...files, `${host}:${dest}`]);

function deployAgent(host) {

    logger.info('Deploying agent...', {host: host});

    // const credentials = {};
    // const privateKey = logger.set(credentials, 'privateKey', privateKey, DEFAULT_PRIVATE_KEY);

    // const credentials = {
    //     username: username,
    //     password: password,
    //     privateKey: files.resolvePath(privateKey || DEFAULT_PRIVATE_KEY)
    // };

    // Destination to install agent
    const dir = INSTALL_DIR;

    // Files to install
    const files = INSTALL_FILES;

    // Create directory on agent
    return SSH(host, `mkdir -p ${dir}`)
        .then(() => SCP(host, dir, ...files))
        .then(() => SSH(host, `ls -lA ${dir}`));
}

// ** Exports
module.exports = deployAgent;