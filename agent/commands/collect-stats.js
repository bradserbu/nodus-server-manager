'use strict';
/**
 * Start a service to collect and send server stats at a specified interval
 */

// ** Constants
const DEFAULT_INTERVAL = 10 * 1000; // Collect stats every 10 seconds

// ** Dependencies
const program = require('nodus-framework').program;
const sendStats = require('./send-stats');
const errors = require('nodus-framework').errors;
const logger = require('nodus-framework').logging.createLogger();

function collectStats(interval) {
    interval = Number(interval || process.env['COLLECTOR_INTERVAL'] || DEFAULT_INTERVAL);

    logger.info("Starting collector...", interval);

    // Collect stats on a specified interval
    program.setTimer(interval, function sendStatsTimer() {
        logger.info("Collecting stats...");
        sendStats()
            .then(result => {
                logger.info("Status sent successfully...");
                logger.debug("RESULTS", result);
            })
            .catch(err => {
                logger.error(errors('COLLECTOR_ERROR', "Error collecting stats", err));
                logger.info("Shutting down collector...");

                program.shutdown();
            });
    });
}

module.exports = collectStats;