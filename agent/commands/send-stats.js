'use strict';

// ** Constants
const DEFAULT_COLLECTOR_HOST = 'localhost';
const DEFAULT_COLLECTOR_PORT = 8081;
const SEND_STATS_METHOD = 'PUT';
const SEND_STATS_PATH = 'api/stats';

// ** Dependencies
const request = require('request-promise');
const getStats = require('../getStats');
const errors = require('nodus-framework').errors;
const logger = require('nodus-framework').logging.createLogger();

function sendStats(host, port) {

    host = host || process.env['COLLECTOR_HOST'] || DEFAULT_COLLECTOR_HOST;
    port = port || process.env['COLLECTOR_PORT'] || DEFAULT_COLLECTOR_PORT;

    const method = SEND_STATS_METHOD;
    const path = SEND_STATS_PATH;

    const uri = `http://${host}:${port}/${path}`;

    return getStats()
        .then(stats => {
            logger.debug("Sending stats...", {
                method: method,
                uri: uri
            });

            // TODO: Save stats in InfluxDB
            return request({
                method: method,
                uri: uri,
                body: stats,
                json: true,
                gzip: true,
                time: true,
                // resolveWithFullResponse: true
            })
                // .then(response => {
                //     logger.trace('RESPONSE', response);
                //     return response.body;
                // });
        });
}

// ** Exports
module.exports = sendStats;