'use strict';

// ** Constants
const DEFAULT_INTERVAL = 10 * 1000;           // Collect every 10 seconds
const DEFAULT_TIMEOUT = 30 * 1000; // Default ms to wait for a connection
const DEFAULT_HOST = 'localhost';
const DEFAULT_PORT = 8081;

const CONNECTION_STATUS = {
    INITIALIZED: 'initialized',
    CONNECTING: 'connecting',
    CONNECTED: 'connected',
    CLOSING: 'closing',
    CLOSED: 'closed'
};

// ** Dependencies
const EventEmitter = require('nodus-framework').events;
const WebSocket = require('ws');
const Promise = require('bluebird');
const errors = require('nodus-framework').errors;
const logger = require('nodus-framework').logging.createLogger();

/**
 * Service to send stats to the collector service
 * - Uses WebSockets as communication layer
 *
 * Methods:
 * - connect: Attempt to establish a connection with the 'Collector Service'.
 * - disconnect: Disconnect from the connector service.
 * - send: Send a message to the Collector Service.
 *
 * Events:
 * - connected:
 * - disconnected:
 */
class Connection extends EventEmitter {
    constructor(host, port) {
        super();

        // Collector Host/Port
        this._host = host || process.env['COLLECTOR_HOST'];
        this._port = port || process.env['COLLECTOR_PORT'];

        this._status = CONNECTION_STATUS.INITIALIZED;
    }

    get isConnected() {
        return this._status === CONNECTION_STATUS.CONNECTED;
    }

    connect() {
        if (this.isConnected) {
            throw errors('ALREADY_CONNECTED', "connect() called when a connection is already established");
        }

        // Set status of connecting
        this._status = CONNECTION_STATUS.CONNECTING;

        // Open a new socket connection
        const url = `ws://${host}:${port}`;
        const socket = new WebSocket(url);
        socket.on('open', () => {
            logger.info("Connection established.");

            // Fire connected event
            this._isConnected = true;
            this._status = CONNECTION_STATUS.CONNECTED;
            self.emit('connected');
        });
        socket.on('close', () => {
            logger.info('Connection closed.');

            // Update status
            this._status = CONNECTION_STATUS.CLOSED;
            this._isConnected = false;
            this._socket = null;

            // Fire 'disconnected' event
            this.emit('disconnected');
        });
        socket.on('error', err => {
            logger.error('SOCKET_ERROR', 'An error occurred on the socket.', err);

            logger.debug('Socket error occurred, closing connection...');

            this.emit('error', err);
            this.disconnect();
        });
        socket.onmessage = msg => {
            logger.debug('RECEIVE_MESSAGE', msg);

            // Publish events sent from the server
            this.emit('receive', msg);
        };

        this._socket = socket;
    }

    disconnect() {
        if (this.isConnected) {
            throw errors("NOT_CONNECTED", "disconnect() called on a closed connection.");
        }

        // Update socket status
        this._status = CONNECTION_STATUS.CLOSING;
        this._socket.close();
    }

    /**
     * Returns a promise that resolves when a connection has been established.
     * - timeout: Number of ms to wait for a connection
     */
    waitForConnection(timeout) {

        timeout = Number(timeout || DEFAULT_TIMEOUT);

        // Check if we already have a connection
        return new Promise((resolve, reject) => {
            if (this.isConnected)
                return resolve(this);

            // Check if we in the process of closing a connection
            if (this._status === CONNECTION_STATUS.CLOSING || this._status.CLOSED) {
                logger.debug('waitForConnection() called when the connection is being closed...', this._status);
                return reject(errors('CONNECTION_CLOSED', 'waitForConnect() called with an invalid status', this._status));
            }

            // Someone else has already attempted a connection, just wait for it...
            if (this._status === CONNECTION_STATUS.CONNECTING) {
                logger.warn("Already waiting for a connection...");
            }

            logger.debug("Waiting for a connection...", {timeout: timeout});
            this.once('connected', () => {
                logger.debug("Connection established successfully...");
                resolve(this);
            });

            this.once('error', err => {
                logger.error('Error occurred while waiting for a connection...');
                reject(err);
            });
        })
            /////////////
            /// TIMEOUT
            .timeout(timeout, 'Timed out while waiting for a connection...');
    }
}

function createCollector(host, port, timeout, interval) {

    host = host || DEFAULT_HOST;
    port = port || DEFAULT_PORT;
    timeout = Number(timeout || DEFAULT_TIMEOUT);
    interval = Number(interval || DEFAULT_INTERVAL);

    const collector = new Connection();
    return collector.waitForConnection(timeout);
}

// ** Exports
module.exports = createCollector;