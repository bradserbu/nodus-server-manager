'use strict';

// ** Constants
const CHARTS_DEFINITIONS = '../data/charts.json';
const CHARTS_SERIES = '../data/charts-series.json';

// ** Dependencies
const _ = require('lodash');
const path = require('path');
const fs = require('fs-extra');
const Promise = require('bluebird');
const readJsonAsync = Promise.promisify(fs.readJson);
const errors = require('nodus-framework').errors;
const Action = require('nodus-framework').actions;
const logger = require('nodus-framework').logging.createLogger();
const metrics = require('../metrics/');
const getChart = require('./get');

function getSeries(metric) {

    const name = metric.name;
    const handler = metrics[name];
    const params = metric.parameters || {};

    return Action(handler)
        .mapNamedArguments()
        .run(params)
        .then(results => {
            // Transform results into 'series' that can be displayed in a NVD3 chart
            // Each instance becomes a set of {key:,values:[]} in the data returned

            const data = {};

            _.each(results, result => {

                const timestamp = new Date(result.time).valueOf(); // MS Epoch
                const host = result.host;
                const instance = result.instance;
                const value = result.value;

                const key = instance
                    ? `instance-${instance}`
                    : host;

                if (!data.hasOwnProperty(key))
                    data[key] = {
                        key: key,
                        values: []
                    };

                logger.debug("TIMESTAMP", {
                    timestamp: timestamp,
                    timestamp_date: new Date(timestamp),
                    time: result.time
                });

                data[key].values.push([timestamp, value]);
            });

            return _.values(data);
        });
}

function getChartSeries(id) {

    // const filepath = path.join(__dirname, CHARTS_SERIES);
    //
    // return readJsonAsync(filepath)
    //     .then(charts => charts[id]);

    return getChart(id)
        .then(chart => {
            logger.debug('CHART', chart);

            // If the chart has predefined series... Just use that
            if (chart.series) {
                logger.debug("Returning chart series...");
                return chart.series;
            }

            logger.debug("Getting series metrics...");
            // Get the metrics for the chart
            const metric = chart.metric;
            logger.debug('METRIC', metrics);

            // Run query to request metrics
            return getSeries(metric);
        });
}

// ** Exports
module.exports = getChartSeries;