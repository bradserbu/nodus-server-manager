'use strict';

// ** Constants
const CHARTS_DEFINITIONS = '../data/charts.json';

// ** Dependencies
const _ = require('lodash');
const path = require('path');
const fs = require('fs-extra');
const Promise = require('bluebird');
const readJsonAsync = Promise.promisify(fs.readJson);
const errors = require('nodus-framework').errors;
const logger = require('nodus-framework').logging.createLogger();

function getChart(id) {
    const filepath = path.join(__dirname, CHARTS_DEFINITIONS);

    return readJsonAsync(filepath)
        .then(charts => {
            logger.debug('CHARTS', charts);

            // Find chart by id
            const chart = _.find(charts, chart => chart.id === id);

            if (!chart)
                throw errors('CHART_NOT_FOUND', 'The chart definition could not be found in the database.', {id: id});

            return chart;
        })
}

// ** Dependencies
module.exports = getChart;