'use strict';

// ** Constants
const CHARTS_DEFINITIONS = '../data/charts.json';

// ** Dependencies
const path = require('path');
const fs = require('fs-extra');
const Promise = require('bluebird');
const readJsonAsync = Promise.promisify(fs.readJson);
const logger = require('nodus-framework').logging.createLogger();

function getCharts() {

    const filepath = path.join(__dirname, CHARTS_DEFINITIONS);
    logger.debug('CHARTS_FILE', filepath);

    return readJsonAsync(filepath);
}

// ** Exports
module.exports = getCharts;