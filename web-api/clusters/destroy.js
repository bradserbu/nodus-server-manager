'use strict';

const NSM_HOME = '~/.nsm';

// ** Dependencies
const _ = require('lodash');
const ObjectID = require('mongodb').ObjectID;
const database = require('../database');
const microtime = require('microtime');
const files = require('nodus-framework').files;
const Path = require('path');
const fs = require('fs-extra');
const Promise = require('bluebird');
const logger = require('nodus-framework').logging.createLogger();
const getCluster = require('./get');
const execa = require('execa');

function destroyResources(cluster) {
    const cluster_id = cluster.ID;
    logger.debug('DEPLOY_CLUSTER', cluster_id);

    const timestamp = microtime.now();

    const CLUSTER_DIR = files.resolvePath(Path.join(NSM_HOME, 'clusters', cluster_id));

    const UpdateStatus = status => {
        logger.debug('UPDATE_STATUS', status);
        return database
            .update('clusters', {
                id: cluster_id,
                status: status
            })
            .then(() => {
            });
    };
    const LeaveCluster = () => {
        logger.debug('Leaving Cluster...');
        const LOG_FILE = Path.join(CLUSTER_DIR, `${timestamp}-leave.log`);

        return execa(
            'ansible-playbook', ['-b', '-i', 'cluster-inventory', 'cluster-destroy.yml'], {
                cwd: CLUSTER_DIR,
                env: Object.assign({}, process.env, {
                    ANSIBLE_HOST_KEY_CHECKING: "False"
                })
            })
            .then(result => {
                    logger.debug(result.stdout);
                    return fs
                        .writeJson(LOG_FILE, result)
                        .then(() => result)
                })
            .catch(err => {
                logger.warn('ERROR LEAVING CLUSTER', err);
                logger.debug('Continuing to destroy resources...')
            });
    };
    const DestroyResources = () => {
        logger.debug('Destroying Resources...');
        const LOG_FILE = Path.join(CLUSTER_DIR, `${timestamp}-destroy.log`);

        return execa(
            'terraform', ['destroy', '-force'], {
                cwd: CLUSTER_DIR
            })
            .then(result => fs
                .writeJson(LOG_FILE, result)
                .then(() => result)
            )
            // Log output
            .then(result => {
                logger.debug('\n' + result.stdout + '\n');
                return result;
            });
    };

    return UpdateStatus('destroying')
        // .then(LeaveCluster)
        .then(DestroyResources)
        .then(() => UpdateStatus('destroyed'));
}

function destroyCluster(id) {
    logger.debug('DELETE_CLUSTER', id);

    // return database
    //     .remove('clusters', {_id: ObjectID(id)});
    return getCluster(id)
        .then(cluster => destroyResources(cluster))
}

module.exports = destroyCluster;