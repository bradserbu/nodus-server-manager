'use strict';

const NSM_HOME = '~/.nsm';

// ** Dependencies
const database = require('../database');
const microtime = require('microtime');
const logger = require('nodus-framework').logging.createLogger();
const deploy = require('./deploy');

function createCluster(cluster) {
    logger.debug('CREATE_CLUSTER', cluster);

    cluster.status = 'creating';

    return database
        .insert('clusters', cluster)
        .then(result => {

            deploy(result.insertedId);

            return result.insertedId
        })
}

module.exports = createCluster;