'use strict';

const NSM_HOME = '~/.nsm';

// ** Dependencies
const _ = require('lodash');
const database = require('../database');
const microtime = require('microtime');
const files = require('nodus-framework').files;
const Path = require('path');
const fs = require('fs-extra');
const Promise = require('bluebird');
const logger = require('nodus-framework').logging.createLogger();
const getCluster = require('./get');
const handlebars = require('handlebars');
const execa = require('execa');

function deploy(cluster) {

    const cluster_id = cluster.ID;
    logger.debug('DEPLOY_CLUSTER', cluster_id);

    const timestamp = microtime.now();

    const CLUSTER_DIR = files.resolvePath(Path.join(NSM_HOME, 'clusters', cluster_id));
    const CLUSTER_JSON = Path.join(CLUSTER_DIR, 'cluster.json');
    const LOG_FILE = Path.join(CLUSTER_DIR, `${timestamp}-deploy.log`);

    const TEMPLATE_DIR = Path.join(__dirname, `templates/cluster`);

    // ACTIVITIES
    const Prepare = () => {
        logger.debug('Creating Cluster Directory...');
        return fs
            .mkdirp(CLUSTER_DIR)
            .then(() => fs.writeJson(CLUSTER_JSON, cluster))
    };
    const CopyFiles = () => {
        logger.debug('Coping deployment files...');

        return fs
            .readdir(TEMPLATE_DIR)
            .then(files => _.map(files, filename => Path.join(TEMPLATE_DIR, filename)))
            .then(files =>
                Promise.all(_.map(
                    files,
                    filename => fs
                        .readFile(filename, {encoding: 'utf8'})
                        .then(contents => {
                            const ext = Path.extname(filename);
                            logger.trace('EXT', ext);
                            if (ext === '.tf') {
                                logger.trace('COMPILE_TEMPLATE', filename);
                                // Compile Template
                                return Promise
                                    .resolve(handlebars.compile(contents))
                                    .then(template => template({cluster}));
                            } else {
                                return contents;
                            }
                        })
                        .then(contents => {
                            const dest = Path.join(CLUSTER_DIR, Path.basename(filename));

                            logger.trace('WRITE_FILE', dest);
                            return fs.writeFile(dest, contents);
                        })))
            );
    };
    const UpdateStatus = status => {
        logger.debug('UPDATE_STATUS', status);
        return database
            .update('clusters', {
                id: cluster_id,
                status: status
            })
            .then(() => {
            });
    };
    const Provision = () => {
        logger.debug('Provisioning Resources...');

        return execa(
            'terraform', ['apply'], {
                cwd: CLUSTER_DIR
            })
            .then(result => fs
                .writeJson(LOG_FILE, result)
                .then(() => result)
            )
            // Log output
            .then(result => {
                logger.debug('\n' + result.stdout + '\n');
                return result;
            });
    };
    const Configure = () => {
        logger.debug('Configuring Cluster Environment...');
        const LOG_FILE = Path.join(CLUSTER_DIR, `${timestamp}-setup.log`);

        return execa(
            'ansible-playbook', ['-b', '-i', 'cluster-inventory', 'cluster.yml'], {
                cwd: CLUSTER_DIR,
                env: Object.assign({}, process.env, {
                    ANSIBLE_HOST_KEY_CHECKING: "False"
                })
            })
            .then(result => {
                    logger.debug(result.stdout);
                    return fs
                        .writeJson(LOG_FILE, result)
                        .then(() => result)
                }
            );
    };

    // Run Deployment
    return UpdateStatus('deploying')
        .then(Prepare)
        .then(CopyFiles)
        .then(Provision)
        .then(Configure)
        .then(() => UpdateStatus('online'));
}

function deployEndpoint(args, send) {

    const id = args.id;

    return database
        .findOne('clusters', id)
        .then(cluster => {
            return deploy(cluster);
        });
}

// ** Exports
module.exports = id => getCluster(id)
    .then(cluster => deploy(cluster));

module.exports.endpoint = deployEndpoint;