#!/usr/bin/env bash

set -e;

SWARM_SSH=`terraform output master-ssh`

ssh $SWARM_SSH docker "$@"