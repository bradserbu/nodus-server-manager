#!/usr/bin/env bash

set -e;

echo "---------------------------------"
echo "-- PLANNING DEPLOYMENT ----------"
echo "---------------------------------"
terraform plan

echo "---------------------------------"
echo "-- CALCULATING DEPENDENCIES -----"
echo "---------------------------------"
sleep 2;
terraform graph
