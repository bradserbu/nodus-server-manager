##Create Swarm Master Instance
resource "aws_instance" "{{cluster.name}}-master" {
  key_name               = "{{cluster.name}}-master"
  depends_on             = ["aws_security_group.cluster_sg"]
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "${var.aws_instance_size}"
  vpc_security_group_ids = ["${aws_security_group.cluster_sg.id}"]
  key_name               = "${var.aws_key_name}"
  tags {
    Name = "{{cluster.name}}-master"
  }
}