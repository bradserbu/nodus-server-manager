##General vars
variable "ssh_user" {
  default = "ubuntu"
}

variable "public_key_path" {
  default = "/Users/bradserbu/.ssh/id_rsa.pub"
}

variable "private_key_path" {
  default = "/Users/bradserbu/.ssh/id_rsa"
}

variable "aws_region" {
  default = "us-east-1"
}

##AWS Specific Vars
variable "aws_worker_count" {
  default = {{cluster.nodes}}
}

variable "aws_key_name" {
  default = "{{cluster.config.aws_key_name}}"
}

variable "aws_instance_size" {
  default = "{{cluster.size}}"
}
