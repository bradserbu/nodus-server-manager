#!/usr/bin/env bash

set -e;

echo "---------------------------------"
echo "-- CONNECTING TO MASTER NODE ----"
echo "---------------------------------"
SWARM_SSH=`terraform output master-ssh`

ssh $SWARM_SSH "$@"