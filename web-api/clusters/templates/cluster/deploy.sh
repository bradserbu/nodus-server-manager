#!/usr/bin/env bash

set -e;

echo "---------------------------------"
echo "-- PROVISIONING INFRASTRUCTURE --"
echo "---------------------------------"
terraform apply

echo "---------------------------------"
echo "-- DEPLOYING SYSTEM RESOURCES ---"
echo "---------------------------------"
sleep 5;
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -b -i cluster-inventory cluster.yml

echo "---------------------------------"
echo "-- INSTALLING APPLICATION -------"
echo "---------------------------------"
sleep 2;
CLUSTER_MASTER=`terraform output master-ip`
echo "$CLUSTER_MASTER"

./run.sh node ls