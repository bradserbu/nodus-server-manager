resource "null_resource" "ansible-provision" {
  depends_on = ["aws_instance.{{cluster.name}}-master", "aws_instance.{{cluster.name}}-worker"]

  provisioner "local-exec" {
    command = "echo \"[master]\" > cluster-inventory"
  }

  provisioner "local-exec" {
    command = "echo \"${format("%s ansible_ssh_user=%s", aws_instance.{{cluster.name}}-master.0.public_ip, var.ssh_user)}\" >> cluster-inventory"
  }

  provisioner "local-exec" {
    command = "echo \"[workers]\" >> cluster-inventory"
  }

  provisioner "local-exec" {
    command = "echo \"${join("\n",formatlist("%s ansible_ssh_user=%s", aws_instance.{{cluster.name}}-worker.*.public_ip, var.ssh_user))}\" >> cluster-inventory"
  }
}
