output "master-ip" {
  value = "${aws_instance.{{cluster.name}}-master.public_ip}"
}

output "master-ssh" {
  value = "${var.ssh_user}@${aws_instance.{{cluster.name}}-master.public_ip}"
}