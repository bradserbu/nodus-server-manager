#!/usr/bin/env bash

set -e;
echo "---------------------------------"
echo "-- SHUTTING DOWN RESOURCES ------"
echo "---------------------------------"
sleep 2;
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -b -i cluster-inventory cluster-destroy.yml

echo "---------------------------------"
echo "-- DESTROYING INFRASTRUCTURE ----"
echo "---------------------------------"
sleep 2;
terraform destroy -force

# Remove Swarm Inventory
rm cluster-inventory
