##Create AWS Swarm Workers
resource "aws_instance" "{{cluster.name}}-worker" {
  depends_on             = ["aws_security_group.cluster_sg"]
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "${var.aws_instance_size}"
  vpc_security_group_ids = ["${aws_security_group.cluster_sg.id}"]
  key_name               = "${var.aws_key_name}"
  count                  = "${var.aws_worker_count}"
  tags {
    Name = "{{cluster.name}}-worker-${count.index}"
  }
}