'use strict';

// ** Dependencies
const _ = require('lodash');
const database = require('../database');

function listClusters() {
    return database
        .all('clusters')
        .then(clusters => _.reject(clusters, cluster => cluster.status === 'destroyed'));
}

// ** Exports
module.exports = listClusters;