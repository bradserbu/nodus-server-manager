'use strict';

// ** Dependencies
const _ = require('lodash');
const database = require('../database');

function listProviders() {
    return database
        .all('providers');
}

// ** Exports
module.exports = listProviders;