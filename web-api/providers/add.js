'use strict';

// ** Dependencies
const _ = require('lodash');
const database = require('../database');

function addProvider(name, type, config) {

    const provider = {
        name: name,
        type: type,
        config: config
    };

    return database
        .insert('providers', provider);
}

// ** Exports
module.exports = addProvider;