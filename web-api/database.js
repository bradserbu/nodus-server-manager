'use strict';

const DEFAULT_MONGO_URL = 'mongodb://nodus.cloud:27017';

// ** Dependencies
const _ = require('lodash');
const util = require('util');
const Promise = require('bluebird');
const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
const errors = require('nodus-framework').errors;
const logger = require('nodus-framework').logging.createLogger();

/**
 * Return a promise that will connect to a mongodb
 * @param url
 * @returns {Promise.<T>}
 */
function connect() {

    const url = process.env['MONGO_URL'] || DEFAULT_MONGO_URL;

    const connectAsync = Promise.promisify(MongoClient.connect);

    return connectAsync(url)
    // Close DB Connection after use
        .disposer((connection) => {
            logger.debug("Closing database connection...");
            connection.close();
        });
}

function all(entity) {
    return Promise.using(connect(), db => {
        const collection = db.collection(entity);

        logger.debug('Getting all documents...', entity);
        return collection.find({}).toArray();
    });
}

const id_fields = ['id', '_id'];
function Id(data) {

    for (let lcv = 0; lcv < id_fields.length; lcv++) {
        const field_name = id_fields[lcv];

        if (data.hasOwnProperty(field_name))
            return data[field_name];
    }
}

function upsert(entity, data, filter) {

    // If filter is empty then try to locate an identifier in the data
    if (util.isNullOrUndefined(filter)) {

        const id_field = Id(data);
        if (!id_field)
            throw errors('ARGUMENT_ERROR', 'Upserts without a "filter" must have a _id in the data', {
                entity: entity,
                data: data,
                filter: filter
            });

        filter = {_id: ObjectId(id_field)};
    }

    return Promise.using(connect(), db => {
        const collection = db.collection(entity);

        logger.debug('Upserting document...', data);
        return collection.updateOne(filter, data, {upsert: true});
    });
}

function update(entity, data, filter) {

    // If filter is empty then try to locate an identifier in the data
    if (util.isNullOrUndefined(filter)) {

        const id_field = Id(data);
        if (!id_field)
            throw errors('ARGUMENT_ERROR', 'Updates without a "filter" must have a _id in the data', {
                entity: entity,
                data: data,
                filter: filter
            });

        filter = {_id: ObjectId(id_field)};
    }

    return Promise.using(connect(), db => {
        const collection = db.collection(entity);

        logger.debug('Updating document...', data);
        return collection.updateOne(filter, {$set: data});
    });
}

function push(entity, filter, values) {

    // If filter is empty then try to locate an identifier in the data
    const id_field = Id(filter);
    if (!id_field)
        throw errors('ARGUMENT_ERROR', 'Updates without a "filter" must have a _id in the data', {
            entity: entity,
            filter: filter
        });

    filter = {_id: ObjectId(id_field)};

    return Promise.using(connect(), db => {
        const collection = db.collection(entity);

        logger.debug('Updating document...', values);
        return collection.updateOne(filter, {$push: values});
    });
}


function findOne(entity, filter) {

    return Promise.using(connect(), db => {

        const collection = db.collection(entity);

        logger.debug('Finding document...', filter);
        return collection.findOne(filter);
    });
}

function find(entity, filter) {

    return Promise.using(connect(), db => {

        const collection = db.collection(entity);

        logger.debug('Finding document...', filter);
        return collection.find(filter).toArray();
    });
}


function insert(entity, data) {
    return Promise.using(connect(), db => {
        const collection = db.collection(entity);

        logger.debug('Inserting document...', data);
        return collection.insertOne(data);
    });
}

function remove(entity, filter) {
    return Promise.using(connect(), db => {
        const collection = db.collection(entity);

        logger.debug('Removing document...', filter);
        return collection.removeOne(filter);
    });
}

// ** Exports
module.exports.all = all;
module.exports.upsert = upsert;
module.exports.update = update;
module.exports.find = find;
module.exports.findOne = findOne;
module.exports.insert = insert;
module.exports.remove = remove;
module.exports.push = push;

module.exports.remove_with_account_id = (entity, account_id) { return remove(entity, { account_id: account_id || entity.ACCOUNT_ID }); }  
