'use strict';

// ** Constants
const DASHBOARD_DEFINITION = '../data/server-dashboard.json';

// ** Dependencies
const path = require('path');
const fs = require('fs-extra');
const Promise = require('bluebird');
const readJsonAsync = Promise.promisify(fs.readJson);
const logger = require('nodus-framework').logging.createLogger();

function getDashboard() {

    const dashboard_file = path.join(__dirname, DASHBOARD_DEFINITION);
    logger.debug('DASHBOARD_FILE', dashboard_file);

    return readJsonAsync(dashboard_file);
}

// ** Exports
module.exports = getDashboard;