'use strict';

const NSM_HOME = '~/.nsm';

// ** Dependencies
const _ = require('lodash');
const fs = require('fs-extra');
const Path = require('path');
const getServer = require('./get');
const files = require('nodus-framework').files;
const Promise = require('bluebird');
const execa = require('execa');
const microtime = require('microtime');
const logger = require('nodus-framework').logging.createLogger();
const database = require('../database');

/**
 * Install required packages to provision software on a server instance.
 * @param server
 */
function install(server, packageName) {
    logger.debug('INSTALL_PACKAGE', server, packageName);

    const timestamp = microtime.now();

    const server_id = server.ID;
    logger.debug('SERVER_ID', server_id);

    const WORKING_DIR = files.resolvePath(Path.join(NSM_HOME, `servers/${server_id}`));
    logger.debug('WORKING_DIR', WORKING_DIR);

    const PACKAGE_DIR = files.resolvePath(Path.join(NSM_HOME, `servers/${server_id}`, packageName));
    logger.debug('PACKAGE_DIR', PACKAGE_DIR);

    // Ansible Software Templates
    const SOFTWARE_TEMPLATE_DIR = Path.join(__dirname, `../software/packages/${packageName}`);
    logger.debug('SOFTWARE_TEMPLATE_DIR', WORKING_DIR);

    const def = fs.readJsonSync(Path.join(SOFTWARE_TEMPLATE_DIR, 'package.json'));
    logger.debug('DEFINITION', def);

    const copySoftwarePackage = () => {
        logger.debug('Copying software package...');

        const files = (def.files || []);
        files.push('package.json');
        files.push('package.yml');

        logger.debug('FILES', files);

        return Promise
            .all(_.map(
                files,
                filename => {
                    logger.debug('COPY', filename);
                    const src = Path.join(SOFTWARE_TEMPLATE_DIR, filename);
                    const dest = Path.join(PACKAGE_DIR, filename);

                    logger.debug('COPY:' + filename, {src, dest});
                    return fs
                        .copy(src, dest)
                })
            );
    };

    const runPlaybook = (playbook) => {
        logger.debug('Running Ansible Playbook...', {playbook});

        // ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -b -i hosts setup.yml
        const LOG_FILE = Path.join(WORKING_DIR, `${timestamp}-run-playbook-${playbook}-log`);

        return execa(
            'ansible-playbook', ['-b', '-i', '../hosts', playbook], {
                cwd: PACKAGE_DIR,
                env: Object.assign({}, process.env, {
                    ANSIBLE_HOST_KEY_CHECKING: "False"
                })
            })
            .then(result => {
                    logger.debug(result.stdout);
                    return fs
                        .writeJson(LOG_FILE, result)
                        .then(() => result)
                }
            );
    };

    const installDependencies = () => {

        const dependencies = def.dependencies || [];

        return Promise
            .all(_.map(
                dependencies,
                dependency => {
                    logger.debug('INSTALL_DEPENDENCY', dependency);
                    return install(server, dependency);
                })
            );
    };

    return fs
        .mkdirp(PACKAGE_DIR)
        .then(() => copySoftwarePackage())
        .then(() => installDependencies())
        .then(() => runPlaybook(`package.yml`))
        .then(() => {

            return database
                .upsert('servers-apps', {
                        server_id: server_id,
                        name: packageName,
                        version: def.version,
                        installedOn: new Date()
                    },
                    {
                        server_id: server_id,
                        name: packageName
                    }
                )
                .then(() => {

                })
        });
}

function installPackage(server_id, packageName) {
    return getServer(server_id)
        .then(server => install(server, packageName));
}

module.exports = installPackage;