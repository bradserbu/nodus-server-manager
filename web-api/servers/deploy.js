'use strict';

const NSM_HOME = '~/.nsm';

// ** Dependencies
const _ = require('lodash');
const database = require('../database');
const Path = require('path');
const fs = require('fs-extra');
const handlebars = require('handlebars');
const files = require('nodus-framework').files;
const Promise = require('bluebird');
const execa = require('execa');
const logger = require('nodus-framework').logging.createLogger();
const microtime = require('microtime');
const jobs = require('../../jobs');
const getServer = require('./get');
const provisionServer = require('./provision');
const installPackage = require('./install');

function deploy (server_id, server) {

    logger.debug('DEPLOY_SERVER', {server_id, server});

    const timestamp = microtime.now();

    const WORKING_DIR = files.resolvePath(Path.join(NSM_HOME, 'servers', server_id));
    const SERVER_JSON = Path.join(WORKING_DIR, 'server.json');
    const TERRAFORM_SCRIPT = Path.join(WORKING_DIR, 'server.tf');
    const LOG_FILE = Path.join(WORKING_DIR, `${timestamp}-create-log`);

    // Terraform Templates
    const providerType = server.provider.type;
    const TEMPLATE_DIR = Path.join(__dirname, `templates/${providerType}`);

    const template_files = {
        provider: Path.join(TEMPLATE_DIR, `provider.tf`),
        os: Path.join(TEMPLATE_DIR, `/os/${server.os.name}.tf`),
        network: Path.join(TEMPLATE_DIR, 'network.tf'),
        instance: Path.join(TEMPLATE_DIR, 'instance.tf'),
        inventory: Path.join(TEMPLATE_DIR, 'inventory.tf'),
        output: Path.join(TEMPLATE_DIR, 'output.tf')
    };

    // Compile Terraform scripts
    const build_templates = () => Promise
        .props(_.mapValues(
            template_files,
            filename => fs
                .readFile(filename, {encoding: 'utf8'})
                .then(contents => handlebars.compile(contents))
                .then(template => template(server))
        ));

    logger.debug('SERVER_WORKING_DIR', WORKING_DIR);

    return fs
        .mkdirp(WORKING_DIR)
        // Save Definition to server.json file...
        .then(() => fs.writeJson(SERVER_JSON, server))
        .then(build_templates)
        // Join templates together into one terraform.tf script file
        .then(templates => _
            .map(templates, (section, name) => `# -- ${name}\n${section}\n`)
            .join('\n'))
        // Save terraform deploy script to working directory
        .then(terraform_script => fs
                .writeFile(TERRAFORM_SCRIPT, terraform_script)
            // .then(() => terraform_script)
        )
        // Run Terraform 'apply' to deploy the server
        .then(() => execa(
            'terraform', ['apply'], {
                cwd: WORKING_DIR
            })
            .then(result => fs
                .writeJson(LOG_FILE, result)
                .then(() => result)
            )
        )
        // Save Public IPAddress to database
        .then(() => execa(
            'terraform', ['output', 'ipAddress'], {
                cwd: WORKING_DIR
            })
            .then(result => {
                const ipAddress = result.stdout.trim();
                logger.debug('IPADDRESS', ipAddress);

                return database.update('servers', {
                    id: server_id,
                    ipAddress: ipAddress
                });
            }));
}

// ** Register handler to provision job
jobs.process('deploy-server', deploy);

/**
 * Install Software Provisioning Components
 */
jobs.process('provision-server', (server_id) => {
    return provisionServer(server_id);
});

/**
 * Install Software Provisioning Components
 */
jobs.process('install-nsm-agent', (server_id) => {
    return installPackage(server_id, 'nsm-agent');
});


function deployServer(args, send) {
    logger.debug('ARGUMENTS', arguments);

    const server_id = args.id;

    logger.debug('SERVER_ID', server_id);

    send('update-activity', {
        name: 'deploy-server',
        status: 'running',
        message: 'Provisioning server resources...',
        started_on: new Date()
    });

    const simulateActivity = (name, delay, message) => {
        logger.debug('SIMULATE_ACTIVITY', name);
        send('update-activity', {
            name: name,
            status: 'running',
            message: message,
            started_on: new Date()
        });

        return Promise
            .delay(delay)
            .then(() => send('update-activity', {
                name: name,
                status: 'completed',
                completed_on: new Date()
            }));
    };

    const runActivity = (name, message, handler) => {
        logger.debug('RUN_ACTIVITY', name);
        send('update-activity', {
            name: name,
            status: 'running',
            message: message,
            started_on: new Date()
        });

        return handler()
            .then(() => send('update-activity', {
                name: name,
                status: 'completed',
                completed_on: new Date()
            }));
    };

    getServer(server_id)
        .tap(server => database.update('servers', {
            id: server_id,
            status: 'deploying'
        }))
        .then(server => jobs.run('deploy-server', {
            server_id: server._id,
            server: server
        }))
        .then(() => send('update-activity', {
            name: 'deploy-server',
            status: 'completed',
            completed_on: new Date()
        }))
        // .then(() => simulateActivity('install-os', 1500, 'Installing operating system...'))
        .then(() => runActivity('install-os', 'Provisioning operating system...', () => Promise
            .delay(5000)    // Wait for server to come online
            .then(() => jobs.run('provision-server', {
                server_id
            })))
        )
        .then(() => runActivity('install-agent', 'Installing "NODUS: Server Agent"...', () => jobs.run('install-nsm-agent', {
            server_id
        })))
        .then(() => simulateActivity('restart-server', 3000, 'Restarting server...'))
        .then(() => simulateActivity('server-online', 5000, 'Waiting for server to come online...'))
        .tap(() => database.update('servers', {
            id: server_id,
            status: 'online'
        }));
}

module.exports = deployServer;
module.exports.deploy = deploy;