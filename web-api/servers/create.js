'use strict';

const NSM_HOME = '~/.nsm';

// ** Dependencies
const _ = require('lodash');
const database = require('../database');
const Path = require('path');
const fs = require('fs-extra');
const handlebars = require('handlebars');
const files = require('nodus-framework').files;
const Promise = require('bluebird');
const execa = require('execa');
const logger = require('nodus-framework').logging.createLogger();
const microtime = require('microtime');
const jobs = require('../../jobs');

function createServer(server) {
    logger.debug('ARGUMENTS', arguments);

    // Set the server status to provisioning
    server.status = 'deploy';

    return database
        .insert('servers', server)
        .then(result => result.insertedId.toString());
}

module.exports = createServer;