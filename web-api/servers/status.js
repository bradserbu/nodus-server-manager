'use strict';

// ** Dependencies
const logger = require('nodus-framework').logging.createLogger();

function serverStatus(params, send) {
    logger.debug('SERVER_STATUS', arguments);

    const server_id = params.id;



    setTimeout(() => send('update-activity', {
        name: 'install-os',
        status: 'running',
        message: 'Status message from server...',
        started_on: new Date()
    }), 5000);
}

// ** Exports
module.exports = serverStatus;