output "ipAddress" {
  value = "${aws_instance.{{name}}.public_ip}"
}

output "ssh" {
  value = "{{os.ssh_user}}@${aws_instance.{{name}}.public_ip}"
}