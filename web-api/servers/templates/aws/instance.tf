##Create Swarm Master Instance
resource "aws_instance" "{{name}}" {
  depends_on             = ["aws_security_group.{{name}}_sg"]
  ami                    = "${data.aws_ami.{{os.name}}.id}"
  instance_type          = "{{instance.size}}"
  key_name               = "{{provider.config.aws_key_name}}",
  vpc_security_group_ids = ["${aws_security_group.{{name}}_sg.id}"]
  tags {
    Name = "{{name}}"
  }
}