resource "null_resource" "inventory" {
  depends_on = ["aws_instance.{{name}}"]

  provisioner "local-exec" {
    command = "echo \"[hosts]\" > hosts"
  }

  provisioner "local-exec" {
    command = "echo \"${format("%s ansible_ssh_user=%s", aws_instance.{{name}}.public_ip, "{{os.ssh_user}}")}\" >> hosts"
  }

//  provisioner "remote-exec" {
//    inline = [
//      "hostname -f > hostname"
//    ]
//  }
}