provider "aws" {
  access_key = "{{provider.config.aws_access_key}}"
  secret_key = "{{provider.config.aws_secret_key}}"
  region = "{{provider.config.aws_region}}"
}