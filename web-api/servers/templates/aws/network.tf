##Create swarm security group
resource "aws_security_group" "{{name}}_sg" {
  name = "{{name}}_sg"
  description = "Firewall Security Rules for Instance"
  {{#each network.ingress}}

  ingress {
    from_port = {{incoming}}
    to_port = {{outgoing}}
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  {{/each}}

  # Allow External Internet Access
  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
}