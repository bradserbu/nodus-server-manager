'use strict';

// ** Dependencies
const database = require('../database');
const request = require('request-promise');
const getServer = require('./get');

// ** Exports
function getStats(id) {

    // Fetch the server details from the databases then forward the request to the appropriate ipAddress
    // TODO: Maintain cache of server->ipaddress for perforamnce
    // TODO: This should be moved to websocket communication layer for performance
    return getServer(id)
        .then(server => {

            // Get Ip Address from server details
            const ipAddress = server.ipAddress;
            // const ipAddress = 'localhost';
            // logger.debug('SERVER_IP', ipAddress);

            return request
                .get({
                    method: 'GET',
                    json: true,
                    uri: `http://${ipAddress}:8081/api/stats`
                })
        })
        // The response would have a nested .data.response.data portion so we just use the data from the target server
        .then(response => response.data);
}

// ** Exports
module.exports = getStats;