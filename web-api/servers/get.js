'use strict'

// ** Dependencies
const ObjectID = require('mongodb').ObjectID;
const database = require('../database');
const logger = require('nodus-framework').logging.createLogger();
const Server = require('../model/Server');

function getById(id) {
    logger.debug('GET_SERVER', id);

    return database
        .findOne('servers', {
            _id: ObjectID(id)
        })
        .then(data => {
            logger.debug('SERVER_DATA', data);
            const server = Server(data);

            logger.debug('SERVER', server);
            return server;
        });
}

module.exports = getById;