'use strict';

// ** Dependencies
const _ = require('lodash');
const microtime = require('microtime');
const jobs = require('../../jobs');
const installPackage = require('./install');
const Promise = require('bluebird');
const logger = require('nodus-framework').logging.createLogger();

/**
 * Install Software Provisioning Components
 */
jobs.process('install-package', (server_id, packageName) => {
    logger.debug('JOB:INSTALL_PACKAGE', {server_id, packageName});
    return installPackage(server_id, packageName);
});

function installEndpoint(args, send) {

    const server_id = args.server_id;
    const packageName = args.packageName;

    logger.debug('INSTALL_ENDPOINT', {server_id, packageName});

    const simulateActivity = (name, delay, message) => {
        logger.debug('SIMULATE_ACTIVITY', name);
        send('update-activity', {
            name: name,
            status: 'running',
            message: message,
            started_on: new Date()
        });

        return Promise
            .delay(delay)
            .then(() => send('update-activity', {
                name: name,
                status: 'completed',
                completed_on: new Date()
            }));
    };

    const runActivity = (name, message, handler) => {
        logger.debug('RUN_ACTIVITY', name);
        send('update-activity', {
            name: name,
            status: 'running',
            message: message,
            started_on: new Date()
        });

        return handler()
            .then(() => send('update-activity', {
                name: name,
                status: 'completed',
                completed_on: new Date()
            }));
    };

    return Promise
        .resolve()
        .then(() => simulateActivity('install-deps', 3000, 'Installing Application Dependencies...'))
        // .then(() => simulateActivity('install-app', 4000, 'Installing Application Package...'))
        .then(() => runActivity('install-app', 'Installing Application Package...', () => jobs.run('install-package', {
            server_id,
            packageName
        })))
        .then(() => simulateActivity('configure-app', 1000, 'Configuring Application Settings...'))
        .then(() => simulateActivity('start-app', 2000, 'Starting Application...'));
}

// ** Exports
module.exports = installEndpoint;