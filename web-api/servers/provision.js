'use strict';

const NSM_HOME = '~/.nsm';

// ** Dependencies
const _ = require('lodash');
const fs = require('fs-extra');
const Path = require('path');
const getServer = require('./get');
const files = require('nodus-framework').files;
const Promise = require('bluebird');
const execa = require('execa');
const microtime = require('microtime');
const logger = require('nodus-framework').logging.createLogger();

/**
 * Install required packages to provision software on a server instance.
 * @param server
 */
function setup(server) {
    logger.debug('SETUP_SERVER', server);

    const timestamp = microtime.now();

    const server_id = server.ID;
    logger.debug('SERVER_ID', server_id);

    const WORKING_DIR = files.resolvePath(Path.join(NSM_HOME, `servers/${server_id}`));
    logger.debug('WORKING_DIR', WORKING_DIR);

    // Ansible Software Templates
    const OS_TYPE = server.os.type;
    const SOFTWARE_TEMPLATE_DIR = Path.join(__dirname, `../software/os/${OS_TYPE}`);
    logger.debug('SOFTWARE_TEMPLATE_DIR', WORKING_DIR);

    const core_software_files = {
        "cfg": {
            src: Path.join(SOFTWARE_TEMPLATE_DIR, 'ansible.cfg'),
            dest: Path.join(WORKING_DIR, 'ansible.cfg')
        },
        "core": {
            src: Path.join(SOFTWARE_TEMPLATE_DIR, 'setup.yml'),
            dest: Path.join(WORKING_DIR, 'setup.yml')
        }
    };

    const copySetupFiles = () => {
        logger.debug('Copying core software files...');
        return Promise
            .all(_
                .map(core_software_files,
                    ({src, dest}, name) => {
                        logger.debug('COPY:' + name, {src, dest});
                        return fs
                            .copy(src, dest)
                    })
            );
    };

    const runPlaybook = (playbook) => {
        logger.debug('Running Ansible Playbook...', {playbook});

        // ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -b -i hosts setup.yml
        const LOG_FILE = Path.join(WORKING_DIR, `${timestamp}-run-playbook-${playbook}-log`);

        return execa(
            'ansible-playbook', ['-b', '-i', 'hosts', playbook], {
                cwd: WORKING_DIR,
                env: Object.assign({}, process.env, {
                    ANSIBLE_HOST_KEY_CHECKING: "False"
                })
            })
            .then(result => fs
                .writeJson(LOG_FILE, result)
                .then(() => result)
            );

    };

    return copySetupFiles()
        .then(() => runPlaybook('setup.yml'))
}

function provisionServer(server_id) {
    return getServer(server_id)
        .then(server => setup(server));
}

module.exports = provisionServer;