'use strict';

const NSM_HOME = '~/.nsm';

// ** Dependencies
const _ = require('lodash');
const util = require('util');
const database = require('../database');
const Path = require('path');
const fs = require('fs-extra');
const handlebars = require('handlebars');
const files = require('nodus-framework').files;
const errors = require('nodus-framework').errors;
const Promise = require('bluebird');
const execa = require('execa');
const logger = require('nodus-framework').logging.createLogger();
const microtime = require('microtime');
const jobs = require('../../jobs');

function terraformDestroy(server_id) {

    logger.debug('DESTROY_SERVER', server_id);

    const timestamp = microtime.now();

    const WORKING_DIR = files.resolvePath(Path.join(NSM_HOME, 'servers', server_id));
    const LOG_FILE = Path.join(WORKING_DIR, `${timestamp}-destroy-log`);

    logger.debug('Terraform destory...');
    return execa(
        'terraform', ['destroy', '-force'],
        {cwd: WORKING_DIR}
    )
        .then(result => fs
            .writeJson(LOG_FILE, result)
            .then(() => result.stdout)
        );
}

function ServerID(name) {
    logger.debug('ARGUMENTS', arguments);

    return database
        .findOne('servers', {name: name})
        .tap(server => {
            if (util.isNullOrUndefined(server))
                throw errors('SERVER_NOT_FOUND', {name});
        })
        .then(server => server._id.toString())
}

jobs.process('destroy-server', server_id => terraformDestroy(server_id));

function destroyServer(args, send) {

    const simulateActivity = (name, delay, message) => {
        logger.debug('SIMULATE_ACTIVITY', name);
        send('update-activity', {
            name: name,
            status: 'running',
            message: message,
            started_on: new Date()
        });

        return Promise
            .delay(delay)
            .then(() => send('update-activity', {
                name: name,
                status: 'completed',
                completed_on: new Date()
            }));
    };

    logger.debug('ARGUMENTS', arguments);

    const server_id = args.id;

    logger.debug('SERVER_ID', server_id);
    database
        .update('servers', {
            id: server_id,
            status: 'destroying'
        })
        .then(() => {
            send('update-activity', {
                name: 'stop-services',
                status: 'running',
                message: 'Stopping active processes...',
                started_on: new Date()
            });
        })
        .delay(3000)
        .then(() => simulateActivity('stop-services', 2000, 'Stopping services...'))
        .then(() => simulateActivity('shutdown-server', 2000, 'Shutting down server resources...'))
        .then(() => {
            send('update-activity', {
                name: 'destroy-server',
                status: 'running',
                message: 'Destroying server resources...',
                started_on: new Date()
            });
        })
        .then(() => jobs.run('destroy-server', {
            server_id: server_id
        }))
        .then(() => database
            .update('servers', {
                id: server_id,
                status: 'destroyed'
            }))
        .then(() => send('update-activity', {
            name: 'destroy-server',
            status: 'completed',
            completed_on: new Date()
        }));
}

module.exports = destroyServer;