'use strict'

// ** Dependencies
const ObjectID = require('mongodb').ObjectID;
const database = require('../database');
const logger = require('nodus-framework').logging.createLogger();
const Server = require('../model/Server');

function getApps(id) {
    logger.debug('GET_SERVER', id);

    return database
        .find('servers-apps', {
            server_id: id
        });
}

module.exports = getApps;