'use strict';

// ** Dependencies
const _ = require('lodash');
const database = require('../database');
const bytes = require('bytes');
const flat = require('flat');

function flatten(obj) {
    const flattened = flat(obj, {
        safe: true
    });

    return _.mapKeys(flattened, (value, key) => key.replace('.', '_'));
}

function listServers() {
    return database
        .all('servers')
        .then(servers => _.reject(servers, server => server.status === 'destroyed'))
        // Add 'id' property as the string value of the MongoDB id
        .then(servers => _.each(servers, server => server.id = server._id.toString()));
        // .then(servers => servers.map(server => {
        //     // Select just specific information about the server to support the WebUI
        //     const data = {
        //         id: server._id.toString(),
        //         name: server.host,
        //         tags: server.tags.join(','),
        //         hostname: server.info.hostname,
        //         ipAddress: server.ipAddress,
        //         isVirtual: server.info.runningInVM,
        //         isContainer: server.info.system.model === 'Docker Container',
        //         os: server.info.os,
        //         system: server.info.system,
        //         cpu: server.info.cpu,
        //         memory: bytes(server.info.memory.total),
        //         cpuCache: server.info.cpuCache
        //     };
        //
        //     const ret = flatten(data);
        //
        //     return ret;
        // }))
}

// ** Exports
module.exports = listServers;