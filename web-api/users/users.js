'use strict';

// ** Dependencies
const gravatar = require('gravatar');
const database = require('../database');
const errors = require('nodus-framework').errors;
const logger = require('nodus-framework').logging.createLogger();

/**
 * Locate a user by his username
 * @param username
 */
function findUser(username) {

    return database.findOne('Users', {
        username: username
    });
}

/**
 * Check if username/password is valid for a user.
 * @param username
 * @param password
 */
function authenticate(username, password) {
    logger.debug('AUTHENTICATE', {
        username: username,
        password: password
    });

    return findUser(username)
        .then(user => {
            logger.debug('USER', user);
            if (!user)
            // res.json({success: false, message: 'Authentication failed. User not found.'});
                throw errors('USER_NOT_FOUND', 'The user was not found.', {username: username});

            // check if password matches
            if (user.password != password)
            //res.json({success: false, message: 'Authentication failed. Wrong password.'});
                throw errors('INVALID_PASSWORD', 'The password supplied was invalid', {username: username});

            return user;
        });
}

function createUser(username, password, name, email) {

    // TODO: Validate Required Arguments

    // Default Username to email address
    username = username || email;

    const user = {
        _id: username, // All usernames should be unique!
        username: username,
        email: email,
        name: name,
        password: password,
        profileImage: gravatar.url(email, {s: '200', r: 'pg', d: 'mm'})
    };

    // TODO: HASH Passwords
    // TODO: Store passwords in User.Passwords table

    return database
        .insert('Users', user);
}

// ** Exports
module.exports.findUser = findUser;
module.exports.authenticate = authenticate;
module.exports.createUser = createUser;