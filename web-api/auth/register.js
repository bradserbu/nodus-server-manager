'use strict';

// ** Dependencies
const Users = require('../users/users');
const logger = require('nodus-framework').logging.createLogger();

function registerUser(name, email, password) {
    // Register new user account
    logger.debug('REGISTER_USER', {
        name: name,
        email: email,
        password: password
    });

    // Use the users email address as the username
    const username = email;

    // Create a new user profile
    return Users
        .createUser(username, password, name, email)
        .then(result => {
            logger.debug('RESULT', result);

            // TODO: Send email verification
            return {
                username: username,
                registered: true,
                verified: false
            };
        });
}

// ** Exports
module.exports = registerUser;