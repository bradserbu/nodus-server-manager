'use strict';

// ** Dependencies
const Users = require('../users/users');
const jwt = require('jsonwebtoken');
const config = require('../config.json');
const logger = require('nodus-framework').logging.createLogger();

function login(username, password) {
    return Users
        .authenticate(username, password)
        .then(user => {
            logger.debug('CREATE_USER_TOKEN', user);

            // Create JWT token with user information
            const token = jwt.sign({user: user}, config.app_secret, {
                expiresIn: "1d" // expires in 24 hours
            });

            logger.debug('TOKEN', token);
            return {
                token: token.toString()
            };
        });
}

module.exports = login;