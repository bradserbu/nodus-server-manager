'use strict';

// ** Dependencies
const Entity = require('./Entity');

// ** Exports
module.exports = data => Entity('Server', data);