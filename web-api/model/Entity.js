'use strict';

// ** Dependencies
const util = require('util');

function Entity(type, data) {
    if (!util.isNullOrUndefined(data)) {
        return Object.assign({
            get TYPE() {
                return type;
            },
            get ID() {
                return this._id.toString();
            }
        }, data);
    }
}

// ** Exports
module.exports = Entity;