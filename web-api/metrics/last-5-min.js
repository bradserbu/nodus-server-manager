'use strict';

// ** Dependencies
const getServerMetric = require('./get-server-metric');
const moment = require('moment');

function lastFiveMinutes(server, metric) {

    // Time Range
    const now = moment();
    const five_min_ago = moment(now).subtract(5, 'minutes');
    const interval = '1m';

    return getServerMetric(server, metric, interval, five_min_ago, now);
}

module.exports = lastFiveMinutes;