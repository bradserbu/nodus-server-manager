'use strict';

// ** Dependencies
const _ = require('lodash');
const getServer = require('../servers/get');
const moment = require('moment');
const errors = require('nodus-framework').errors;
const MetricQuery = require('../../collector/metrics/MetricQuery');
const logger = require('nodus-framework').logging.createLogger();

/**
 * Fetch a metric for a server over the last hour
 */
function getServerMetric(server, metric, interval, from, to) {

    // TODO: Store the 'collector' address of the server in the 'servers' table in mongodb
    return getServer(server)
        .then(server => {

            if (!server)
                throw errors('NOT_FOUND', 'The server was not found in the database.', {server: server});

            // Get the servers internal hostname, which will be the 'host' in the metrics database
            const hostname = server.info.hostname;

            if (!hostname)
                throw errors('HOSTNAME_NOT_FOUND', 'The servers hostname was not found in the database.', {server: server});

            logger.debug('HOSTNAME', hostname);

            // Build a metric query
            const query = MetricQuery(metric, interval, from, to, hostname);
            logger.debug('QUERY', query);

            return query.run();
        });
}

// ** Exports
module.exports = getServerMetric;