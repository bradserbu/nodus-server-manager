'use strict';

// ** Dependencies
const getServerMetric = require('./get-server-metric');
const moment = require('moment');

function lastFifteenMinutes(server, metric) {

    // Time Range
    const now = moment();
    const fifteen_min_ago = moment(now).subtract(15, 'minutes');
    const interval = '1m';

    return getServerMetric(server, metric, interval, fifteen_min_ago, now);
}

module.exports = lastFifteenMinutes;