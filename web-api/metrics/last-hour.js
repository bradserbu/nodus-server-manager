'use strict';

// ** Dependencies
const getServerMetric = require('./get-server-metric');
const moment = require('moment');

function lastHour(server, metric) {

    // Time Range
    const now = moment();
    const one_hour_ago = moment(now).subtract(1, 'hour');
    const interval = '1m';

    return getServerMetric(server, metric, interval, one_hour_ago, now);
}

module.exports = lastHour;