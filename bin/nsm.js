#!/usr/bin/env node
'use strict';

// ** Dependencies
const yargs = require('yargs');
const logger = require('nodus-framework').logging.createLogger();

// ** Program Commands
const COMMANDS = {
    'agent-service': function(argv) {
        console.log("Installing agent as a service...");

        // Load Init Script
    }
};

// Parse Command Line Args
const argv = yargs.argv;

// Consume the first argument
const command = argv._.shift();
if (!command) {
    console.log("Please enter a command.");
    // TODO: Show Help...
}
