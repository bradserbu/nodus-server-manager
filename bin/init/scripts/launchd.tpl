<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
  <dict>
	  <key>Label</key>
	  <string>com.nodus.nsm-agent</string>
	  <key>UserName</key>
	  <string>%USER%</string>
    <key>KeepAlive</key>
    <true/>
	  <key>ProgramArguments</key>
	  <array>
		  <string>/usr/bin/env</string>
		  <string>%NSM_PATH% bin/start-agent.sh</string>
	  </array>
	  <key>RunAtLoad</key>
	  <true/>
	  <key>OnDemand</key>
	  <false/>
	  <key>LaunchOnlyOnce</key>
	  <true/>
	  <key>EnvironmentVariables</key>
    <dict>
      <key>COLLECTOR_HOST</key>
      <string>%COLLECTOR_HOST%</string>
      <key>COLLECTOR_PORT</key>
      <string>%COLLECTOR_PORT%</string>
    </dict>
	  <key>StandardErrorPath</key>
	  <string>/tmp/com.nodus.nsm.err</string>
	  <key>StandardOutPath</key>
	  <string>/tmp/com.nodus.nsm.out</string>
  </dict>
</plist>