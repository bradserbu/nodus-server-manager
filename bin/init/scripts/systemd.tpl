[Unit]
Description=NODUS Server Manager Agent
Documentation=https://bitbucket.org/bradserbu/nodus-server-manager
After=network.target

[Service]
Type=simple
User=nodus
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity
Environment=PATH=/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin
Environment=NSM_HOME=/home/nodus/nodus-server-manager

ExecStart=/home/nodus/nodus-server-manager/start-agent.sh
ExecReload=%PM2_PATH% reload all
ExecStop=%PM2_PATH% kill
KillSignal=SIGIN

[Install]
WantedBy=multi-user.target
