'use strict';

// ** Dependencies
const _ = require('lodash');
const Promise = require('bluebird');
const util = require('util');
const fs = require('fs-extra');
const Path = require('path');
const errors = require('nodus-framework').errors;
const logger = require('nodus-framework').logging.createLogger();
const detectInitSystem = require('./detectInitSystem');
const shelljs = require('shelljs');
const Commands = require('./commands');
const execa = require('execa');

const PARAMETERS = {
    USER: {
        type: String,
        required: true,
        defaultValue: () => 'sudo'
    },
    NSM_PATH: {
        type: String,
        required: true,
        defaultValue: () => Path.join(__dirname, '../..')
    },
    COLLECTOR_HOST: {
        type: String,
        required: true,
        defaultValue: () => 'localhost'
    },
    COLLECTOR_PORT: {
        type: Number,
        required: true,
        defaultValue: () => 8081
    }
};

// ** TODO: Move Parameters and DefaultValue to nodus-framework as a generic module
// TODO: Promise Support?
// TODO: Autoload from environment?
const DefaultValue = defaultValue => util.isFunction(defaultValue)
    ? defaultValue()
    : defaultValue;

const ScriptParameters = parameters => values => Promise.resolve(_
    .mapValues(parameters,
        (param, name) => {
            const type = param.type;
            const defaultValue = param.defaultValue;
            const isRequired = param.required;

            // Get the parameter value
            let value = values && values.hasOwnProperty(name)
                ? values[name]
                : (logger.info("Using parameter default...", name), DefaultValue(defaultValue));

            // Check Required
            if (util.isNullOrUndefined(value) && isRequired)
                throw errors('ARGUMENT_ERROR', `The parameter "${name}" is required and must have a value.`, {
                    parameter: name,
                    value: value
                });

            // Type conversion/validation
            if (type) {
                value = type(value);
            }

            logger.debug("- PARAMETER:", {
                name,
                value
            });

            return value;
        }
    ));

const ReplaceParameters = (content, parameters) => _
    .reduce(
        parameters,
        (contents, value, name) => {
            logger.info(`Replacing template parameter...`, {name, value});
            return contents.replace(new RegExp(`%${name}%`, "g"), value);
        },
        content
    );

function InitService(initSystem) {

    // The path to the script file for the init system
    const path = Path.join(__dirname, 'scripts', `${initSystem}.tpl`);

    // The parameters for the init script
    const parameters = ScriptParameters(PARAMETERS); // TODO: Let scripts set their own parameters

    // Load the contents of the template
    const template = () => fs.readFile(path, {encoding: 'utf-8'});

    // Create an init script
    const generate = params => template()
        .then(content => parameters(params)
            .then(params => ReplaceParameters(content, params)));

    // Generate the script and install it as a service
    const install = name => {

        logger.info("Installing service...", {name});
        const commands = Commands[initSystem].install(serviceName);

        // Run commands to install the script
        return Promise.map(commands, cmd => {
            logger.debug("RUN", cmd);
            return execa.stdout(cmd)
        });
    };

    return {
        path,
        parameters,
        template,
        generate
    }
}

// ** Exports
module.exports = InitService;