'use strict';

// ** Dependencies
const shelljs = require('shelljs');
const chalk = require('chalk');
const logger = require('nodus-framework').logging.createLogger();

/**
 * Detect running init system
 */
function detectInitSystem() {
    var hash_map = {
        'systemctl': 'systemd',
        'update-rc.d': 'upstart',
        'chkconfig': 'systemv',
        'rc-update': 'openrc',
        'launchctl': 'launchd'
    };
    var init_systems = Object.keys(hash_map);

    for (var i = 0; i < init_systems.length; i++) {
        if (shelljs.which(init_systems[i]) != null) {
            break;
        }
    }

    if (i >= init_systems.length) {
        logger.error('Init system not found.');
        return null;
    }

    logger.info('Init System found: ' + chalk.bold(hash_map[init_systems[i]]));
    return hash_map[init_systems[i]];
}

// ** Exports
module.exports = detectInitSystem;