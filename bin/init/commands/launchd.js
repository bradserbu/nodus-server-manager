'use strict';

// ** Dependencies
const path = require('path');

// ** Exports
function install (serviceName) {
    const destination = path.join(process.env.HOME, 'Library/LaunchAgents/' + serviceName + '.plist');
    return [
        'launchctl load -w ' + destination
    ];
}

function uninstall(serviceName) {
    throw('NOT_IMPLEMENTED');
}

// ** Exports
module.exports = {
    install,
    uninstall
};