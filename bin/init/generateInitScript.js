'use strict';

// ** Dependencies
const util = require('util');
const path = require('path');
const fs = require('fs-extra');
const InitScript = require('./InitService');
const detectInitSystem = require('./detectInitSystem');

// ** Defaults
const DEFAULT_USER = 'sudo';
const DEFAULT_NSM_PATH = path.join(__dirname, '../..');
const DEFAULT_COLLECTOR_HOST = 'localhost';
const DEFAULT_COLLECTOR_PORT = 8081;

function generateInitScript(initSystem = detectInitSystem(), options = {}) {

    // Parameters
    const user = options.user || DEFAULT_USER;
    const nsmPath = options.nsmPath || DEFAULT_NSM_PATH;
    const collectorHost = options.collectorHost || DEFAULT_COLLECTOR_HOST;
    const collectorPort = options.collectorPort || DEFAULT_COLLECTOR_PORT;

    // Load Init Script
    const script = InitScript(initSystem);

    return script.generate({
        user,
        nsmPath,
        collectorHost,
        collectorPort
    });
}

// ** Exports
module.exports = generateInitScript;