# NODUS Server Manager #

## Installation ##

- **STEP 1:** Install NodeJS
- **STEP 2:** Create '_nodus_' User Account
- **STEP 3:** Extract 'nodus-server-manager'
- **STEP 4:** Install Dependencies
- **STEP 5:** (TEST) Manually start the 'nsm-agent' collector
- **STEP 6:** Install 'nsm-agent' Service

### STEP 1: Install NodeJS ###

#### Red Hat® Enterprise Linux® / RHEL, CentOS and Fedora ####

**Supported Red Hat® Enterprise Linux® versions:**

- RHEL 5 (32-bit and 64-bit)
- RHEL 6 (32-bit and 64-bit)
- RHEL 7 (64-bit)

**Supported CentOS versions:***

- CentOS 5 (32-bit and 64-bit)
- CentOS 6 (32-bit and 64-bit)
- CentOS 7 (64-bit)


Node.js is available from the NodeSource Enterprise Linux and Fedora binary distributions repository. Support for this repository, along with its scripts, can be found on GitHub at nodesource/distributions.

Note that the Node.js packages for EL 5 (RHEL5 and CentOS 5) depend on the EPEL repository being available. The setup script will check and provide instructions if it is not installed.

**Run as root on RHEL, CentOS or Fedora:**

```
curl --silent --location https://rpm.nodesource.com/setup_7.x | bash -
```

**Install build tools, as root:**
```
yum -y install gcc-c++ make
```

**Then install, as root:**

```
yum -y install nodejs
```

**To check that node was installed successfully run:**
```
node --version
```

**NodeJS should print it's version information to the console:** 
```
v7.8.0
```

### STEP 2: Create '_nodus_' User Account ###

It is advised to create a separate service account for running the `agent` and `collector` services in the background.

Run the following commands, as root (_sudo_)

**Create a '_nodus_' user account:**
```
adduser nodus
```

**Use the `passwd` command to set the 'nodus' users password.**
```
passwd nodus
```

**Set and confirm the new user's password at the prompt. _A strong password is highly recommended!_**
```
Set password prompts:
Changing password for user username.
New password:
Retype new password:
passwd: all authentication tokens updated successfully.
```

### STEP 3: Extract 'nodus-server-manager' ###

Login as the 'nodus' user you just created.
```
su - nodus
```

Download the `nodus-server-manager` release package.
```
curl -lO http://147.75.66.217:8888/nodus-server-manager-0.1.0-0.tgz
```

Extract the `nodus-server-manager` release
```
tar -xvf nodus-server-manager-0.1.0-0.tgz
```

### STEP 4: Install Dependencies ###

There is an `install.sh` script located in the nodus-server-manager folder we just created that will install all 
the package dependencies.

**Change to the `nodus-server-manager` release directory, and run the `install.sh` script: **
```
cd nodus-server-manager
./install.sh
```

### STEP 5: (TEST) Manually start the 'nsm-agent' collector

To test that everything has been successfully installed, you can manually start the `nsm-agent` by running the following 
command from within the `nodus-server-manager` directory:
```
# Start `nsm-agent` process and collect stats every `10 seconds`
./start-agent.sh
```
_Press `CTRL+C` to EXIT_

The service should the following output: 
  
```
[nodus@nodus-centos nodus-server-manager]$ ./start-agent.sh
agent:commands:collect-stats:info Starting collector... 10000 +0ms
agent:commands:collect-stats:info Collecting stats... +10s
agent:commands:collect-stats:info Status sent successfully... +429ms
agent:commands:collect-stats:info Collecting stats... +10s
agent:commands:collect-stats:info Status sent successfully... +286ms
```

### STEP 5: Install the `nsm-agent` service (optional)

To run and start the `nsm-agent` automatically on reboot, using the standard linux `systemd` init system.

** Run the following, _as root_**
```
# Install 'nsm-agent.service' script
cp /home/nodus/nodus-server-manager/init-scripts/nsm-agent.service /etc/systemd/system

# Enable the service
systemctl enable nsm-agent.service

# Start the service
systemctl start nsm-agent.service
```

The `nsm-agent` service should now be installed and automatically collecting statistics.

- The service will start automatically on reboot

**To check the status of the service, run the following**
```
# Check the status of the service
systemctl status nsm-agent.service
```

You should see something similar to the following.  

**Note the `Active: active (running)`, which indicates the service is running and installed successfully**
```
[nodus@nodus-centos nodus-server-manager]$ sudo systemctl status nsm-agent.service
● nsm-agent.service - NODUS Server Manager Agent
   Loaded: loaded (/etc/systemd/system/nsm-agent.service; enabled; vendor preset: disabled)
   Active: active (running) since Fri 2017-05-26 03:43:42 UTC; 16s ago
     Docs: https://bitbucket.org/bradserbu/nodus-server-manager
 Main PID: 23096 (node)
   CGroup: /system.slice/nsm-agent.service
           └─23096 nsm-agent node_modules/.bin/nodus-run commands/collect-stats.js

May 26 03:43:42 nodus-centos systemd[1]: Started NODUS Server Manager Agent.
May 26 03:43:42 nodus-centos systemd[1]: Starting NODUS Server Manager Agent...
May 26 03:43:43 nodus-centos start-agent.sh[23096]: Fri, 26 May 2017 03:43:43 GMT agent:commands:collect-stats:info Starti...10000
May 26 03:43:53 nodus-centos start-agent.sh[23096]: Fri, 26 May 2017 03:43:53 GMT agent:commands:collect-stats:info Collec...ts...
May 26 03:43:54 nodus-centos start-agent.sh[23096]: Fri, 26 May 2017 03:43:54 GMT agent:commands:collect-stats:info Status...ly...
Hint: Some lines were ellipsized, use -l to show in full.
```

_(Optional)_ To view detailed system logs you can also use the following command: 

**Run the following command to view the `nsm-agent` logs**
```
journalctl -u nsm-agent.service
```
